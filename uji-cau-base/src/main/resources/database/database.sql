CREATE TABLE UJI_CAU.CAU_ALERTAS
(
  ID               NUMBER            NOT NULL,
  PRIORIDAD_NOMBRE VARCHAR2(20 BYTE) NOT NULL,
  NIVEL            NUMBER            NOT NULL,
  TIEMPO           NUMBER            NOT NULL
);

CREATE UNIQUE INDEX UJI_CAU.CAU_ALERTAS_PK ON UJI_CAU.CAU_ALERTAS (ID);
ALTER TABLE UJI_CAU.CAU_ALERTAS ADD ( CONSTRAINT CAU_ALERTAS_PK PRIMARY KEY ( ID );

CREATE TABLE UJI_CAU.CAU_ALERTAS_PARTES
(
  ID           NUMBER NOT NULL,
  ALERTA_ID    NUMBER NOT NULL,
  PARTE_ID     NUMBER,
  FECHA_ALERTA DATE,
  ENVIOS       VARCHAR2(255 BYTE)
);

CREATE UNIQUE INDEX UJI_CAU.CAU_ALERTAS_PARTES_PK ON UJI_CAU.CAU_ALERTAS_PARTES (ID);
ALTER TABLE UJI_CAU.CAU_ALERTAS_PARTES ADD ( CONSTRAINT CAU_ALERTAS_PARTES_PK PRIMARY KEY ( ID );
ALTER TABLE UJI_CAU.CAU_ALERTAS_PARTES ADD (CONSTRAINT CAU_ALERTAS_PARTES_FK1 FOREIGN KEY (ALERTA_ID) REFERENCES UJI_CAU.CAU_ALERTAS (ID));

CREATE TABLE UJI_CAU.CAU_AREAS
(
  ID      NUMBER(8),
  NOMBRE  VARCHAR2(255 BYTE),
  EXTERNA INTEGER DEFAULT 0
);
CREATE UNIQUE INDEX UJI_CAU.CAU_AREAS_PKX ON UJI_CAU.CAU_AREAS (ID);
ALTER TABLE UJI_CAU.CAU_AREAS ADD (CONSTRAINT CAU_AREAS_PK PRIMARY KEY (ID));


CREATE TABLE UJI_CAU.CAU_CATEGORIAS_AREAS
(
  ID           NUMBER(8),
  NOMBRE       VARCHAR2(255 BYTE),
  ALTA         INTEGER,
  CAU_AREAS_ID NUMBER(8)
);

CREATE INDEX UJI_CAU.CAU_RESOLUCIONES_CAU_AREAS_FKX ON UJI_CAU.CAU_CATEGORIAS_AREAS (CAU_AREAS_ID);
CREATE UNIQUE INDEX UJI_CAU.CAU_RESOLUCIONES_PKX ON UJI_CAU.CAU_CATEGORIAS_AREAS (ID);

ALTER TABLE UJI_CAU.CAU_CATEGORIAS_AREAS ADD (CONSTRAINT CAU_CATEGORIAS_PK PRIMARY KEY (ID));
ALTER TABLE UJI_CAU.CAU_CATEGORIAS_AREAS ADD (CONSTRAINT CAU_CATEGORIAS_CAU_AREAS_FK FOREIGN KEY (CAU_AREAS_ID) REFERENCES UJI_CAU.CAU_AREAS (ID));

CREATE TABLE UJI_CAU.CAU_CONFIGURACION
(
  ID               NUMBER(12),
  MAIL_INVENTARIO  VARCHAR2(1024 BYTE),
  MAIL_SUGERENCIAS VARCHAR2(1024 BYTE),
  MAIL_ENCUESTAS   VARCHAR2(1024 BYTE),
  N_CAU_ENCUESTAS  VARCHAR2(1024 BYTE)
);

CREATE UNIQUE INDEX UJI_CAU.CAU_CONFIGURACION_PKX ON UJI_CAU.CAU_CONFIGURACION (ID);

ALTER TABLE UJI_CAU.CAU_CONFIGURACION ADD (CONSTRAINT CAU_CONFIGURACION_PK PRIMARY KEY (ID));

CREATE TABLE UJI_CAU.CAU_PARTES
(
  ID                  NUMBER(12),
  ASUNTO              VARCHAR2(255 CHAR),
  CODIGO_BARRAS       VARCHAR2(30 CHAR),
  DESCRIPCION         VARCHAR2(4000 CHAR),
  PRESENTE            INTEGER DEFAULT 0,
  PER_ID_CREACION     NUMBER(8),
  PER_ID_AFECTADO     NUMBER(8),
  PER_ID_SOLICITANTE  NUMBER(8),
  ID_PRIORIDAD        NUMBER(8),
  ID_IMPACTO          NUMBER(8),
  ID_URGENCIA         NUMBER(8),
  ID_UBICACION_FISICA NUMBER(18),
  EXTENSION_TELEFONO  VARCHAR2(10 BYTE),
  TIPO                VARCHAR2(100 BYTE),
  PER_ID_ASIGNADO     NUMBER(8),
  ID_AREA             NUMBER(8),
  ID_CATEGORIA        NUMBER(8),
  ID_TIPO             NUMBER(8),
  PAR_ID_EXTERNO      NUMBER(12),
  LEIDO               INTEGER DEFAULT 0,
  SUSCRITO            NUMBER DEFAULT 1
);

CREATE INDEX UJI_CAU.CAU_IPOS_PARTES_FKX ON UJI_CAU.CAU_PARTES (ID_TIPO);
CREATE INDEX UJI_CAU.CAU_PARTES_AREAS_FKX ON UJI_CAU.CAU_PARTES (ID_AREA);
CREATE INDEX UJI_CAU.CAU_PARTES_CAU_RES_FKX ON UJI_CAU.CAU_PARTES (ID_CATEGORIA);
CREATE INDEX UJI_CAU.CAU_PARTES_EXT_CRE_FKX ON UJI_CAU.CAU_PARTES (PER_ID_CREACION);
CREATE INDEX UJI_CAU.CAU_PARTES_EXTERNOS_FKX ON UJI_CAU.CAU_PARTES (PAR_ID_EXTERNO);
CREATE INDEX UJI_CAU.CAU_PARTES_IMPACTOS_FKX ON UJI_CAU.CAU_PARTES
(ID_IMPACTO);
CREATE INDEX UJI_CAU.CAU_PARTES_PER_EN_NOM_DE_FKX ON UJI_CAU.CAU_PARTES (PER_ID_AFECTADO);
CREATE INDEX UJI_CAU.CAU_PARTES_UBI_FISICAS_FKX ON UJI_CAU.CAU_PARTES (ID_UBICACION_FISICA);
CREATE INDEX UJI_CAU.CAU_PER_ASIGNADO_FKX ON UJI_CAU.CAU_PARTES
(PER_ID_ASIGNADO);
CREATE INDEX UJI_CAU.CAU_PER_TECNICO_FKX ON UJI_CAU.CAU_PARTES
(PER_ID_SOLICITANTE);
CREATE INDEX UJI_CAU.CAU_PRIORIDADES_FKX ON UJI_CAU.CAU_PARTES
(ID_PRIORIDAD);
CREATE INDEX UJI_CAU.CAU_URGENCIAS_FKX ON UJI_CAU.CAU_PARTES
(ID_URGENCIA);

CREATE UNIQUE INDEX UJI_CAU.CAU_PARTES_PKX ON UJI_CAU.CAU_PARTES (ID);
ALTER TABLE UJI_CAU.CAU_PARTES ADD (CONSTRAINT CAU_PARTES_PK PRIMARY KEY (ID));

ALTER TABLE UJI_CAU.CAU_PARTES ADD
(CONSTRAINT CAU_PARTES_AREAS_FK FOREIGN KEY (ID_AREA) REFERENCES UJI_CAU.CAU_AREAS (ID),
CONSTRAINT CAU_PARTES_EXT_CRE_FK FOREIGN KEY (PER_ID_CREACION) REFERENCES GRI_PER.PER_PERSONAS (ID),
CONSTRAINT CAU_PARTES_PER_EN_NOM_DE_FK FOREIGN KEY (PER_ID_AFECTADO) REFERENCES GRI_PER.PER_PERSONAS (ID),
CONSTRAINT CAU_PER_ASIGNADO_FK FOREIGN KEY (PER_ID_ASIGNADO) REFERENCES GRI_PER.PER_PERSONAS (ID),
CONSTRAINT CAU_PER_TECNICO_FK FOREIGN KEY (PER_ID_SOLICITANTE) REFERENCES GRI_PER.PER_PERSONAS (ID),
CONSTRAINT CAU_PARTES_EXTERNOS_FK FOREIGN KEY (PAR_ID_EXTERNO) REFERENCES UJI_CAU.CAU_PARTES (ID),
CONSTRAINT CAU_PARTES_CAU_RES_FK FOREIGN KEY (ID_CATEGORIA) REFERENCES UJI_CAU.CAU_CATEGORIAS_AREAS (ID),
CONSTRAINT CAU_PARTES_IMPACTOS_FK FOREIGN KEY (ID_IMPACTO) REFERENCES UJI_CAU.CAU_TIPOS_IMPACTOS (ID),
CONSTRAINT CAU_PRIORIDADES_FK FOREIGN KEY (ID_PRIORIDAD) REFERENCES UJI_CAU.CAU_TIPOS_PRIORIDADES (ID),
CONSTRAINT CAU_URGENCIAS_FK FOREIGN KEY (ID_URGENCIA) REFERENCES UJI_CAU.CAU_TIPOS_URGENCIAS (ID),
CONSTRAINT CAU_IPOS_PARTES_FK FOREIGN KEY (ID_TIPO) REFERENCES UJI_CAU.CAU_TIPOS_PARTES (ID));

CREATE TABLE UJI_CAU.CAU_PARTES_COMUNICACIONES
(
  PAR_ID          NUMBER(12),
  ID              NUMBER,
  DESCRIPCION     VARCHAR2(4000 BYTE),
  F_CREACION      DATE,
  PER_ID_CREACION NUMBER(8),
  INTERNO         INTEGER
);

CREATE INDEX UJI_CAU.CAU_PARTES_COMUNICACIONES_FKV2 ON UJI_CAU.CAU_PARTES_COMUNICACIONES (PER_ID_CREACION);
CREATE INDEX UJI_CAU.CAU_PARTES_COMUNICACIONES_FKX ON UJI_CAU.CAU_PARTES_COMUNICACIONES (PAR_ID);
CREATE UNIQUE INDEX UJI_CAU.CAU_PARTES_COMUNICACIONES_PKX ON UJI_CAU.CAU_PARTES_COMUNICACIONES (ID);

ALTER TABLE UJI_CAU.CAU_PARTES_COMUNICACIONES ADD (CONSTRAINT CAU_PARTES_COMUNICACIONES_PK PRIMARY KEY (ID));

ALTER TABLE UJI_CAU.CAU_PARTES_COMUNICACIONES ADD (
CONSTRAINT CAU_PARTES_COMUNICACIONES_FKV2 FOREIGN KEY (PER_ID_CREACION) REFERENCES GRI_PER.PER_PERSONAS (ID),
CONSTRAINT CAU_PARTES_COMUNICACIONES_FK FOREIGN KEY (PAR_ID) REFERENCES UJI_CAU.CAU_PARTES (ID));

CREATE TABLE UJI_CAU.CAU_PARTES_ESTADOS
(
  ID            NUMBER(8),
  PAR_ID        NUMBER(12),
  ESTADO_ID     NUMBER,
  F_INICIO      DATE,
  F_FIN         DATE,
  PER_ID_INICIO NUMBER(8),
  PER_ID_FIN    NUMBER(8),
  DESCRIPCION   VARCHAR2(1024 BYTE),
  ACTUAL        NUMBER
);

CREATE INDEX UJI_CAU.CAU_PAR_EST_CAU_ESTADOS_FKX ON UJI_CAU.CAU_PARTES_ESTADOS (ESTADO_ID);

CREATE INDEX UJI_CAU.CAU_PARTES_ESTADOS_CAMB_FKX ON UJI_CAU.CAU_PARTES_ESTADOS (PER_ID_FIN);

CREATE INDEX UJI_CAU.CAU_PARTES_ESTADOS_CRE_FKX ON UJI_CAU.CAU_PARTES_ESTADOS (PER_ID_INICIO);

CREATE INDEX UJI_CAU.CAU_PARTES_ESTADOS_FKX ON UJI_CAU.CAU_PARTES_ESTADOS (PAR_ID);

CREATE UNIQUE INDEX UJI_CAU.CAU_PARTES_ESTADOS_PKX ON UJI_CAU.CAU_PARTES_ESTADOS (ID);

ALTER TABLE UJI_CAU.CAU_PARTES_ESTADOS ADD (CONSTRAINT CAU_PARTES_ESTADOS_PK PRIMARY KEY (ID));

ALTER TABLE UJI_CAU.CAU_PARTES_ESTADOS ADD (
CONSTRAINT CAU_PARTES_ESTADOS_CAMB_FK FOREIGN KEY (PER_ID_FIN) REFERENCES GRI_PER.PER_PERSONAS (ID),
CONSTRAINT CAU_PARTES_ESTADOS_CRE_FK FOREIGN KEY (PER_ID_INICIO) REFERENCES GRI_PER.PER_PERSONAS (ID),
CONSTRAINT CAU_PARTES_ESTADOS_FK FOREIGN KEY (PAR_ID) REFERENCES UJI_CAU.CAU_PARTES (ID));


CREATE TABLE UJI_CAU.CAU_PARTES_FICHEROS
(
  ID             NUMBER(8),
  FICHERO        BLOB,
  NOTAS          VARCHAR2(4000 BYTE),
  NOMBRE_FICHERO VARCHAR2(250 BYTE),
  PAR_ID         NUMBER(12),
  MIME           VARCHAR2(255 BYTE)
);

CREATE INDEX UJI_CAU.CAU_PARTES_FICHEROS_FKX ON UJI_CAU.CAU_PARTES_FICHEROS (PAR_ID);

CREATE UNIQUE INDEX UJI_CAU.CAU_PARTES_FICHEROS_PKX ON UJI_CAU.CAU_PARTES_FICHEROS (ID);

ALTER TABLE UJI_CAU.CAU_PARTES_FICHEROS ADD (CONSTRAINT CAU_PARTES_FICHEROS_PK
PRIMARY KEY (ID));

ALTER TABLE UJI_CAU.CAU_PARTES_FICHEROS ADD (CONSTRAINT CAU_PARTES_FICHEROS_FK FOREIGN KEY (PAR_ID) REFERENCES UJI_CAU.CAU_PARTES (ID));

CREATE TABLE UJI_CAU.CAU_TECNICOS_AREAS
(
  ID                   NUMBER(8),
  PER_ID_TECNICO       NUMBER(8),
  ID_AREA              NUMBER(8),
  F_INICIO_INACTIVIDAD DATE,
  F_FIN_INACTIVIDAD    DATE,
  TIPO                 VARCHAR2(40 BYTE)
);

CREATE UNIQUE INDEX UJI_CAU.CAU_AT_UNKX ON UJI_CAU.CAU_TECNICOS_AREAS (PER_ID_TECNICO, ID_AREA);

CREATE INDEX UJI_CAU.CAU_TEC_AREAS_FKX ON UJI_CAU.CAU_TECNICOS_AREAS (ID_AREA);

CREATE INDEX UJI_CAU.CAU_TEC_AR_FKX ON UJI_CAU.CAU_TECNICOS_AREAS (PER_ID_TECNICO);

CREATE UNIQUE INDEX UJI_CAU.CAU_TECNICOS_AREAS_PKX ON UJI_CAU.CAU_TECNICOS_AREAS (ID);

ALTER TABLE UJI_CAU.CAU_TECNICOS_AREAS ADD (CHECK (TIPO IN
                                                   ('ADMINISTRADOR', 'EXTERNO', 'JEFE_AREA', 'PRIMERA', 'SEGUNDA', 'SEGUNDA_ASIGNAR')), CONSTRAINT CAU_TECNICOS_AREAS_PK PRIMARY KEY (ID), CONSTRAINT CAU_AT_UNK UNIQUE (PER_ID_TECNICO, ID_AREA));

ALTER TABLE UJI_CAU.CAU_TECNICOS_AREAS ADD (
CONSTRAINT CAU_TEC_AREAS_FK FOREIGN KEY (ID_AREA) REFERENCES UJI_CAU.CAU_AREAS (ID),
CONSTRAINT CAU_TEC_AR_FK FOREIGN KEY (PER_ID_TECNICO) REFERENCES GRI_PER.PER_PERSONAS (ID));

CREATE TABLE UJI_CAU.CAU_TIPOS_ESTADOS_PARTES
(
  ID              NUMBER,
  NOMBRE          VARCHAR2(255 CHAR),
  ESTADO_PADRE_ID NUMBER
);

CREATE UNIQUE INDEX UJI_CAU.CAU_TIPOS_ESTADOS_PARTES_PKX ON UJI_CAU.CAU_TIPOS_ESTADOS_PARTES (ID);

CREATE INDEX UJI_CAU.CAU_TIP_PAR_ESTADO_PARTES_FKX ON UJI_CAU.CAU_TIPOS_ESTADOS_PARTES (ESTADO_PADRE_ID);

ALTER TABLE UJI_CAU.CAU_TIPOS_ESTADOS_PARTES ADD (CONSTRAINT CAU_TIPOS_ESTADOS_PARTES_PK PRIMARY KEY (ID));

ALTER TABLE UJI_CAU.CAU_TIPOS_ESTADOS_PARTES ADD (CONSTRAINT CAU_TIP_PAR_ESTADO_PARTES_FK FOREIGN KEY (ESTADO_PADRE_ID) REFERENCES UJI_CAU.CAU_TIPOS_ESTADOS_PARTES (ID));

CREATE TABLE UJI_CAU.CAU_TIPOS_IMPACTOS
(
  ID     NUMBER(8),
  NOMBRE VARCHAR2(255 BYTE)
);

CREATE UNIQUE INDEX UJI_CAU.CAU_TIPOS_IMPACTOS_PKX ON UJI_CAU.CAU_TIPOS_IMPACTOS (ID);

ALTER TABLE UJI_CAU.CAU_TIPOS_IMPACTOS ADD (CONSTRAINT CAU_TIPOS_IMPACTOS_PK PRIMARY KEY (ID));

CREATE TABLE UJI_CAU.CAU_TIPOS_PARTES
(
  ID     NUMBER(8),
  NOMBRE VARCHAR2(255 BYTE)
);

CREATE UNIQUE INDEX UJI_CAU.CAU_TIPOS_PARTES_PKX ON UJI_CAU.CAU_TIPOS_PARTES (ID);

ALTER TABLE UJI_CAU.CAU_TIPOS_PARTES ADD (CONSTRAINT CAU_TIPOS_PARTES_PK PRIMARY KEY (ID));

CREATE TABLE UJI_CAU.CAU_TIPOS_PRIORIDADES
(
  ID           NUMBER(8),
  NOMBRE       VARCHAR2(255 BYTE),
  COLOR        VARCHAR2(100 BYTE),
  ID_IMPACTOS  NUMBER(8),
  ID_URGENCIAS NUMBER(8)
);

CREATE INDEX UJI_CAU.CAU_PRIO_TIPOS_IMPACTOS_FKX ON UJI_CAU.CAU_TIPOS_PRIORIDADES (ID_URGENCIAS);

CREATE INDEX UJI_CAU.CAU_TIPOS_PRIO_NOMBRE_INX ON UJI_CAU.CAU_TIPOS_PRIORIDADES (NOMBRE);

CREATE UNIQUE INDEX UJI_CAU.CAU_TIPOS_PRIORIDAD_PKX ON UJI_CAU.CAU_TIPOS_PRIORIDADES (ID);

CREATE INDEX UJI_CAU.CAU_TIPOS_PRIO_URGEN_FKX ON UJI_CAU.CAU_TIPOS_PRIORIDADES (ID_IMPACTOS);

ALTER TABLE UJI_CAU.CAU_TIPOS_PRIORIDADES ADD (CONSTRAINT CAU_TIPOS_PRIORIDAD_PK PRIMARY KEY (ID));

ALTER TABLE UJI_CAU.CAU_TIPOS_PRIORIDADES ADD (
CONSTRAINT CAU_PRIO_TIPOS_IMPACTOS_FK FOREIGN KEY (ID_URGENCIAS) REFERENCES UJI_CAU.CAU_TIPOS_IMPACTOS (ID),
CONSTRAINT CAU_TIPOS_PRIO_URGEN_FK FOREIGN KEY (ID_IMPACTOS) REFERENCES UJI_CAU.CAU_TIPOS_URGENCIAS (ID));

CREATE TABLE UJI_CAU.CAU_TIPOS_URGENCIAS
(
  ID     NUMBER(8),
  NOMBRE VARCHAR2(255 BYTE)
);

CREATE UNIQUE INDEX UJI_CAU.CAU_TIPOS_URGENCIAS_PKX ON UJI_CAU.CAU_TIPOS_URGENCIAS (ID);

ALTER TABLE UJI_CAU.CAU_TIPOS_URGENCIAS ADD (CONSTRAINT CAU_TIPOS_URGENCIAS_PK PRIMARY KEY (ID));

CREATE TABLE UJI_CAU.CAU_VMC_PERSONAS
(
  ID                   NUMBER(8)          NOT NULL,
  NOMBRE               VARCHAR2(120 CHAR) NOT NULL,
  APELLIDO1            VARCHAR2(120 CHAR),
  APELLIDO2            VARCHAR2(120 CHAR),
  NOMBRE_COMPLETO      VARCHAR2(1206 CHAR),
  DNI                  VARCHAR2(100 CHAR) NOT NULL,
  TIPO                 VARCHAR2(5 CHAR),
  CUENTA               VARCHAR2(60 CHAR),
  EXTENSION_TELEFONO   VARCHAR2(4000 CHAR),
  ID_UBICACION_FISICA  NUMBER,
  APELLIDOS_NOMBRE_BUS VARCHAR2(4000 CHAR),
  NOMBRE_COMPLETO_BUS  VARCHAR2(4000 CHAR)
);

CREATE TABLE UJI_CAU.MIGRACION_CAU
(
  ID_ANT   NUMBER(22),
  ID_NUEVO NUMBER(22)
);


CREATE OR REPLACE FORCE VIEW UJI_CAU.CAU_EXT_CALENDARIO
(
    DIA, MES, ANYO, SEMANA, TIPO_LAB, INCIDENCIA_LAB, TIPO_ACA, INCIDENCIA_ACA, DIA_SEMANA, HORAS_DIARIAS1, HORAS_DIARIAS2, NO_RECUPERABLE, HAY_TARDES, FECHA_COMPLETA
)
AS
  SELECT
    DIA,
    MES,
    "AÑO",
    SEMANA,
    TIPO_LAB,
    INCIDENCIA_LAB,
    TIPO_ACA,
    INCIDENCIA_ACA,
    DIA_SEMANA,
    HORAS_DIARIAS1,
    HORAS_DIARIAS2,
    NO_RECUPERABLE,
    HAY_TARDES,
    FECHA_COMPLETA
  FROM grh_calendario;


CREATE OR REPLACE FORCE VIEW UJI_CAU.CAU_EXT_ELEMENTOS_INVENTARIOS
(
    CODIGO_BARRAS,
    DESCRIPCION,
    ID_UBICACION_FISICA,
    ALTA,
    MARCA,
    MODELO,
    N_SERIE,
    IP_EQUIPO,
    NOMBRE_DNS,
    SIS_SISTEMA,
    MANTENIMIENTO,
    EMPRESA_MANTENIMENTO,
    DESCRIPCION_CONTRATO,
    QUIEN_ADMINISTRA,
    USU_ADMIN
)
AS
  SELECT
    CB,
    (SELECT
    a.nombre
     FROM gre_articulos a
     WHERE a.id = l.id_art)
      descripcion,
SELECT
  UBI.EDI_ARE_AREA || UBI.EDI_EDIFICIO || UBI.PLANTA|UBI.DEPENDENCIA|UBI.TUBIC_ID
FROM EST_UBICACIONES UBI WHERE l.id_ubi = ID )
id_ubicacion_fisica,
estado,
(SELECT f.descripcion FROM gre_fabricantes f WHERE f.id = l.id_mod_fab)
marca,
(SELECT m.descripcion FROM gre_modelos m
WHERE id_fab = l.ID_MOD_FAB
AND id_art = l.ID_MOD_ART
AND id_mod = m.id)
modelo,
N_SERIE,
(SELECT ip_ip1|| '.'|| ip_ip2|| '.'|| ip_ip3|| '.'|| ip_ip4
FROM GEI_VW_TARJETAS_IPS ip
WHERE ip.lin_id = l.id AND ROWNUM = 1)
IP_EQUIPO,
(SELECT NOMBRE
FROM GEI_VW_TARJETAS_IPS ip
WHERE ip.lin_id = l.id AND ROWNUM = 1)
NOMBRE_EQUIPO,
(SELECT DESCRIPCIO
FROM gpi_sistemes_op op
WHERE op.SISTEMA = l.SIS_SISTEMA)
SIS_SISTEMA,
(SELECT DECODE (me.tipo, 'G', 'Garantia', me.mant_id)
FROM gre_manten_inf me,
gre_mantenimientos mm,
per_personas p
WHERE       me.lin_id = l.id
AND SYSDATE BETWEEN me.data_inici AND me.data_fi
AND mm.id(+) = me.mant_id
AND p.id = NVL (me.per_id, mm.per_id_ter)
AND ROWNUM = 1)
mantenimento,
(SELECT p.nombre_comercial
FROM gre_manten_inf me,
gre_mantenimientos mm,
per_personas p
WHERE me.lin_id = l.id
AND SYSDATE BETWEEN me.data_inici AND me.data_fi
AND mm.id(+) = me.mant_id
AND p.id = NVL (me.per_id, mm.per_id_ter)
AND ROWNUM = 1)
empresa_mantenimento,
(SELECT 'Del '
|| TO_CHAR (me.data_inici, 'dd/mm/yyyy')
|| ' al '
|| TO_CHAR (me.data_fi, 'dd/mm/yyyy')
FROM gre_manten_inf me
WHERE me.lin_id = l.id
AND SYSDATE BETWEEN me.data_inici AND me.data_fi
AND ROWNUM = 1)
Descripcion_contrato,
administra,
(SELECT DECODE (
l.administra,
'U',
DECODE (
l.usu_administra,
NULL,
SUBSTR (busca_cuenta (e.per_id),
1,
instr (busca_cuenta (e.per_id), '@') - 1),
l.usu_administra
),
NULL
)
FROM gei_per_equipo e
WHERE lin_id = l.id AND rownum = 1)
usu_administra
FROM gre_lineas_inventario l
WHERE CB LIKE '2%' AND id_art LIKE '11%';

CREATE OR REPLACE FORCE VIEW UJI_CAU.CAU_EXT_PERSONAS
(
    ID,
    NOMBRE,
    APELLIDO1,
    APELLIDO2,
    NOMBRE_COMPLETO,
    DNI,
    TIPO,
    CUENTA,
    EXTENSION_TELEFONO,
    ID_UBICACION_FISICA,
    APELLIDOS_NOMBRE_BUS,
    NOMBRE_COMPLETO_BUS
)
AS
  SELECT
    P.ID,
    p.nombre,
    APELLIDO1,
    APELLIDO2,
    LTRIM(LTRIM(
              LTRIM(REPLACE(
                        APELLIDO1 || ' ' || APELLIDO2 || ', ' || NOMBRE,
                        ' , ',
                        ', '
                    )),
              ','
          )),
    p.IDENTIFICACION,
    (SELECT
       ACT_id
     FROM grh_vw_contrataciones_ult
     WHERE per_id = p.id AND ROWNUM = 1),
--busca_cuenta_ext (P.ID),
    busca_cuenta(p.id),
/*(SELECT   MAX (nombre)
   FROM   per_cuentas
  WHERE   psv_per_id = p.id and principal='S'), */
    busca_extension(P.ID),
    (SELECT
       f.id
     FROM GRH_VW_TELEFONS t, CAU_EXT_UBICACIONES_FISICAS f
     WHERE PER_ID = P.ID AND ROWNUM = 1 AND f.NOMBRE = UFISICA),
    LIMPIA(LTRIM(LTRIM(
                     LTRIM(REPLACE(
                               APELLIDO1
                               || ' '
                               || APELLIDO2
                               || ', '
                               || NOMBRE,
                               ' , ',
                               ', '
                           )),
                     ','
                 ))),
    LIMPIA(LTRIM(LTRIM(
                     LTRIM(REPLACE(
                               NOMBRE
                               || ' '
                               || APELLIDO1
                               || ' '
                               || APELLIDO2,
                               ' , ',
                               ', '
                           )),
                     ','
                 )))
  FROM per_personas p
  WHERE p.id IN
        (SELECT
           per_id
         FROM per_personas_subvinculos
         WHERE per_id = p.id
               AND nvl(fecha_fin, '31/01/2050') >
                   trunc(sysdate));

CREATE OR REPLACE FORCE VIEW UJI_CAU.CAU_EXT_PERSONAS_UBICACIONES
(
    PER_ID,
    UBI_ID
)
AS
  SELECT
    p.id,
    f.id
  FROM per_personas p, GRH_VW_TELEFONS t, CAU_EXT_UBICACIONES_FISICAS f
  WHERE PER_ID = P.ID AND f.NOMBRE = UFISICA
        AND p.id IN
            (SELECT
               per_id
             FROM per_personas_subvinculos
             WHERE per_id = p.id
                   AND NVL(fecha_fin, '31/01/2050') >
                       TRUNC(SYSDATE));


CREATE OR REPLACE FORCE VIEW UJI_CAU.CAU_EXT_UBICACIONES_FISICAS
(
    FIS_ID,
    NOMBRE,
    UBI_ID,
    NOMBRE_UBICACION,
    ID
)
AS
  SELECT
    ID,
    CODIGO_UBICACION,
    LOGICALID,
    LOGICAL,
    ID * 100000 + LOGICALID
  FROM EST_VW_UBICACIONES, EST_VW_SC_LOGICAS
  WHERE ESTADO_UBI = 'A' AND CODIGO_UBICACION = SPACEID;


CREATE OR REPLACE FORCE VIEW UJI_CAU.CAU_VW_PARTES
(
    ID,
    ASUNTO,
    CODIGO_BARRAS,
    DESCRIPCION,
    PRESENTE,
    LEIDO,
    SUSCRITO,
    EXTENSION_TELEFONO,
    TIPO,
    ID_AREA,
    ID_CATEGORIA,
    PER_ID_ASIGNADO,
    ASIGNADO_NOMBRE_COMPLETO,
    ASIGNADO_CUENTA,
    ASIGNADO_EXTENSION_TELEFONO,
    ASIGNADO_DEPARTAMENTO,
    ASIGNADO_UBICACION,
    FECHA_ASIGNACION,
    FECHA_CREACION,
    PER_ID_CREACION,
    CREADOR_NOMBRE_COMPLETO,
    CREADOR_CUENTA,
    CREADOR_EXTENSION_TELEFONO,
    CREADOR_DEPARTAMENTO,
    CREADOR_UBICACION,
    PER_ID_SOLICITANTE,
    SOLICITANTE_NOMBRE_COMPLETO,
    SOLICITANTE_CUENTA,
    SOLICITANTE_EXTENSION_TELEFONO,
    SOLICITANTE_DEPARTAMENTO,
    SOLICITANTE_UBICACION,
    PER_ID_AFECTADO,
    AFECTADO_NOMBRE_COMPLETO,
    AFECTADO_CUENTA,
    AFECTADO_EXTENSION_TELEFONO,
    AFECTADO_DEPARTAMENTO,
    AFECTADO_UBICACION,
    ID_PRIORIDAD,
    PRIORIDAD_NOMBRE,
    PRIORIDAD_COLOR,
    ID_UBICACION_FISICA,
    UBICACION_NOMBRE,
    ID_IMPACTO,
    ID_URGENCIA,
    PAR_ID_EXTERNO,
    ESTADO,
    ESTADO_NOMBRE,
    ESTADO_PARENT,
    ESTADO_F_INICIO,
    ESTADO_F_FIN
)
AS
  SELECT
    p.id,
    p.asunto,
    p.codigo_barras,
    p.descripcion,
    p.presente,
    p.leido,
    p.suscrito,
    p.extension_telefono,
    p.tipo,
    p.ID_AREA,
    p.ID_CATEGORIA,
    p.per_id_asignado,
    perAsig.NOMBRE_COMPLETO    asignado_nombre_completo,
    perAsig.cuenta             asignado_cuenta,
    perAsig.EXTENSION_TELEFONO asignado_extension_telefono,
    (SELECT
    NOMBRE_UBICACION
     FROM CAU_EXT_UBICACIONES_FISICAS
     WHERE id = perAsig.ID_UBICACION_FISICA)
                               asignado_departamento,
    (SELECT
    NOMBRE
     FROM CAU_EXT_UBICACIONES_FISICAS
     WHERE id = perAsig.ID_UBICACION_FISICA)
                               asignado_ubicacion,
    (SELECT
    max(f_inicio)
     FROM CAU_PARTES_ESTADOS
     WHERE par_id = p.id AND estado_id = 30)
                               fecha_asignacion,
    (SELECT
    min(f_inicio)
     FROM CAU_PARTES_ESTADOS
     WHERE par_id = p.id AND estado_id = 10)
                               fecha_creacion,
    p.PER_ID_CREACION,
    perCrea.NOMBRE_COMPLETO    creador_nombre_completo,
    perCrea.cuenta             creador_cuenta,
    perCrea.EXTENSION_TELEFONO creador_extension_telefono,
    ubiCrea.NOMBRE_UBICACION   creador_departamento,
    ubiCrea.NOMBRE             creador_ubicacion,
    p.per_id_solicitante,
    perSoli.NOMBRE_COMPLETO    solicitante_NOMBRE_COMPLETO,
    perSoli.cuenta             solicitante_cuenta,
    perSoli.EXTENSION_TELEFONO solicitante_extension_telefono,
    ubiSoli.NOMBRE_UBICACION   solicitante_departamento,
    ubiSoli.NOMBRE             solicitante_ubicacion,
    p.per_id_afectado,
    perAfec.NOMBRE_COMPLETO    afectado_NOMBRE_COMPLETO,
    perAfec.cuenta             afectado_cuenta,
    perAfec.EXTENSION_TELEFONO afectado_EXTENSION_TELEFONO,
    ubiAfec.NOMBRE_UBICACION   afectado_departamento,
    ubiAfec.NOMBRE             afectado_ubicacion,
    p.ID_PRIORIDAD,
    prio.nombre                prioridad_nombre,
    prio.color                 prioridad_color,
    p.ID_UBICACION_FISICA,
    ubi.nombre                 ubicacion_nombre,
    p.ID_IMPACTO,
    p.ID_URGENCIA,
    p.par_id_externo,
    tipoEst.id                 estado,
    tipoEst.nombre             estado_nombre,
    tipoEst.ESTADO_PADRE_ID    estado_parent,
    est.f_inicio               estado_f_inicio,
    est.f_fin                  estado_f_fin
  FROM CAU_PARTES p,
    CAU_VMC_PERSONAS perAsig,
    CAU_VMC_PERSONAS perCrea,
    CAU_VMC_PERSONAS perSoli,
    CAU_VMC_PERSONAS perAfec,
    CAU_TIPOS_PRIORIDADES prio,
    CAU_EXT_UBICACIONES_FISICAS ubi,
    CAU_PARTES_ESTADOS est,
    CAU_TIPOS_ESTADOS_PARTES tipoEst,
    CAU_EXT_UBICACIONES_FISICAS ubiAfec,
    CAU_EXT_UBICACIONES_FISICAS ubiCrea,
    CAU_EXT_UBICACIONES_FISICAS ubiSoli
  WHERE p.PER_ID_ASIGNADO = perAsig.id (+)
        AND p.PER_ID_CREACION = perCrea.id
        AND p.PER_ID_SOLICITANTE = perSoli.id
        AND p.PER_ID_AFECTADO = perAfec.id
        AND p.ID_PRIORIDAD = prio.id (+)
        AND p.ID_UBICACION_FISICA = ubi.id (+)
        AND p.ID = est.par_id
        AND est.ACTUAL = 1
        AND est.estado_id = tipoEst.id
        AND perAfec.id_ubicacion_fisica = ubiAfec.id (+)
        AND perCrea.id_ubicacion_fisica = ubiCrea.id (+)
        AND perSoli.id_ubicacion_fisica = ubiSoli.id (+);


CREATE OR REPLACE FORCE VIEW UJI_CAU.CAU_VW_PARTES_ALERTAS
(
    PARTE_ID,
    PER_ID_ASIGNADO,
    ASIGNADO_CUENTA,
    NIVEL
)
AS
  SELECT
    p.id            parte_id,
    per_id_asignado per_id_asignado,
    asignado_cuenta asignado_cuenta,
    1               nivel
  FROM cau_vw_partes p
  WHERE p.estado < 30
        AND NOT exists
  (SELECT
     *
   FROM cau_alertas_partes ap
   WHERE ap.fecha_alerta > p.estado_f_inicio
         AND ap.parte_id = p.id)
        AND (sysdate
             - CASE
               WHEN p.estado_f_inicio - TRUNC(p.estado_f_inicio) >
                    (19 / 24)
               THEN
                 trunc(p.estado_f_inicio + 1) + (07 / 24)
               WHEN p.estado_f_inicio - TRUNC(p.estado_f_inicio) <
                    (07 / 24)
               THEN
                 TRUNC(p.estado_f_inicio) + (07 / 24)
               WHEN p.estado_f_inicio
                    + (SELECT
                         tiempo
                       FROM cau_alertas
                       WHERE prioridad_nombre =
                             nvl(p.id_prioridad, 5)
                             AND nivel = 1)
                    - trunc(p.estado_f_inicio) > (19 / 24)
               THEN
                 p.estado_f_inicio + (12 / 24)
                 + (to_date(
                        to_char(trunc(p.estado_f_inicio),
                                'dd-mm-yyyy')
                        || '19:00:00',
                        'dd-mm-yyyyhh24:mi:ss'
                    )
                    - p.estado_f_inicio)
               ELSE
                 p.estado_f_inicio
               END
             - (SELECT
                  count(*)
                FROM cau_ext_calendario c
                WHERE c.fecha_completa BETWEEN trunc(
                    p.estado_f_inicio
                )
                      AND trunc(sysdate)
                      AND tipo_lab IN ('F', 'N')))
            / (1 / (24 * 60)) >
            (SELECT
               tiempo
             FROM cau_alertas
             WHERE prioridad_nombre = nvl(p.id_prioridad, 5)
                   AND nivel = 1)
  UNION
  SELECT
    p.id,
    per_id_asignado,
    asignado_cuenta,
    2
  FROM cau_vw_partes p
  WHERE p.estado = 30
        AND NOT exists
  (SELECT
     *
   FROM cau_alertas_partes ap
   WHERE ap.fecha_alerta > p.estado_f_inicio
         AND ap.parte_id = p.id)
        AND (sysdate
             - CASE
               WHEN p.estado_f_inicio - TRUNC(p.estado_f_inicio) >
                    (19 / 24)
               THEN
                 trunc(p.estado_f_inicio + 1) + (07 / 24)
               WHEN p.estado_f_inicio - TRUNC(p.estado_f_inicio) <
                    (07 / 24)
               THEN
                 TRUNC(p.estado_f_inicio) + (07 / 24)
               WHEN p.estado_f_inicio
                    + (SELECT
                         tiempo
                       FROM cau_alertas
                       WHERE prioridad_nombre =
                             nvl(p.id_prioridad, 5)
                             AND nivel = 2)
                    - trunc(p.estado_f_inicio) > (19 / 24)
               THEN
                 p.estado_f_inicio + (12 / 24)
                 + (to_date(
                        to_char(trunc(p.estado_f_inicio),
                                'dd-mm-yyyy')
                        || '19:00:00',
                        'dd-mm-yyyyhh24:mi:ss'
                    )
                    - p.estado_f_inicio)
               ELSE
                 p.estado_f_inicio
               END
             - (SELECT
                  count(*)
                FROM cau_ext_calendario c
                WHERE c.fecha_completa BETWEEN trunc(
                    p.estado_f_inicio
                )
                      AND trunc(sysdate)
                      AND tipo_lab IN ('F', 'N')))
            / (1 / (24 * 60)) >
            (SELECT
               tiempo
             FROM cau_alertas
             WHERE prioridad_nombre = nvl(p.id_prioridad, 5)
                   AND nivel = 2)
  UNION
  SELECT
    p.id,
    per_id_asignado,
    asignado_cuenta,
    3
  FROM cau_vw_partes p
  WHERE p.estado > 30 AND p.estado < 60
        AND NOT exists
  (SELECT
     *
   FROM cau_alertas_partes ap
   WHERE ap.fecha_alerta > p.estado_f_inicio
         AND ap.parte_id = p.id)
        AND (sysdate
             - CASE
               WHEN p.estado_f_inicio - TRUNC(p.estado_f_inicio) >
                    (19 / 24)
               THEN
                 trunc(p.estado_f_inicio + 1) + (07 / 24)
               WHEN p.estado_f_inicio - TRUNC(p.estado_f_inicio) <
                    (07 / 24)
               THEN
                 TRUNC(p.estado_f_inicio) + (07 / 24)
               WHEN p.estado_f_inicio
                    + (SELECT
                         tiempo
                       FROM cau_alertas
                       WHERE prioridad_nombre =
                             nvl(p.id_prioridad, 5)
                             AND nivel = 3)
                    - trunc(p.estado_f_inicio) > (19 / 24)
               THEN
                 p.estado_f_inicio + (12 / 24)
                 + (to_date(
                        to_char(trunc(p.estado_f_inicio),
                                'dd-mm-yyyy')
                        || '19:00:00',
                        'dd-mm-yyyyhh24:mi:ss'
                    )
                    - p.estado_f_inicio)
               ELSE
                 p.estado_f_inicio
               END
             - (SELECT
                  count(*)
                FROM cau_ext_calendario c
                WHERE c.fecha_completa BETWEEN trunc(
                    p.estado_f_inicio
                )
                      AND trunc(sysdate)
                      AND tipo_lab IN ('F', 'N')))
            / (1 / (24 * 60)) >
            (SELECT
               tiempo
             FROM cau_alertas
             WHERE prioridad_nombre = nvl(p.id_prioridad, 5)
                   AND nivel = 3);

CREATE OR REPLACE FORCE VIEW UJI_CAU.CAU_VW_PARTES_CDM
(
    ID,
    ASUNTO,
    CODIGO_BARRAS,
    DESCRIPCION,
    PRESENTE,
    LEIDO,
    SUSCRITO,
    EXTENSION_TELEFONO,
    TIPO,
    ID_AREA,
    ID_CATEGORIA,
    PER_ID_ASIGNADO,
    ASIGNADO_NOMBRE_COMPLETO,
    ASIGNADO_CUENTA,
    ASIGNADO_EXTENSION_TELEFONO,
    ASIGNADO_DEPARTAMENTO,
    ASIGNADO_UBICACION,
    FECHA_ASIGNACION,
    FECHA_CREACION,
    PER_ID_CREACION,
    CREADOR_NOMBRE_COMPLETO,
    CREADOR_CUENTA,
    CREADOR_EXTENSION_TELEFONO,
    CREADOR_DEPARTAMENTO,
    CREADOR_UBICACION,
    PER_ID_SOLICITANTE,
    SOLICITANTE_NOMBRE_COMPLETO,
    SOLICITANTE_CUENTA,
    SOLICITANTE_EXTENSION_TELEFONO,
    SOLICITANTE_DEPARTAMENTO,
    SOLICITANTE_UBICACION,
    PER_ID_AFECTADO,
    AFECTADO_NOMBRE_COMPLETO,
    AFECTADO_CUENTA,
    AFECTADO_EXTENSION_TELEFONO,
    AFECTADO_DEPARTAMENTO,
    AFECTADO_UBICACION,
    ID_PRIORIDAD,
    PRIORIDAD_NOMBRE,
    PRIORIDAD_COLOR,
    ID_UBICACION_FISICA,
    UBICACION_NOMBRE,
    ID_IMPACTO,
    ID_URGENCIA,
    PAR_ID_EXTERNO,
    ESTADO,
    ESTADO_NOMBRE,
    ESTADO_PARENT,
    ESTADO_F_INICIO,
    ESTADO_F_FIN
)
AS
  SELECT
    ID,
    ASUNTO,
    CODIGO_BARRAS,
    DESCRIPCION,
    PRESENTE,
    LEIDO,
    SUSCRITO,
    EXTENSION_TELEFONO,
    TIPO,
    ID_AREA,
    ID_CATEGORIA,
    PER_ID_ASIGNADO,
    ASIGNADO_NOMBRE_COMPLETO,
    ASIGNADO_CUENTA,
    ASIGNADO_EXTENSION_TELEFONO,
    ASIGNADO_DEPARTAMENTO,
    ASIGNADO_UBICACION,
    FECHA_ASIGNACION,
    FECHA_CREACION,
    PER_ID_CREACION,
    CREADOR_NOMBRE_COMPLETO,
    CREADOR_CUENTA,
    CREADOR_EXTENSION_TELEFONO,
    CREADOR_DEPARTAMENTO,
    CREADOR_UBICACION,
    PER_ID_SOLICITANTE,
    SOLICITANTE_NOMBRE_COMPLETO,
    SOLICITANTE_CUENTA,
    SOLICITANTE_EXTENSION_TELEFONO,
    SOLICITANTE_DEPARTAMENTO,
    SOLICITANTE_UBICACION,
    PER_ID_AFECTADO,
    AFECTADO_NOMBRE_COMPLETO,
    AFECTADO_CUENTA,
    AFECTADO_EXTENSION_TELEFONO,
    AFECTADO_DEPARTAMENTO,
    AFECTADO_UBICACION,
    ID_PRIORIDAD,
    PRIORIDAD_NOMBRE,
    PRIORIDAD_COLOR,
    ID_UBICACION_FISICA,
    UBICACION_NOMBRE,
    ID_IMPACTO,
    ID_URGENCIA,
    PAR_ID_EXTERNO,
    ESTADO,
    ESTADO_NOMBRE,
    ESTADO_PARENT,
    ESTADO_F_INICIO,
    ESTADO_F_FIN
  FROM CAU_VW_PARTES
  WHERE PER_ID_ASIGNADO IN (3406, 88284, 4756, 148513, 143464);

CREATE SEQUENCE UJI_CAU.HIBERNATE_SEQUENCE;
CREATE SEQUENCE UJI_CAU.PARTES_SEQUENCE;



