CREATE TABLE UJI_CAU.cau_encuestas
(
  id NUMBER PRIMARY KEY NOT NULL,
  parte_id NUMBER,
  persona_id NUMBER,
  atencion NUMBER,
  informacion NUMBER,
  solucion NUMBER,
  tiempo number,
  motivo VARCHAR(1000)

);
CREATE INDEX "cau_encuestas_parte_id_index" ON UJI_CAU.cau_encuestas (parte_id);
CREATE INDEX "cau_encuestas_persona_id_index" ON UJI_CAU.cau_encuestas (persona_id);

ALTER TABLE UJI_CAU.CAU_ENCUESTAS
  ADD CONSTRAINT CAU_ENCUESTAS_CAU_PARTES_ID_fk
FOREIGN KEY (PARTE_ID) REFERENCES CAU_PARTES (ID);

update CAU_TIPOS_ESTADOS_PARTES set id=id*100;
update CAU_TIPOS_ESTADOS_PARTES set ESTADO_PADRE_ID=ESTADO_PADRE_ID*100;
update CAU_PARTES_ESTADOS set ESTADO_ID=ESTADO_ID*100;

CREATE OR REPLACE FORCE VIEW "UJI_CAU"."CAU_VW_PARTES" ("ID", "ASUNTO", "CODIGO_BARRAS", "DESCRIPCION", "PRESENTE", "LEIDO", "SUSCRITO", "EXTENSION_TELEFONO", "TIPO", "ID_AREA", "ID_CATEGORIA", "PER_ID_ASIGNADO", "ASIGNADO_NOMBRE_COMPLETO", "ASIGNADO_CUENTA", "ASIGNADO_EXTENSION_TELEFONO", "ASIGNADO_DEPARTAMENTO", "ASIGNADO_UBICACION", "FECHA_ASIGNACION", "FECHA_CREACION", "PER_ID_CREACION", "CREADOR_NOMBRE_COMPLETO", "CREADOR_CUENTA", "CREADOR_EXTENSION_TELEFONO", "CREADOR_DEPARTAMENTO", "CREADOR_UBICACION", "PER_ID_SOLICITANTE", "SOLICITANTE_NOMBRE_COMPLETO", "SOLICITANTE_CUENTA", "SOLICITANTE_EXTENSION_TELEFONO", "SOLICITANTE_DEPARTAMENTO", "SOLICITANTE_UBICACION", "PER_ID_AFECTADO", "AFECTADO_NOMBRE_COMPLETO", "AFECTADO_CUENTA", "AFECTADO_EXTENSION_TELEFONO", "AFECTADO_DEPARTAMENTO", "AFECTADO_UBICACION", "ID_PRIORIDAD", "PRIORIDAD_NOMBRE", "PRIORIDAD_COLOR", "ID_UBICACION_FISICA", "UBICACION_NOMBRE", "ID_IMPACTO", "ID_URGENCIA", "PAR_ID_EXTERNO", "ESTADO", "ESTADO_NOMBRE", "ESTADO_PARENT", "ESTADO_F_INICIO", "ESTADO_F_FIN", "ESTADO_DESCRIPCION", "IP_INVENTARIO", "TEXTO_RESOLUCION", "HAS_ATTACHMENT") AS
  SELECT   p.id,
            p.asunto,
            p.codigo_barras,
            p.descripcion,
            p.presente,
            p.leido,
            p.suscrito,
            p.extension_telefono,
            p.tipo,
            p.ID_AREA,
            p.ID_CATEGORIA,
            p.per_id_asignado,
            perAsig.NOMBRE_COMPLETO asignado_nombre_completo,
            perAsig.cuenta asignado_cuenta,
            perAsig.EXTENSION_TELEFONO asignado_extension_telefono,
            (SELECT   NOMBRE_UBICACION
               FROM   CAU_EXT_UBICACIONES_FISICAS
              WHERE   id = perAsig.ID_UBICACION_FISICA)
               asignado_departamento,
            (SELECT   NOMBRE
               FROM   CAU_EXT_UBICACIONES_FISICAS
              WHERE   id = perAsig.ID_UBICACION_FISICA)
               asignado_ubicacion,
            (SELECT   MAX (f_inicio)
               FROM   CAU_PARTES_ESTADOS
              WHERE   par_id = p.id AND estado_id = 3000)
               fecha_asignacion,
            (SELECT   MIN (f_inicio)
               FROM   CAU_PARTES_ESTADOS
              WHERE   par_id = p.id AND estado_id = 1000)
               fecha_creacion,
            p.PER_ID_CREACION,
            perCrea.NOMBRE_COMPLETO creador_nombre_completo,
            perCrea.cuenta creador_cuenta,
            perCrea.EXTENSION_TELEFONO creador_extension_telefono,
            ubiCrea.NOMBRE_UBICACION creador_departamento,
            ubiCrea.NOMBRE creador_ubicacion,
            p.per_id_solicitante,
            perSoli.NOMBRE_COMPLETO solicitante_NOMBRE_COMPLETO,
            perSoli.cuenta solicitante_cuenta,
            perSoli.EXTENSION_TELEFONO solicitante_extension_telefono,
            ubiSoli.NOMBRE_UBICACION solicitante_departamento,
            ubiSoli.NOMBRE solicitante_ubicacion,
            p.per_id_afectado,
            perAfec.NOMBRE_COMPLETO afectado_NOMBRE_COMPLETO,
            perAfec.cuenta afectado_cuenta,
            perAfec.EXTENSION_TELEFONO afectado_EXTENSION_TELEFONO,
            ubiAfec.NOMBRE_UBICACION afectado_departamento,
            ubiAfec.NOMBRE afectado_ubicacion,
            p.ID_PRIORIDAD,
            prio.nombre prioridad_nombre,
            prio.color prioridad_color,
            p.ID_UBICACION_FISICA,
            ubi.nombre ubicacion_nombre,
            p.ID_IMPACTO,
            p.ID_URGENCIA,
            p.par_id_externo,
            tipoEst.id estado,
            tipoEst.nombre estado_nombre,
            tipoEst.ESTADO_PADRE_ID estado_parent,
            est.f_inicio estado_f_inicio,
            est.f_fin estado_f_fin,
            est.descripcion,
            (select i.IP_EQUIPO from CAU_EXT_ELEMENTOS_INVENTARIOS i where i.CODIGO_BARRAS=p.CODIGO_BARRAS and i.IP_EQUIPO is not null) ip_inventario,
            (SELECT DESCRIPCION
FROM   CAU_PARTES_ESTADOS
WHERE   par_id = p.id
        AND estado_id = 6000 and F_INICIO =(select max(F_INICIO) from CAU_PARTES_ESTADOS where PAR_ID=p.id AND estado_id = 6000)) texto_resolucion,
            (SELECT count(*) FROM CAU_PARTES_FICHEROS WHERE par_id = p.id) hasAttachment
     FROM   CAU_PARTES p,
            CAU_VMC_PERSONAS perAsig,
            CAU_VMC_PERSONAS perCrea,
            CAU_VMC_PERSONAS perSoli,
            CAU_VMC_PERSONAS perAfec,
            CAU_TIPOS_PRIORIDADES prio,
            CAU_EXT_UBICACIONES_FISICAS ubi,
            CAU_PARTES_ESTADOS est,
            CAU_TIPOS_ESTADOS_PARTES tipoEst,
            CAU_EXT_UBICACIONES_FISICAS ubiAfec,
            CAU_EXT_UBICACIONES_FISICAS ubiCrea,
            CAU_EXT_UBICACIONES_FISICAS ubiSoli
    WHERE       p.PER_ID_ASIGNADO = perAsig.id(+)
            AND p.PER_ID_CREACION = perCrea.id
            AND p.PER_ID_SOLICITANTE = perSoli.id
            AND p.PER_ID_AFECTADO = perAfec.id
            AND p.ID_PRIORIDAD = prio.id(+)
            AND p.ID_UBICACION_FISICA = ubi.id(+)
            AND p.ID = est.par_id
            AND est.ACTUAL = 1
            AND est.estado_id = tipoEst.id
            AND perAfec.id_ubicacion_fisica = ubiAfec.id(+)
            AND perCrea.id_ubicacion_fisica = ubiCrea.id(+)
            AND perSoli.id_ubicacion_fisica = ubiSoli.id(+)
 ;

 CREATE OR REPLACE FORCE VIEW "UJI_CAU"."CAU_VW_PARTES_ALERTAS" ("PARTE_ID", "PER_ID_ASIGNADO", "ASIGNADO_CUENTA", "NIVEL") AS
  SELECT   p.id parte_id,
         per_id_asignado per_id_asignado,
         asignado_cuenta asignado_cuenta,
         1 nivel
  FROM   cau_vw_partes p
 WHERE   p.estado < 3000
         AND NOT EXISTS
               (SELECT   *
                  FROM   cau_alertas_partes ap
                 WHERE   ap.fecha_alerta > p.estado_f_inicio
                         AND ap.parte_id = p.id)
         AND CASE
               WHEN p.estado_f_inicio - TRUNC (p.estado_f_inicio) > (19 / 24)
               THEN
                  TRUNC (p.estado_f_inicio + 1) + (07 / 24)
               WHEN p.estado_f_inicio - TRUNC (p.estado_f_inicio) < (07 / 24)
               THEN
                  TRUNC (p.estado_f_inicio) + (07 / 24)
               ELSE
                  p.estado_f_inicio
            END
            + (SELECT   COUNT ( * )
                 FROM   cau_ext_calendario c
                WHERE   c.fecha_completa BETWEEN TRUNC (p.estado_f_inicio)
                                             AND  TRUNC (sysdate)
                        AND tipo_lab IN ('F', 'N'))
            - nvl (
                 ( (select   to_char (p.estado_f_inicio, 'hh24') / 24
                      from   cau_ext_calendario c
                     where   to_char (p.estado_f_inicio, 'dd/mm/yyyy') =
                                c.fecha_completa
                             and tipo_lab in ('F', 'N'))+ (7 / 24)),
                 0
              )

            + (( (SELECT   COUNT ( * )
                   FROM   cau_ext_calendario c
                  WHERE   c.fecha_completa BETWEEN TRUNC (p.estado_f_inicio)
                                               AND  TRUNC (sysdate)
                          AND tipo_lab NOT IN ('F', 'N'))
               - 1)
              * (12 / 24))
            + ((SELECT   tiempo
                 FROM   cau_alertas
                WHERE   prioridad_nombre = NVL (p.prioridad_nombre, 5)
                        AND nivel = 1)
              / (24 * 60)) < sysdate
UNION
SELECT   p.id,
         per_id_asignado,
         asignado_cuenta,
         2
  FROM   cau_vw_partes p
 WHERE   p.estado = 3000
         AND NOT EXISTS
               (SELECT   *
                  FROM   cau_alertas_partes ap
                 WHERE   ap.fecha_alerta > p.estado_f_inicio
                         AND ap.parte_id = p.id)
         AND CASE
               WHEN p.estado_f_inicio - TRUNC (p.estado_f_inicio) > (19 / 24)
               THEN
                  TRUNC (p.estado_f_inicio + 1) + (07 / 24)
               WHEN p.estado_f_inicio - TRUNC (p.estado_f_inicio) < (07 / 24)
               THEN
                  TRUNC (p.estado_f_inicio) + (07 / 24)
               ELSE
                  p.estado_f_inicio
            END
            + (SELECT   COUNT ( * )
                 FROM   cau_ext_calendario c
                WHERE   c.fecha_completa BETWEEN TRUNC (p.estado_f_inicio)
                                             AND  TRUNC (sysdate)
                        AND tipo_lab IN ('F', 'N'))
            - nvl (
                 ( (select   to_char (p.estado_f_inicio, 'hh24') / 24
                      from   cau_ext_calendario c
                     where   to_char (p.estado_f_inicio, 'dd/mm/yyyy') =
                                c.fecha_completa
                             and tipo_lab in ('F', 'N'))  + (7 / 24)),
                 0
              )

            + (( (SELECT   COUNT ( * )
                   FROM   cau_ext_calendario c
                  WHERE   c.fecha_completa BETWEEN TRUNC (p.estado_f_inicio)
                                               AND  TRUNC (sysdate)
                          AND tipo_lab NOT IN ('F', 'N'))
               - 1)
              * (12 / 24))
            + ((SELECT   tiempo
                 FROM   cau_alertas
                WHERE   prioridad_nombre = NVL (p.prioridad_nombre, 5)
                        AND nivel = 2)
              / (24 * 60)) < sysdate
UNION
SELECT   p.id,
         per_id_asignado,
         asignado_cuenta,
         3
  FROM   cau_vw_partes p
 WHERE   p.estado > 3000 AND p.estado < 5000
         AND NOT EXISTS
               (SELECT   *
                  FROM   cau_alertas_partes ap
                 WHERE   ap.fecha_alerta > p.estado_f_inicio
                         AND ap.parte_id = p.id)
         AND CASE
               WHEN p.estado_f_inicio - TRUNC (p.estado_f_inicio) > (19 / 24)
               THEN
                  TRUNC (p.estado_f_inicio + 1) + (07 / 24)
               WHEN p.estado_f_inicio - TRUNC (p.estado_f_inicio) < (07 / 24)
               THEN
                  TRUNC (p.estado_f_inicio) + (07 / 24)
               ELSE
                  p.estado_f_inicio
            END
            + (SELECT   COUNT ( * )
                 FROM   cau_ext_calendario c
                WHERE   c.fecha_completa BETWEEN TRUNC (p.estado_f_inicio)
                                             AND  TRUNC (sysdate)
                        AND tipo_lab IN ('F', 'N'))
            - nvl (
                 ( (select   to_char (p.estado_f_inicio, 'hh24') / 24
                      from   cau_ext_calendario c
                     where   to_char (p.estado_f_inicio, 'dd/mm/yyyy') =
                                c.fecha_completa
                             and tipo_lab in ('F', 'N'))+ (7 / 24)),
                 0
              )

            + ( ( (SELECT   COUNT ( * )
                     FROM   cau_ext_calendario c
                    WHERE   c.fecha_completa BETWEEN TRUNC (
                                                        p.estado_f_inicio
                                                     )
                                                 AND  TRUNC (sysdate)
                            AND tipo_lab NOT IN ('F', 'N'))
                 - 1)
               * (12 / 24))
            + ( (SELECT   tiempo
                   FROM   cau_alertas
                  WHERE   prioridad_nombre = NVL (p.prioridad_nombre, 5)
                          AND nivel = 3)
               / (24 * 60)) < sysdate
 ;

 CREATE TABLE UJI_CAU.CAU_TAGS
(
    id NUMBER PRIMARY KEY NOT NULL,
    tag VARCHAR2(255) NOT NULL
);
CREATE TABLE UJI_CAU.CAU_PARTES_TAGS
(
    id NUMBER PRIMARY KEY NOT NULL,
    par_id NUMBER NOT NULL,
    tag_id NUMBER NOT NULL,
    CONSTRAINT CAU_TAGS_PARTES_ID_fk FOREIGN KEY (par_id) REFERENCES CAU_PARTES (ID),
    CONSTRAINT CAU_TAGS_CAU_TAGS_ID_fk FOREIGN KEY (tag_id) REFERENCES CAU_TAGS (ID)
);


ALTER TABLE UJI_CAU.CAU_PARTES ADD notifica_tecnico INT DEFAULT 1 NULL;