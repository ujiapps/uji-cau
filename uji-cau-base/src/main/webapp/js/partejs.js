$(document).ready(
    function () {
        var parteId = $('#parteId').text();
        var userId = $('#userId').text();
        var afectadoId = $('#afectadoId').text();
        var solicitanteId = $('#solicitanteId').text();
        var estado = $('#parteEstado').text();
        var rechazarMotivo = $('#rechazarMotivo');
        if (afectadoId != userId && afectadoId != solicitanteId) {
            $('#rechazarParte').hide();
            $('#cerrarParte').hide();
            $('#enviarComunicacion').hide();
            $('#chatComunicacion').hide();
            $('#formUploadfile').hide();
        } else {
            if (estado < 6000) {
                $('#rechazarParte').hide();
                $('#cerrarParte').hide();
            } else if (estado == 7000) {
                $('#rechazarParte').hide();
                $('#cerrarParte').hide();
                $('#enviarComunicacion').hide();
                $('#chatComunicacion').hide();
                $("#upload-file").hide();
                $('#formUploadfile').hide();
            } else {
                $('#enviarComunicacion').hide();
                $('#chatComunicacion').hide();
            }
        }
        $.ajaxSetup({cache: false});

        function loadChat(parteId, userId) {
            $.getJSON('/cau/rest/parte/public/' + parteId + '/comunicaciones', function (res) {
                updateChat(userId, res);
            });
        }

        loadChat(parteId, userId);
        $.getJSON('/cau/rest/parte/' + parteId + '/ficheros', function (res) {
            $('#archivoTexto').text($('#archivoTexto').text().replace(/\((.*)\)/,'('+ res.data.length+')'));
            for (var i = 0; i < res.data.length; i++) {
                var item = res.data[i];
                $('#upload-list').append(
                    '<li><input type="button" class="download-button"/><span class="ml16"><a href="/cau/rest/parte/' + parteId + '/ficheros/' + item.id + '/descargar">' + item.nombreFichero
                    + '</a></span></li>');
            }
            $('#progressBar').width(0);
        });

        $("#upload-file-parte").change(
            function () {
                var obj = $(this);
                // if(this.files[0].size>30000000){
                //     alert("Tamany maxim de fitxer 30 Megues")
                //     return
                // }
                $(obj).css(
                    {
                        'display': 'none'
                    }).parent().append('<input type="file" class="upload-file-parte" style="display: none; name="' + obj.attr('name') + '" placeholder="Seleccione archivo"/>')


                var formData = new FormData($('#formUploadfile')[0]);
                $(".spinner").show();
                $.ajax(
                    {
                        url: '/cau/rest/parte/' + parteId + '/ficheros',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = (evt.loaded / evt.total) * 100;
                                    $('#progressBar').css('width', percentComplete + '%');
                                    if (percentComplete == 100) {
                                        $('#progressBar').css('display', 'none');
                                    }
                                }
                            }, false);

                            xhr.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                }
                            }, false);

                            return xhr;
                        },
                    }).done(function (res) {
                    var json = JSON.parse(res);
                    $(".spinner").hide();
                    $('#upload-list').append(
                        '<li><input type="button" class="download-button"/><span class="ml16"><a href="/cau/rest/parte/' + parteId + '/ficheros/' + json.data.id + '/descargar">'
                        + json.data.nombreFichero + '</a></span></li>');
                });

            });
        $('p[name=suscrito]').click(function () {
            var sus = $("p[name=suscrito]");
            var val = sus.attr("value");
            if (val) {
                var param = (val == "1") ? 0 : 1;
                $.ajax(
                    {
                        type: 'PUT',
                        url: '/cau/rest/parte/public/' + parteId + '/suscribir',
                        dataType: 'json',
                        data: "suscrito=" + param,
                        success: function (res) {
                            if (param == "0") {
                                sus.text($("#textoNoRecibir").text());
                                sus.attr('value', 0);
                                sus.removeClass("icon-eye").addClass("icon-eye-off");
                            } else {
                                sus.text($("#textoRecibir").text());
                                sus.attr('value', 1);
                                sus.removeClass("icon-eye-off").addClass("icon-eye");
                            }
                        }
                    });
            }
        });
        $('#enviarComunicacion').click(function () {
            var desc = $("#chatComunicacion").val();
            if (desc.length > 0) {
                $('#enviarComunicacion').prop("disabled", true);
                $.ajax(
                    {
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: '/cau/rest/parte/' + parteId + '/comunicaciones',
                        dataType: 'json',
                        data: JSON.stringify(
                            {
                                id: null,
                                parteId: parteId,
                                descripcion: desc,
                                tipo: 0,
                                personaCreacionId: userId
                            }),
                        success: function (res) {
                            if (res.success) {
                                $("#chatComunicacion").val("");
                                res.data = [res.data];
                                loadChat(parteId, userId);
                                $('#enviarComunicacion').prop("disabled", false);
                            } else {
                                alert(res.message);
                            }
                        }
                    });
            }
        });

        function mostrarRechazar() {
            $('#rechazarDiag').dialog(
                {
                    resizable: true,
                    width: 350,
                    height: 'auto',
                    modal: true,
                    buttons: {
                        "Reobrir incidéncia": function () {
                            if (rechazarMotivo.val().length > 0) {
                                $(":button:contains('Reobrir incidéncia')").prop("disabled", true).addClass("ui-state-disabled");
                                $.ajax(
                                    {
                                        type: 'POST',
                                        contentType: 'application/json; charset=utf-8',
                                        url: '/cau/rest/parte/public/' + parteId + '/rechazar',
                                        dataType: 'json',
                                        context: $(this),
                                        data: JSON.stringify({rechazarMotivo: $('#rechazarMotivo').val()}),
                                        complete: function (res) {
                                            $(this).dialog("close");
                                            window.location.replace("/cau/rest/parte/public/listado");
                                        }
                                    });
                            } else {
                                rechazarMotivo.addClass("ui-state-error");
                                setTimeout(function () {
                                    rechazarMotivo.removeClass("ui-state-error", 1500);
                                }, 500);
                            }


                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    },
                    close: function () {
                        rechazarMotivo.val("");
                    }
                });
        };

        $('#rechazarParte').click(function () {
            mostrarRechazar();

        });

        function mostrarCerrar() {
            $('#cerrarDiag').dialog(
                {
                    resizable: false,
                    height: 'auto',
                    width: 450,
                    modal: true,
                    buttons: {
                        "Tancar incidència": function () {
                            $(":button:contains('Tancar incidència')").prop("disabled", true).addClass("ui-state-disabled");
                            $.ajax(
                                {
                                    type: 'POST',
                                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                                    url: '/cau/rest/parte/public/' + parteId + '/cerrar',
                                    dataType: 'json',
                                    context: $(this),
                                    data: $('#cerrarForm').serialize(),
                                    complete: function (data) {
                                        $('#cerrarDiag').html("<p>Gràcies per utilitzar els nostres serveis</p>");
                                        $('#cerrarDiag').dialog(
                                            {
                                                dialogClass: 'no-close',
                                                closeOnEscape: true,
                                                resizable: false,
                                                height: 170,
                                                modal: true,
                                                title: "",
                                                buttons: {
                                                    "Aceptar": function () {
                                                        window.location.replace("/cau/rest/parte/public/listado");
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            });
                                    }
                                });

                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });
        };
        $('#cerrarParte').click(function () {
            mostrarCerrar();
        });

        function updateChat(userId, res) {
            $('#comunicacion').show();
            var r = '';
            for (var i = 0; i < res.data.length; i++) {

                var item = res.data[i];
                if (item.interno == 3) {
                    r += '<div class="chat chat-comunicado';
                } else {
                    r += '<div class="chat';
                }
                if (item.personaCreacionId == userId) {
                    r += ' me">' + $('<div/>').text(item.descripcion).html();
                } else {
                    r += ' you">' + $('<div/>').text(item.descripcion).html();

                }
                if (typeof item.fechaCreacion !== 'undefined') {
                    r += '<footer>' + item.fechaCreacion + ' por ' + item.nombreCompleto + '</footer>';
                } else {
                    r += '<footer>' + formatUnixTimestamp(item.fecha) + ' por ' + item.nombreCompleto + '</footer>';
                }
                r += '</div><div class="clear"></div>';
            }
            $("#comunicacion").append(r);
            $("#comunicacion").scrollTop($("#comunicacion")[0].scrollHeight);
            if (res.data.length == 0)
                $("#comunicacion").hide();
        }

        function formatUnixTimestamp(ts) {
            if (ts < 0)
                ts = 0;
            if (ts == 0)
                return '???';
            //ts = ts / 1000;
            var dt = new Date(ts * 1000);
            var d = dt.getDate();
            if (d < 10)
                d = '0' + d;
            var m = dt.getMonth() + 1;
            if (m < 10)
                m = '0' + m;
            var hh = dt.getHours();
            if (hh < 10)
                hh = '0' + hh;
            var mm = dt.getMinutes();
            if (mm < 10)
                mm = '0' + mm;
            var ss = dt.getSeconds();
            if (ss < 10)
                ss = '0' + ss;
            return d + '/' + m + '/' + dt.getFullYear() + ' ' + hh + ':' + mm + ':' + ss;
        }

        var accion = window.location.hash.split('#')[1];
        if (estado >= 6000 && estado < 7000) {
            if (accion == "tancar") {
                mostrarCerrar();
            } else if (accion == "reobrir") {
                mostrarRechazar();
            }
        }

        $('#albaran').click(function () {
            window.open('/cau/rest/parte/' + parteId + '/albaran', '_blank');
        });
    });
