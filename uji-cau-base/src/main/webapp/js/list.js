$(document).ready(function ()
{
    $("#estados").select2();
    $('#formsearch').hide();

    $('#mas').click(function ()
    {
        $('#formsearch').slideToggle();
    });
    $('#datepickerIni').datepicker();
    $('#datepickerFin').datepicker();

    updateSearchByParams();
    function updateSearchByParams() {
        var params = decodeURI(
            (RegExp('filter=(.*?)(&|$)').exec(location.search) || [, null])[1]
        );
        var filters = [];
        if (params) filters = $.parseJSON(params);

        var estados=[];
        for (i in filters) {
            var p = filters[i].property;
            var v = filters[i].value;

            if (p == 'fechaInicio') {
                $('#datepickerIni').datepicker('setDate', v);
            } else if (p == 'fechaFin') {
                $('#datepickerFin').datepicker('setDate', v);
            } else if (p == 'descripcion') {
                $('#searchInput').val(v);
                $('#descripcion').val(v);
            } else if (p == 'estado'){
                estados.push(v);
            }
        }
        $('#estados').val(estados).trigger("change");
    }
    $('#buscar').click(function (e)
    {
        e.preventDefault();
        var data = [];
        var ini = $('#datepickerIni').val();
        var fin = $('#datepickerFin').val();
        var adj = $('#adjunto').val();
        var estados = $('#estados').val();
        var desc= $('#descripcion').val();
        if (ini)
        {
            data.push({"property": "fechaInicio", "value": ini });
        }
        if (fin)
        {
            data.push({"property": "fechaFin", "value": fin});
        }
        if (adj)
        {
            data.push({"property": "adjuntos", "value": 1});
        }
        if (desc)
        {
            data.push({"property": "descripcion", "value": desc});
        }
        if (estados)
        {
            for(var i in estados)
            {
                data.push({"property": 'estado', "value": estados[i]});
            }
        }

        window.location = '/cau/rest/parte/public/listado/?' + 'filter=' + encodeURI(JSON.stringify(data));

    });

    $('#listado-incidencias tr').click(function ()
    {
        var href = $(this).find("a").attr("href");
        if (href)
        {
            window.location = href;
        }
    });
});