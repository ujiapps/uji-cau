$(document).ready(function () {
    var r = function () {
        var obj = $(this);
        obj.removeAttr('id');
        obj.parent().append('<input type="file" class="upload-file" style="display:none" id="upload-file" name="datafile"/>');
        $('#upload-list').append('<li><input type="button" class="upload-file-cancel"/><span class="ml16">' + obj[0].files[0].name + '</span></li>');
        $(".upload-file").change(r);
        $(".upload-file-cancel").click(function () {
            $(this).parent().remove();
            $(obj).remove();
        });
    };
    $('#adj').click(function(){
        $('#upload-file').trigger('click');
    });
    $("#upload-file").change(r);

    var form = $('#newParteForm');
    var cb = $('#cb');
    var cbInfo = $('#cbInfo');
    var asunto = $('#asunto');
    var asuntoInfo = $('#asuntoInfo');
    var descrip = $('#descrip');
    var descripInfo = $('#descripInfo');
    function do_nothing() {
        console.log("click prevented");
        return false;
    }

    cb.blur(validateCB);
    asunto.blur(validateAsunto);
    asunto.keyup(validateAsunto);
    cb.keyup(validateCB);
    descrip.keyup(validateDescrip);
    $("input:submit").click(function () {
        var $button = this;
        var oldValue = $button.value;
        setTimeout(function() {
            $button.disabled = true;
            $button.value = '...';
            setTimeout(function() {
                $button.disabled = false;
                $button.value = oldValue;
            }, 1000);
        }, 0);
    });
    $(form).keypress(function (e) {
        var charCode = e.charCode || e.keyCode || e.which;
        if (charCode == 13 && e.target.name != 'descrip') {
            return false;
        }
    });

    form.submit(function (e) {
        if (validateDescrip() & validateCB() & validateAsunto()) {
            return true;
        }
        else
            return false;
    });

    var $oldSol = $('#solicitante').val();
    $('#results').hide();
    $('#div-afectado').hide();

    $("#afectado").select2({
        placeholder: "Search",
        minimumInputLength: 3,
        ajax: {
            url: "/cau/rest/persona/search",
            dataType: 'json',
            data: function (term, page) {
                return {
                    query: term,
                    limit: 15
                };
            },
            results: function (data, page) {
                return {results: data.data};
            }
        },
        formatResult: formatSearchPersona,
        formatSelection: formatSelectPersona,
        escapeMarkup: function (m) {
            return m;
        }
    });

    $('#afectadoId').click(function () {
        if ($(this).is(':checked')) {
            $('#div-afectado').show();
            $('#afectado').val('');
            $('#afectado').attr('disabled', false);
        }
        else {
            $('#div-afectado').hide();
            $('#afectado').val($oldSol);
            $('#afectado').attr('disabled', true);
        }
    });

    function formatSearchPersona(data) {
        markut = '<li id="res-' + data.id + '" class="persona-item">' + data.nombreCompleto;
        if (data.cuenta !== undefined) {
            markut += ' - ' + data.cuenta + '</li>';
        } else {
            markut += '</li>';
        }
        return markut;
    }

    function formatSelectPersona(data) {
        $('#afectadoId').attr('value', data.id);
        return data.nombreCompleto
    }

    function searchCB() {
        var s = cb.val();
        $('#cbDesc').show();
        $.get('/cau/rest/inventario/' + s, function (data) {
            if (typeof data.data.descripcion === "undefined") {
                res = '<div>Codi no catalogat</div>';
                cb.val('');
            }
            else {
                res = '<span>' + data.data.descripcion + '</span>';
            }
            $('#cbDesc').html(res);
        })
    }

    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    function validateCB(e) {
        if (e && e.keyCode != 13 && e.type != "blur") {
            return
        }
        var v = cb.val();
        if (v.length == 0) {
            cbInfo.hide();
            cb.removeClass('error');
            $('#cbDesc').hide();
            return true;
        }
        if (v.length < 12) {
            v = '2' + pad(v, 11);
        }
        if (isValidCodigoBarras(v)) {
            cbInfo.hide();
            cb.removeClass('error');
            cb.val(v);
            searchCB();
            return true;
        }
        else {
            cb.addClass('error');
            cbInfo.addClass('error');
            cbInfo.show();
            return false;
        }
    }

    function validateDescrip() {
        if (descrip.val().length == 0) {
            descripInfo.addClass('error');
            descripInfo.show();
            descrip.addClass('error');
            return false;
        }
        else {
            descripInfo.hide();
            descrip.removeClass('error');
            return true;
        }
    }

    function validateAsunto() {
        if (asunto.val().length == 0) {
            asuntoInfo.addClass('error');
            asuntoInfo.show();
            asunto.addClass('error');
            return false;
        }
        {
            asuntoInfo.hide();
            asunto.removeClass('error');
            return true;
        }
    }


    function isValidCodigoBarras(cb) {
        if (cb == null || cb.length != 12)
            return false;

        var sum = 0;
        for (var c = cb.length - 2; c >= 0; c--)
            sum += parseInt(cb.charAt(c)) * (((c + 1) % 2 == 0) ? 1 : 3);
        var dif = 10 - (sum % 10);
        if (dif == 10)
            dif = 0;
        if (dif != parseInt(cb.charAt(cb.length - 1)))
            return false;

        return true;
    }

});
