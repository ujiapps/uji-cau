package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cau.dao.MensajesDAO;
import es.uji.apps.cau.dao.TecnicoAreaDAO;
import es.uji.apps.cau.model.Mensaje;
import es.uji.apps.cau.utils.Perfiles;
import es.uji.apps.cau.utils.TipoMensaje;

@Service
public class MensajesService {

    @Autowired
    private MensajesDAO mensajesDAO;

    @Autowired
    private TecnicoAreaDAO tecnicoAreaDAO;

    public MensajesService(MensajesDAO mensajesDAO) {
        this.mensajesDAO = mensajesDAO;
    }

    public List<Mensaje> getMensajes(Long userId) {
        if(tecnicoAreaDAO.getPerfiles(userId).stream().noneMatch(s -> s.contains(Perfiles.ADMINISTRADOR.toString()) || s.contains(Perfiles.PRIMERA.toString()))){
        return mensajesDAO.getMensajesByUserId(userId) ;
    }
        return mensajesDAO.getMensajesAdmin(userId);
    }

    public Mensaje getMensaje(Long id) {
        return mensajesDAO.getMensaje(id);
    }

    public Mensaje addMensaje(String msg, TipoMensaje tipo, Long userId) {
        Mensaje mensaje = new Mensaje();
        mensaje.setTipo(tipo);
        mensaje.setMensaje(msg);
        if (tipo.equals(TipoMensaje.PERSONAL))
        {
            mensaje.setPersonaId(userId);
        } else if (tecnicoAreaDAO.getPerfiles(userId).stream().noneMatch(s -> s.contains(Perfiles.ADMINISTRADOR.toString()) || s.contains(Perfiles.PRIMERA.toString())))
        {
            mensaje.setTipo(TipoMensaje.PERSONAL);
            mensaje.setPersonaId(userId);
        }
        return mensajesDAO.insert(mensaje);
    }

    public void deleteAlerta(Long id) {
        mensajesDAO.delete(Mensaje.class, id);
    }

    public Mensaje updateMensaje(Long id, String msj, TipoMensaje tipo, Long userId) {

        if (tecnicoAreaDAO.getPerfiles(userId).stream().noneMatch(s -> s.contains(Perfiles.ADMINISTRADOR.toString()) || s.contains(Perfiles.PRIMERA.toString())))
        {
            Mensaje mensaje = mensajesDAO.getMensajeByUserId(id, userId);
            mensaje.setMensaje(msj);
            return mensajesDAO.update(mensaje);
        } else
        {
            Mensaje mensaje = mensajesDAO.getMensaje(id);
            mensaje.setMensaje(msj);
            mensaje.setTipo(tipo);
            return mensajesDAO.update(mensaje);
        }
    }

    public List<Mensaje> getListadoMensajes(Long userId) {
        if (tecnicoAreaDAO.getPerfiles(userId).stream().noneMatch(s -> s.contains(Perfiles.ADMINISTRADOR.toString()) || s.contains(Perfiles.PRIMERA.toString()))){
            return mensajesDAO.getListadosMensajesTecnico(userId);
        }
        return mensajesDAO.getMensajesAdmin(userId);
    }
}