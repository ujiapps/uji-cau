package es.uji.apps.cau.utils;

public enum Estados
{
    NUEVO(1000), ENCOLADO(2000), DEVUELTO(2100), ASIGNADO(3000), RECHAZADO(3100), ENPROGRESO(4000), PENDIENTE(5000), RESUELTO(6000), CERRADO(7000);

    private final Long value;

    private Estados(Integer value)
    {
        this.value = Long.valueOf(value);
    }

    public static Integer fromString(String estado)
    {
        if (estado.equalsIgnoreCase("NUEVO"))
        {
            return 1000;
        } else if (estado.equalsIgnoreCase("ENCOLADO"))
        {
            return 2000;
        } else if (estado.equalsIgnoreCase("ASIGNADO"))
        {
            return 3000;
        } else if (estado.equalsIgnoreCase("ENPROGRESO"))
        {
            return 4000;
        } else if (estado.equalsIgnoreCase("PENDIENTE"))
        {
            return 5000;
        } else if (estado.equalsIgnoreCase("RESUELTO"))
        {
            return 6000;
        } else if (estado.equalsIgnoreCase("CERRADO"))
        {
            return 7000;
        }
        throw new IllegalArgumentException("No valid value: " + estado);

    }

    public Long getValue()
    {
        return this.value;
    }

    @Override
    public String toString()
    {
        return String.valueOf(value);
    }
}
