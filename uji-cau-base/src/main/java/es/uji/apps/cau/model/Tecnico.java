package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CAU_TECNICO")
public class Tecnico implements Serializable {

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "F_INICIO_ACTIVIDAD")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicioActividad;

    @Column(name = "F_FIN_ACTIVIDAD")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinActividad;

    private String empresa;

    @Id
    @ManyToOne
    @JoinColumn(name = "PER_ID_TECNICO")
    private Persona tecnico;

    public Tecnico() {
    }

       public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFechaInicioActividad() {
        return fechaInicioActividad;
    }

    public void setFechaInicioActividad(Date fechaInicioActividad) {
        this.fechaInicioActividad = fechaInicioActividad;
    }

    public Date getFechaFinActividad() {
        return fechaFinActividad;
    }

    public void setFechaFinActividad(Date fechaFinActividad) {
        this.fechaFinActividad = fechaFinActividad;
    }

    public Persona getTecnico() {
        return this.tecnico;
    }

    public void setTecnico(Persona cauPersona) {
        this.tecnico = cauPersona;
    }

    public String getEmpresa()
    {
        return empresa;
    }

    public void setEmpresa(String empresa)
    {
        this.empresa = empresa;
    }
}
