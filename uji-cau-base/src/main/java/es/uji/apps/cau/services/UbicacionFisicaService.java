package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cau.dao.UbicacionFisicaDAO;
import es.uji.apps.cau.model.UbicacionFisica;

@Service
public class UbicacionFisicaService {
    @Autowired
    private UbicacionFisicaDAO ubicacionFisicaDAO;

    public UbicacionFisica getExtUbicacionFisica(String ubicacion) {
        return ubicacionFisicaDAO.getExtUbicacionFisica(ubicacion);
    }

    public List<UbicacionFisica> searchExtUbicacionFisica(String search) {
        return ubicacionFisicaDAO.searchExtUbicacionFisica(search);
    }

    public UbicacionFisica getExtUbicacionFisicaById(Long ubicacionId) {
        return ubicacionFisicaDAO.getExtUbicacionFisicaById(ubicacionId);
    }

    public List<UbicacionFisica> getExtUbicacionFisica() {
        return ubicacionFisicaDAO.getExtUbicacionFisica();

    }

    public List<UbicacionFisica> getUbicacionFisicaByperId(Long perId) {
        return ubicacionFisicaDAO.getUbicacionFisicaByperId(perId);
    }
}
