package es.uji.apps.cau.services.rest;


import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.services.TagService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tag")
public class TagResource extends CoreBaseService
{

    @InjectParam
    private TagService tagService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTag(@QueryParam("search") String query)
    {
        return UIEntity.toUI(tagService.getAll(query));
    }
}