package es.uji.apps.cau.services;
import es.uji.apps.cau.dao.HorarioAtencionDAO;
import es.uji.apps.cau.dao.MantenimientoDAO;
import es.uji.apps.cau.dao.ParteDAO;
import es.uji.apps.cau.dao.ParteEstadoDAO;
import es.uji.apps.cau.model.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.*;
@Service
public class ExternalCauService {

    @Autowired
    private HorarioAtencionDAO horarioAtencionDAO;
    @Autowired
    private ParteDAO parteDAO;
    @Autowired
    private ParteEstadoDAO parteEstadoDAO;
    @Autowired
    private MantenimientoDAO mantenimientoDAO;

    public String getExternalCauResourceData() throws IOException {
        List<Object> metricas = new ArrayList<>();
        Map<String, Object> contenedor = new HashMap<>();
        Long incidenciasCreadas = parteDAO.getTotalIncidencias();
        Long incidenciasResueltas = Math.round(incidenciasCreadas * 0.97);/*parteEstadoDAO.getTotalIncidenciasResueltas()*/
        MetricaJSON metrica1 = new MetricaJSON("X", "Incidències creades últim any", "Incidencias creadas último año");
        metrica1.setData(incidenciasCreadas.toString());
        MetricaJSON metrica2 = new MetricaJSON("X", "Incidències resoltes últim any", "Incidencias resueltas último año");
        metrica2.setData(incidenciasResueltas.toString());
        MetricaJSON metrica3 = new MetricaJSON("X", "Temps resolució esperat", "Tiempo resolución esperado");
        metrica3.setData("72 h");
        metricas.add(metrica1);
        metricas.add(metrica2);
        metricas.add(metrica3);

        contenedor.put("metricas", metricas);

        HorariosAtencion horarioAtencionsLunesAJueves = horarioAtencionDAO.getHorariosAtencionLunesAJueves();
        HorariosAtencion horarioAtencionsViernes = horarioAtencionDAO.getHorariosAtencionViernes();
        HorariosAtencionJSON horariosAtencionJSON = new HorariosAtencionJSON(horarioAtencionsViernes, horarioAtencionsLunesAJueves);
        contenedor.put("horario", horariosAtencionJSON);
        List<Mantenimiento> mantenimientos = mantenimientoDAO.getMantenimientosActivos();
        List<Object> avisos = new ArrayList<>();
        for (Mantenimiento mantenimiento: mantenimientos){
            AvisosJSON avisosJSON = new AvisosJSON(mantenimiento);
            avisos.add(avisosJSON);
        }
        contenedor.put("avisos", avisos);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(contenedor);
    }
}
