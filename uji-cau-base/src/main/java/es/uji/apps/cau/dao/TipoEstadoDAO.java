package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.exceptions.GeneralCAUException;
import es.uji.apps.cau.model.QTipoEstado;
import es.uji.apps.cau.model.TipoEstado;
import es.uji.apps.cau.utils.Estados;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoEstadoDAO extends BaseDAODatabaseImpl
{
    private QTipoEstado qTipoEstado = QTipoEstado.tipoEstado;

    public List<TipoEstado> getTiposEstados()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoEstado);

        return query.list(qTipoEstado);
    }

    public List<TipoEstado> getTiposEstadosParent()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoEstado).where(qTipoEstado.parent.isNull());

        return query.list(qTipoEstado);
    }

    public TipoEstado getTipoEstadoById(Long tipoEstadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoEstado).where(qTipoEstado.id.eq(tipoEstadoId));

        List<TipoEstado> tipoEstadosList = query.list(qTipoEstado);
        if (tipoEstadosList.size() > 0)
        {
            return tipoEstadosList.get(0);
        }
        return null;
    }

    public TipoEstado getTipoEstadoByName(String tipoEstadoName)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoEstado).where(qTipoEstado.nombre.eq(tipoEstadoName));

        List<TipoEstado> tipoEstadosList = query.list(qTipoEstado);
        if (tipoEstadosList.size() > 0)
        {
            return tipoEstadosList.get(0);
        }
        return null;
    }

    public List<TipoEstado> getTipoEstadoPendientes()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoEstado).where(qTipoEstado.parent.eq(Estados.PENDIENTE.getValue()));

        return query.orderBy(qTipoEstado.id.asc()).list(qTipoEstado);
    }

    public List<TipoEstado> getTipoEstadosecundario(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoEstado).where(qTipoEstado.parent.eq(id));

        return query.orderBy(qTipoEstado.id.asc()).list(qTipoEstado);
    }

    public Long getTipoEstadoNextVal(Long id) throws GeneralCAUException
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoEstado).where(qTipoEstado.parent.eq(id));
        List<Long> ids = query.list(qTipoEstado.id);

        for (long i = id+1; i < (id + 1000); i++)
        {
            if (!ids.contains(i))
            {
                return i;
            }
        }
        throw new GeneralCAUException("No se puede crear mas tipos de estados");
    }

    public TipoEstado insertTipoEstado(TipoEstado atributo)
    {
        return insert(atributo);
    }
}
