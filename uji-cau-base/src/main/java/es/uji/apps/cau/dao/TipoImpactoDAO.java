package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.QTipoImpacto;
import es.uji.apps.cau.model.TipoImpacto;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoImpactoDAO extends BaseDAODatabaseImpl
{
    private QTipoImpacto qTipoImpacto = QTipoImpacto.tipoImpacto;

    public List<TipoImpacto> getTiposImpactos()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoImpacto);

        return query.list(qTipoImpacto);
    }

    public TipoImpacto getTipoImpacto(Long tipoImpactoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoImpacto).where(qTipoImpacto.id.eq(tipoImpactoId));

        List<TipoImpacto> tipoImpactoList = query.list(qTipoImpacto);
        if (tipoImpactoList.size() > 0)
        {
            return tipoImpactoList.get(0);
        }
        return null;
    }
}
