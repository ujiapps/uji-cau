package es.uji.apps.cau.services.rest;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.services.TiposService;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.apps.cau.utils.ItemSort;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tipos")
public class TiposResources extends CoreBaseService
{

    @InjectParam
    private TiposService tiposService;

    @Path("estados")
    public TiposEstadosResources getPlatformItem(
            @InjectParam TiposEstadosResources tiposEstadosResources)
    {
        return tiposEstadosResources;
    }

    @GET
    @Path("parte")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposParte()
    {
        return UIEntity.toUI(tiposService.getTiposParte());
    }

    @GET
    @Path("parte/{parteTipoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTipoParte(@PathParam("parteTipoId") Long parteTipoId)
    {
        return UIEntity.toUI(tiposService.getTipoParte(parteTipoId));
    }

//    @GET
//    @Path("estados")
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<UIEntity> getTipoEstados()
//    {
//        return UIEntity.toUI(tiposService.getTipoEstados());
//    }
//    @GET
//    @Path("estados/parent")
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<UIEntity> getTipoEstadosPrincipales()
//    {
//        return UIEntity.toUI(tiposService.getTiposEstadosParent());
//    }
//
//    @GET
//    @Path("estados/pendiente")
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<UIEntity> getTipoEstadoPendientes()
//    {
//        return UIEntity.toUI(tiposService.getTipoEstadoPendientes());
//    }

    @GET
    @Path("prioridad")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoPrioridad(@QueryParam("filter") @DefaultValue("") String filter,
                                           @QueryParam("sort") @DefaultValue("") String sort) throws IOException
    {
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        return UIEntity.toUI(tiposService.getTiposPrioridades(filterList, sortList));
    }

    @GET
    @Path("prioridad/{prioridadId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTipoPrioridad(@PathParam("prioridadId") Long prioridadId)
    {
        return UIEntity.toUI(tiposService.getTipoPrioridad(prioridadId));
    }

    @GET
    @Path("urgencias")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposUrgencias()
    {
        return UIEntity.toUI(tiposService.getTiposUrgencias());
    }

    @GET
    @Path("urgencias/{urgenciaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTipoUrgencia(@PathParam("urgenciaId") Long urgenciaId)
    {
        return UIEntity.toUI(tiposService.getTipoUrgencia(urgenciaId));
    }

    @GET
    @Path("impactos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposImpactos()
    {
        return UIEntity.toUI(tiposService.getTiposImpactos());
    }

    @GET
    @Path("impactos/{impactoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTipoImpacto(@PathParam("impactoId") Long prioridadId)
    {
        return UIEntity.toUI(tiposService.getTipoImpacto(prioridadId));
    }
}
