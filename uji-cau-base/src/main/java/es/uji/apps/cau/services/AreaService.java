package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cau.dao.AreaDAO;
import es.uji.apps.cau.dao.PersonaDAO;
import es.uji.apps.cau.model.Area;

@Service
public class AreaService {
    @Autowired
    private AreaDAO areaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Transactional
    public Area insertArea(Area area) {
        return areaDAO.insert(area);
    }

    @Transactional
    public Area updateArea(Area area) {
        return areaDAO.update(area);
    }

    @Transactional
    public void deleteArea(Long id) {
        //TODO Un area se deberia borrar si esta asignada a un parte?
        areaDAO.delete(Area.class, id);
    }

    public List<Area> getAreas(Integer externa) {
        return areaDAO.getAreas(externa);
    }

    public Area getAreaById(Long areaId) {
        return areaDAO.getAreaById(areaId);
    }

    public List<Area> getAreasByTecnicoId(Long tecnicoId) {
        return areaDAO.getAreasByTecnicoId(tecnicoId);
    }

}
