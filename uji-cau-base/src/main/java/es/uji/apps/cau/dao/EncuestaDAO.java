package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.Encuesta;
import es.uji.apps.cau.model.QEncuesta;
import es.uji.apps.cau.model.QParteVW;
import es.uji.apps.cau.utils.Paginacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;

@Repository
public class EncuestaDAO extends BaseDAODatabaseImpl {

    private QEncuesta qEncuesta = QEncuesta.encuesta;
    private QParteVW qParteVW = QParteVW.parteVW;

    public List<Encuesta> getEncuestas(String queryString, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(queryString);
        Long queryStringLong = 0L;

        try
        {
            queryStringLong = Long.parseLong(queryString);
        }
        catch (Exception e)
        {
        }
        query.from(qEncuesta).join(qEncuesta.parte, qParteVW).fetch()
                .where(qEncuesta.parte.asignadoNombreCompleto.contains(cadenaLimpia.toUpperCase())
                        .or(qEncuesta.parte.asignadoCuenta.toLowerCase().contains(cadenaLimpia.toLowerCase()))
                        .or(qEncuesta.parte.id.eq(queryStringLong))
                        .or(qEncuesta.id.eq(queryStringLong)));
        if(paginacion!=null)
        {
            paginacion.setTotalCount(query.count());
            query.limit(paginacion.getLimit()).offset(paginacion.getStart());
        }
        return query.list(qEncuesta);
    }
}