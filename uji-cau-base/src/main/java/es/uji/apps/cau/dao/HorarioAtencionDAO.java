package es.uji.apps.cau.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.HorariosAtencion;
import es.uji.apps.cau.model.QHorariosAtencion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class HorarioAtencionDAO extends BaseDAODatabaseImpl {

    private QHorariosAtencion qHorariosAtencion= QHorariosAtencion.horariosAtencion;

    public List<HorariosAtencion> getHorariosAtencion() {

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qHorariosAtencion).list(qHorariosAtencion);
    }

    public HorariosAtencion getHorariosAtencionViernes() {
        Date hoy = new Date();
        JPAQuery query = new JPAQuery(entityManager)
                .from(qHorariosAtencion)
                .where(qHorariosAtencion.fechaInicio.loe(hoy)
                        .and(qHorariosAtencion.fechaFin.goe(hoy))
                        .and(qHorariosAtencion.diasSemana.eq("5")));
        return query.singleResult(qHorariosAtencion);
    }

    public HorariosAtencion getHorariosAtencionLunesAJueves() {
        Date hoy = new Date();
        JPAQuery query = new JPAQuery(entityManager)
                .from(qHorariosAtencion)
                .where(qHorariosAtencion.fechaInicio.loe(hoy)
                        .and(qHorariosAtencion.fechaFin.goe(hoy))
                        .and(qHorariosAtencion.diasSemana.eq("1,2,3,4")));
        return query.singleResult(qHorariosAtencion);
    }

    public HorariosAtencion getById(Long id) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qHorariosAtencion).where(qHorariosAtencion.id.eq(id)).uniqueResult(qHorariosAtencion);
    }
}