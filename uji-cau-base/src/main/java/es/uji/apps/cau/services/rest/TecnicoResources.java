package es.uji.apps.cau.services.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.exceptions.GeneralCAUException;
import es.uji.apps.cau.model.AreaTecnico;
import es.uji.apps.cau.model.Persona;
import es.uji.apps.cau.model.Tecnico;
import es.uji.apps.cau.services.TecnicoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("tecnicos")
public class TecnicoResources extends CoreBaseService {
    @InjectParam
    private TecnicoService tecnicoService;

    private final SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> getTecnicosAreas(
            @QueryParam("query") @DefaultValue("") String queryFilter) {
        List<Tecnico> listTecnicoArea = tecnicoService.getTecnicos(queryFilter);
        List<UIEntity> listTecnicoAreaUI = new ArrayList<>();

        for (Tecnico tecnicoArea : listTecnicoArea) {
            UIEntity tecnicoAreaUI = tecnicoToUI(tecnicoArea);
            listTecnicoAreaUI.add(tecnicoAreaUI);
        }
        return listTecnicoAreaUI;
    }

    private UIEntity tecnicoToUI(Tecnico tecnicoArea) {
        UIEntity tecnicoAreaUI = UIEntity.toUI(tecnicoArea);
        tecnicoAreaUI.put("nombreTecnico", tecnicoArea.getTecnico().getNombreCompleto());
        tecnicoAreaUI.put("cuenta", tecnicoArea.getTecnico().getCuenta());
        tecnicoAreaUI.put("extension", tecnicoArea.getTecnico().getExtensionTelefono());
        return tecnicoAreaUI;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addTecnicoArea(@FormParam("tecnicoId") Long tecnicoId,
                                   @FormParam("tipo") String tipo,
                                   @FormParam("fechaInicioActividad") String fechaInicioInactividad,
                                   @FormParam("fechaFinActividad") String fechaFinInactividad,
                                   @FormParam("empresa") String empresa) throws ParseException, GeneralCAUException {

        if (ParamUtils.isNotNull(tecnicoService.getTecnicoById(tecnicoId))) {
            throw new GeneralCAUException("El tècnic ja esta donat d'alta");
        }

        Tecnico tecnico = new Tecnico();
        Persona extpersona = new Persona();
        extpersona.setId(tecnicoId);
        tecnico.setTecnico(extpersona);
        tecnico.setTipo(tipo);
        tecnico.setEmpresa(empresa);
        if (ParamUtils.isNotNull(fechaInicioInactividad)) {
            tecnico.setFechaInicioActividad(formateadorFecha.parse(fechaInicioInactividad));
        } else {
            tecnico.setFechaInicioActividad(new Date());
        }
        if (ParamUtils.isNotNull(fechaFinInactividad)) {
            tecnico.setFechaFinActividad(formateadorFecha.parse(fechaFinInactividad));
        }

        try {
            tecnico = tecnicoService.insertTecnico(tecnico);

        } catch (Exception c) {
            throw new GeneralCAUException("Error al insertar tecnico");
        }
        return tecnicoToUI(tecnico);

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity editTecnicoArea(@FormParam("tecnicoId") Long tecnicoId,
                                    @FormParam("tipo") String tipo,
                                    @FormParam("fechaInicioActividad") String fechaInicioInactividad,
                                    @FormParam("fechaFinActividad") String fechaFinInactividad,
                                    @FormParam("empresa") String empresa) throws ParseException, GeneralCAUException {

        Tecnico tecnico = tecnicoService.getTecnicoById(tecnicoId);
        if (ParamUtils.isNotNull(tipo)) {
            tecnico.setTipo(tipo);

        }
        if (ParamUtils.isNotNull(empresa)) {
            tecnico.setEmpresa(empresa);

        }
        if (ParamUtils.isNotNull(fechaInicioInactividad)) {
            tecnico.setFechaInicioActividad(formateadorFecha.parse(fechaInicioInactividad));
        } else {
            tecnico.setFechaInicioActividad(new Date());
        }
        if (ParamUtils.isNotNull(fechaFinInactividad)) {
            tecnico.setFechaFinActividad(formateadorFecha.parse(fechaFinInactividad));
        }

        try {
            tecnico = tecnicoService.updateTecnico(tecnico);

        } catch (Exception c) {
            throw new GeneralCAUException("Error al editar tecnico");
        }
        return tecnicoToUI(tecnico);

    }

    @GET
    @Path("/{tecnicoId}/areas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAreasByTecnicoId(@PathParam("tecnicoId") Long perId) {
        return UIEntity.toUI(tecnicoService.getAreasTecnico(perId));
    }


    @PUT
    @Path("/{tecnicoId}/areas/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateArea(@PathParam("tecnicoId") Long tecnicoId, @PathParam("id") Long id, UIEntity uiEntity) {

        AreaTecnico areaTecnico = tecnicoService.getAreaTecnico(id);
        areaTecnico.setAreaId(uiEntity.getLong("areaId"));
        areaTecnico.setAreaPrincipal(uiEntity.getBoolean("areaPrincipal") ? 1L : 0L);
        areaTecnico.setPerIdTecnico(tecnicoId);

        return UIEntity.toUI(tecnicoService.updateTecnico(areaTecnico));
    }

    @POST
    @Path("/{tecnicoId}/areas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addArea(@PathParam("tecnicoId") Long tecnicoId, UIEntity uiEntity) {
        AreaTecnico areaTecnico = new AreaTecnico();
        areaTecnico.setAreaId(uiEntity.getLong("areaId"));
        areaTecnico.setAreaPrincipal(uiEntity.getBoolean("areaPrincipal") ? 1L : 0L);
        areaTecnico.setPerIdTecnico(tecnicoId);

        return UIEntity.toUI(tecnicoService.insertTecnico(areaTecnico));
    }


    @DELETE
    @Path("/{tecnicoId}/areas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("tecnicoId") Long tecnicoId, @PathParam("id") Long id) {
        tecnicoService.deleteTecnicoArea(id);

        return Response.noContent().build();
    }

}
