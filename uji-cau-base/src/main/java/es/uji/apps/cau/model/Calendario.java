package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_EXT_CALENDARIO")
public class Calendario implements Serializable {
    @Id
    private Long id;
    @Column(name = "DIA")
    private Integer dia;
    @Column(name = "MES")
    private Integer mes;
    @Column(name = "ANYO")
    private Integer anyo;
    @Column(name = "SEMANA")
    private Integer semana;
    @Column(name = "TIPO_LAB")
    private String tipoLab;
    @Column(name = "INCIDENCIA_LAB")
    private String incidenciaLab;
    @Column(name = "TIPO_ACA")
    private String tipoAca;
    @Column(name = "INCIDENCIA_ACA")
    private String incidenciaAca;
    @Column(name = "DIA_SEMANA")
    private String diaSemana;
    @Column(name = "HORAS_DIARIAS1")
    private Float horasDiarias1;
    @Column(name = "HORAS_DIARIAS2")
    private Float horasDiarias2;
    @Column(name = "NO_RECUPERABLE")
    private Integer noRecuperable;
    @Column(name = "HAY_TARDES")
    private String hayTardes;
    @Column(name = "FECHA_COMPLETA")
    private Date fechaCompleta;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getDia()
    {
        return dia;
    }

    public void setDia(Integer dia)
    {
        this.dia = dia;
    }

    public Integer getMes()
    {
        return mes;
    }

    public void setMes(Integer mes)
    {
        this.mes = mes;
    }

    public Integer getAnyo()
    {
        return anyo;
    }

    public void setAnyo(Integer anyo)
    {
        this.anyo = anyo;
    }

    public Integer getSemana()
    {
        return semana;
    }

    public void setSemana(Integer semana)
    {
        this.semana = semana;
    }

    public String getTipoLab()
    {
        return tipoLab;
    }

    public void setTipoLab(String tipoLab)
    {
        this.tipoLab = tipoLab;
    }

    public String getIncidenciaLab()
    {
        return incidenciaLab;
    }

    public void setIncidenciaLab(String incidenciaLab)
    {
        this.incidenciaLab = incidenciaLab;
    }

    public String getTipoAca()
    {
        return tipoAca;
    }

    public void setTipoAca(String tipoAca)
    {
        this.tipoAca = tipoAca;
    }

    public String getIncidenciaAca()
    {
        return incidenciaAca;
    }

    public void setIncidenciaAca(String incidenciaAca)
    {
        this.incidenciaAca = incidenciaAca;
    }

    public String getDiaSemana()
    {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana)
    {
        this.diaSemana = diaSemana;
    }

    public Float getHorasDiarias1()
    {
        return horasDiarias1;
    }

    public void setHorasDiarias1(Float horasDiarias1)
    {
        this.horasDiarias1 = horasDiarias1;
    }

    public Float getHorasDiarias2()
    {
        return horasDiarias2;
    }

    public void setHorasDiarias2(Float horasDiarias2)
    {
        this.horasDiarias2 = horasDiarias2;
    }

    public Integer getNoRecuperable()
    {
        return noRecuperable;
    }

    public void setNoRecuperable(Integer noRecuperable)
    {
        this.noRecuperable = noRecuperable;
    }

    public String getHayTardes()
    {
        return hayTardes;
    }

    public void setHayTardes(String hayTardes)
    {
        this.hayTardes = hayTardes;
    }

    public Date getFechaCompleta()
    {
        return fechaCompleta;
    }

    public void setFechaCompleta(Date fechaCompleta)
    {
        this.fechaCompleta = fechaCompleta;
    }
}
