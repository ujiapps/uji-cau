package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.Tuple;

import es.uji.apps.cau.dao.TecnicoAreaDAO;
import es.uji.apps.cau.model.TecnicoArea;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TecnicoAreaService {
    @Autowired
    private TecnicoAreaDAO tecnicoAreaDAO;

    @Transactional
    public TecnicoArea insertTecnicoArea(TecnicoArea tecnicoArea) {
        return tecnicoAreaDAO.insert(tecnicoArea);
    }

    @Transactional
    public TecnicoArea updateTecnicoArea(TecnicoArea tecnicoArea) {
        return tecnicoAreaDAO.update(tecnicoArea);
    }

    @Transactional
    public void deleteTecnicoArea(Long id) {
        tecnicoAreaDAO.delete(TecnicoArea.class, id);
    }

    public List<TecnicoArea> getTecnicosAreas(List<ItemFilter> filterList) {
        return tecnicoAreaDAO.getTecnicosArea(filterList);
    }

    public List<Tuple> getTecnicosPartesAsignados() {
        return tecnicoAreaDAO.getTecnicosPartesAsignados();
    }

    public TecnicoArea getTecnicoAreaById(Long tecnicoAreaId) {
        return tecnicoAreaDAO.getTecnicoAreaById(tecnicoAreaId);
    }

    public TecnicoArea getTecnicoAreaByPerId(Long perId) throws RegistroNoEncontradoException {
        return tecnicoAreaDAO.getTecnicoAreaByPerId(perId);
    }

    public Boolean hasPerfil(String perfil, Long perId) {
        return tecnicoAreaDAO.hasPerfil(perfil, perId);
    }
    public Boolean hasPerfiles(List<String> perfiles, Long perId) {
        return tecnicoAreaDAO.hasPerfiles(perfiles, perId);
    }

    public List<TecnicoArea> getJefesAreaByTecnico(Long perId) throws RegistroNoEncontradoException {
        TecnicoArea tecnicoArea = tecnicoAreaDAO.getTecnicoAreaByPerId(perId);
       return tecnicoAreaDAO.getJefesAreaId(tecnicoArea.getArea().getId());
    }

    public List<TecnicoArea> getTecnicosPrimera(){
        return tecnicoAreaDAO.getTecnicosPrimera();
    }


    public List<TecnicoArea> getTecnicosAreasById(List<ItemFilter> filterList, List<Long> areas)
    {
        return tecnicoAreaDAO.getTecnicosAreasById(filterList, areas);
    }
}