package es.uji.apps.cau.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Mensaje;
import es.uji.apps.cau.services.MensajesService;
import es.uji.apps.cau.utils.TipoMensaje;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("mensajes")
public class MensajesResources extends CoreBaseService {

    @InjectParam
    MensajesService mensajesService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll() {
        Long userConnected = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(mensajesService.getMensajes(userConnected));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("listado")
    public List<UIEntity> getListadoMensajes() {
        Long userConnected = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(mensajesService.getListadoMensajes(userConnected));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id) {
        return UIEntity.toUI(mensajesService.getMensaje(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(@FormParam("mensaje") String mensaje ,@FormParam("tipo") String tipo) {
        ParamUtils.checkNotNull(mensaje);
        Long userConnected = AccessManager.getConnectedUserId(request);
        mensajesService.addMensaje(mensaje, TipoMensaje.valueOf(tipo),userConnected);
        return UIEntity.toUI(mensaje);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("id") Long id, @FormParam("mensaje") String mensaje ,@FormParam("tipo") String tipo) {
        ParamUtils.checkNotNull(mensaje);
        Long userConnected = AccessManager.getConnectedUserId(request);
        mensajesService.updateMensaje(id,mensaje, TipoMensaje.valueOf(tipo),userConnected);
        return UIEntity.toUI(mensaje);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
        mensajesService.deleteAlerta(id);
        return Response.noContent().build();
    }
}