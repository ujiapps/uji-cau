package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.QPersonaUbicacion;
import es.uji.apps.cau.model.QUbicacionFisica;
import es.uji.apps.cau.model.UbicacionFisica;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class UbicacionFisicaDAO extends BaseDAODatabaseImpl {
    QUbicacionFisica qUbicacionFisica = QUbicacionFisica.ubicacionFisica;

    public UbicacionFisica getExtUbicacionFisica(String codigoUbicacion) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qUbicacionFisica).where(qUbicacionFisica.nombre.like(codigoUbicacion.toUpperCase()));

        List<UbicacionFisica> ubicacionFisicas = query.list(qUbicacionFisica);
        if (ubicacionFisicas.size() > 0)
        {
            return ubicacionFisicas.get(0);
        }
        return null;
    }

    public List<UbicacionFisica> searchExtUbicacionFisica(String search) {
        JPAQuery query = new JPAQuery(entityManager);

        if (search != null && !search.isEmpty()) {
            query.from(qUbicacionFisica).where(qUbicacionFisica.estado.eq("A").and(qUbicacionFisica.nombre.lower().like("%" + search.toLowerCase() + "%")));
        }

        return query.list(qUbicacionFisica);
    }

    public UbicacionFisica getExtUbicacionFisicaById(Long ubicacionId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qUbicacionFisica).where(qUbicacionFisica.id.eq(ubicacionId));

        return query.uniqueResult(qUbicacionFisica);
    }

    public List<UbicacionFisica> getExtUbicacionFisica() {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qUbicacionFisica).where(qUbicacionFisica.estado.eq("A"));

        return query.list(qUbicacionFisica);
    }

    public List<UbicacionFisica> getUbicacionFisicaByperId(Long perId) {
        JPAQuery query = new JPAQuery(entityManager);

        QPersonaUbicacion qPersonaUbicacion = QPersonaUbicacion.personaUbicacion;

        query.from(qUbicacionFisica, qPersonaUbicacion).where(qPersonaUbicacion.ubicacionId.eq(qUbicacionFisica.id).and(qPersonaUbicacion.personaId.eq(perId)));

        return query.list(qUbicacionFisica);
    }
}
