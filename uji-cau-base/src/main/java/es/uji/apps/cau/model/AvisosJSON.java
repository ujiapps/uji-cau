package es.uji.apps.cau.model;
import org.codehaus.jackson.annotate.JsonProperty;

public class AvisosJSON {
    @JsonProperty
    private String name_ca;

    @JsonProperty
    private String name_es;

    @JsonProperty
    private String tipo;

    public AvisosJSON(Mantenimiento mantenimiento) {
        this.name_ca = mantenimiento.getTitulo();
        this.name_es = mantenimiento.getTituloEs();
        this.tipo = mantenimiento.getCategoria().name();
    }

    public AvisosJSON() {
    }

    public String getName_ca() {
        return name_ca;
    }

    public void setName_ca(String name_ca) {
        this.name_ca = name_ca;
    }

    public String getName_es() {
        return name_es;
    }

    public void setName_es(String name_es) {
        this.name_es = name_es;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
