package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.Categoria;
import es.uji.apps.cau.model.QCategoria;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CategoriaDAO extends BaseDAODatabaseImpl
{

    private QCategoria qCategoria = QCategoria.categoria;

    public List<Categoria> getCategorias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qCategoria).list(qCategoria);
    }

    public Categoria getCategoriaById(Long categoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCategoria).where(qCategoria.id.eq(categoriaId));
        return query.uniqueResult(qCategoria);
    }

    public List<Categoria> getCategoriasByAreaId(Long areaId, Boolean all)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (all)
        {
            query.from(qCategoria).where(qCategoria.area.id.eq(areaId));
        } else
        {
            query.from(qCategoria).where(qCategoria.area.id.eq(areaId).and(qCategoria.alta.eq(true)));
        }
        return query.list(qCategoria);
    }
}
