package es.uji.apps.cau.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.cau.model.ParteComunicacion;
import es.uji.apps.cau.model.ParteEstado;

@XmlRootElement(name = "Comunicacion")
public class ComunicacionUI {
    private Long id;
    private String descripcion;
    private Long fecha;
    private Date fechaCreacion;
    private Long tipo;
    private String nombreCompleto;
    private Long personaCreacionId;
    private Long parteId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getFecha() {
        return fecha;
    }

    public void setFecha(Long fecha) {
        this.fecha = fecha;
    }

    public Long getTipo() {
        return tipo;
    }

    public void setTipo(Long tipo) {
        this.tipo = tipo;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Long getPersonaCreacionId() {
        return personaCreacionId;
    }

    public void setPersonaCreacionId(Long personaCreacionId) {
        this.personaCreacionId = personaCreacionId;
    }

    public Long getParteId() {
        return parteId;
    }

    public void setParteId(Long parteId) {
        this.parteId = parteId;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public static ComunicacionUI toUI(ParteComunicacion parteComunicacion) {
        ComunicacionUI comunicacionUI = new ComunicacionUI();
        comunicacionUI.setId(parteComunicacion.getId());
        comunicacionUI.setParteId(parteComunicacion.getId());
        comunicacionUI.setPersonaCreacionId(parteComunicacion.getPersonaCreacion().getId());
        comunicacionUI
                .setNombreCompleto(parteComunicacion.getPersonaCreacion().getNombreCompleto());
        comunicacionUI.setDescripcion(parteComunicacion.getDescripcion());
        comunicacionUI.setFecha(parteComunicacion.getFechaCreacion().getTime() / 1000);
        comunicacionUI.setFechaCreacion(parteComunicacion.getFechaCreacion());

        comunicacionUI.setTipo(parteComunicacion.getInterno());

        return comunicacionUI;

    }

    public static ComunicacionUI toUI(ParteEstado parteEstado) {
        ComunicacionUI comunicacionUI = new ComunicacionUI();
        comunicacionUI.setParteId(parteEstado.getId());
        comunicacionUI.setPersonaCreacionId(parteEstado.getPersonaIni().getId());
        comunicacionUI.setNombreCompleto(parteEstado.getPersonaIni().getNombreCompleto());
        comunicacionUI.setDescripcion(parteEstado.getDescripcion());
        comunicacionUI.setFecha(parteEstado.getFechaInicio().getTime() / 1000);
        comunicacionUI.setFechaCreacion(parteEstado.getFechaInicio());
        comunicacionUI.setTipo(2L);
        return comunicacionUI;

    }

    public static List<ComunicacionUI> toUI(List<ParteComunicacion> parteComunicacionList,
                                            List<ParteEstado> parteEstadoList) {
        List<ComunicacionUI> comunicacionUIList = new ArrayList<>();
        for (ParteComunicacion parteComunicacion : parteComunicacionList) {
            comunicacionUIList.add(toUI(parteComunicacion));
        }
        for (ParteEstado parteEstado : parteEstadoList) {
            comunicacionUIList.add(toUI(parteEstado));
        }

        Collections.sort(comunicacionUIList, (o1, o2) -> o1.getFecha().compareTo(o2.getFecha()));
        return comunicacionUIList;
    }

}
