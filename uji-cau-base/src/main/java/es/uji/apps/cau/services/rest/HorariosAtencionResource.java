package es.uji.apps.cau.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.services.HorariosAtencionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("horarios")
public class HorariosAtencionResource extends CoreBaseService {

    @InjectParam
    private HorariosAtencionService horariosAtencionService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHorariosAtencion() {
        return UIEntity.toUI(horariosAtencionService.getHorariosAtencion());
    }



    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(@FormParam("fechaInicio") String fechaInicio,
                        @FormParam("fechaFin") String fechaFin,
                        @FormParam("horaInicio") String horaInicio,
                        @FormParam("horaFin") String horaFin,
                        @FormParam("diasSemana") List<String> diasSemana) {
        ParamUtils.checkNotNull(fechaInicio);
        return UIEntity.toUI(horariosAtencionService.addHorarios(fechaInicio,fechaFin, horaInicio, horaFin, diasSemana));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("id") Long id,
                        @FormParam("fechaInicio") String fechaInicio,
                        @FormParam("fechaFin") String fechaFin,
                        @FormParam("horaInicio") String horaInicio,
                        @FormParam("horaFin") String horaFin,
                        @FormParam("diasSemana") List<String> diasSemana) {
        ParamUtils.checkNotNull(id,fechaInicio);
        return UIEntity.toUI(horariosAtencionService.updateHorarios(id,fechaInicio,fechaFin, horaInicio, horaFin, diasSemana));
    }



    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        horariosAtencionService.deleteHorarioAtencion(id);

        return Response.noContent().build();
    }
}