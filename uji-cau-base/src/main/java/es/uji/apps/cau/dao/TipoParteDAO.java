package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.QTipoParte;
import es.uji.apps.cau.model.TipoParte;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoParteDAO extends BaseDAODatabaseImpl
{
    private QTipoParte qTipoParte = QTipoParte.tipoParte;

    public List<TipoParte> getTiposParte()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoParte);

        return query.list(qTipoParte);
    }

    public TipoParte getTipoParte(Long tipoParteId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoParte).where(qTipoParte.id.eq(tipoParteId));

        List<TipoParte> tipoParteList = query.list(qTipoParte);
        if (tipoParteList.size() > 0)
        {
            return tipoParteList.get(0);
        }
        return null;
    }
}
