package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cau.dao.TipoEstadoDAO;
import es.uji.apps.cau.dao.TipoImpactoDAO;
import es.uji.apps.cau.dao.TipoParteDAO;
import es.uji.apps.cau.dao.TipoPrioridadDAO;
import es.uji.apps.cau.dao.TipoUrgenciaDAO;
import es.uji.apps.cau.exceptions.GeneralCAUException;
import es.uji.apps.cau.model.TipoEstado;
import es.uji.apps.cau.model.TipoImpacto;
import es.uji.apps.cau.model.TipoParte;
import es.uji.apps.cau.model.TipoPrioridad;
import es.uji.apps.cau.model.TipoUrgencia;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.apps.cau.utils.ItemSort;

@Service
public class TiposService {
    @Autowired
    private TipoUrgenciaDAO tipoUrgenciaDAO;

    @Autowired
    private TipoPrioridadDAO tipoPrioridadDAO;

    @Autowired
    private TipoImpactoDAO tipoImpactoDAO;

    @Autowired
    private TipoParteDAO tipoParteDAO;

    @Autowired
    private TipoEstadoDAO tipoEstadoDAO;

    @Transactional
    public TipoImpacto insertTipoImpacto(TipoImpacto parte) {
        return tipoImpactoDAO.insert(parte);
    }

    @Transactional
    public TipoImpacto updateTipoImpacto(TipoImpacto parte) {
        return tipoImpactoDAO.update(parte);
    }

    @Transactional
    public void deleteTipoImpacto(Long id) {
        tipoImpactoDAO.delete(TipoImpacto.class, id);
    }

    public List<TipoParte> getTiposParte() {
        return tipoParteDAO.getTiposParte();
    }

    public TipoParte getTipoParte(Long tipoParteId) {
        return tipoParteDAO.getTipoParte(tipoParteId);
    }

    public List<TipoImpacto> getTiposImpactos() {
        return tipoImpactoDAO.getTiposImpactos();
    }

    public TipoImpacto getTipoImpacto(Long tipoImpactoId) {
        return tipoImpactoDAO.getTipoImpacto(tipoImpactoId);
    }

    public TipoUrgencia insertTipoUrgencia(TipoUrgencia parte) {
        return tipoUrgenciaDAO.insert(parte);
    }

    public TipoUrgencia updateTipoUrgencia(TipoUrgencia parte) {
        return tipoUrgenciaDAO.update(parte);
    }

    public void deleteTipoUrgencia(Long id) {
        tipoUrgenciaDAO.delete(TipoUrgencia.class, id);
    }

    public List<TipoUrgencia> getTiposUrgencias() {
        return tipoUrgenciaDAO.getTiposUrgencias();
    }

    public TipoUrgencia getTipoUrgencia(Long tipoImpactoId) {
        return tipoUrgenciaDAO.getTipoUrgencia(tipoImpactoId);
    }

    public TipoPrioridad insertTipoPrioridad(TipoPrioridad parte) {
        return tipoPrioridadDAO.insert(parte);
    }

    public TipoPrioridad updateTipoPrioridad(TipoPrioridad parte) {
        return tipoPrioridadDAO.update(parte);
    }

    public void deleteTipoPrioridad(Long id) {
        tipoPrioridadDAO.delete(TipoPrioridad.class, id);
    }

    public List<TipoPrioridad> getTiposPrioridades(List<ItemFilter> filterList,
                                                   List<ItemSort> sortList) {
        return tipoPrioridadDAO.getTiposPrioridades(filterList, sortList);
    }

    public TipoPrioridad getTipoPrioridad(Long tipoPrioridadId) {
        return tipoPrioridadDAO.getTipoPrioridad(tipoPrioridadId);
    }

    public TipoEstado getTipoEstadoById(Long tipoEstado) {
        return tipoEstadoDAO.getTipoEstadoById(tipoEstado);
    }

    public TipoEstado getTipoEstadoByName(String nameTipoEstado) {
        return tipoEstadoDAO.getTipoEstadoByName(nameTipoEstado);
    }

    public TipoEstado insertTipoEstado(TipoEstado atributo) {
        return tipoEstadoDAO.insertTipoEstado(atributo);
    }

    public List<TipoEstado> getTipoEstados() {
        return tipoEstadoDAO.getTiposEstados();
    }

    public List<TipoEstado> getTiposEstadosParent() {
        return tipoEstadoDAO.getTiposEstadosParent();
    }

    public List<TipoEstado> getTipoEstadoPendientes() {
        return tipoEstadoDAO.getTipoEstadoPendientes();
    }

    public TipoPrioridad getTipoPrioridadByImpactoAndUrgencia(Long impactoId, Long urgenciaId) {
        return tipoPrioridadDAO.getTipoPrioridadByImpactoAndUrgencia(impactoId, urgenciaId);
    }

    public List<TipoEstado> getTipoEstadosecundario(Long id) {
        return tipoEstadoDAO.getTipoEstadosecundario(id);
    }

    public Long getTipoEstadoNextVal(Long tipoId) throws GeneralCAUException {
        return tipoEstadoDAO.getTipoEstadoNextVal(tipoId);
    }

    public TipoEstado addSecundario(Long parentId, String nombre) throws GeneralCAUException {
        TipoEstado tipoEstado = new TipoEstado();
        tipoEstado.setParent(parentId);
        tipoEstado.setNombre(nombre);
        tipoEstado.setId(tipoEstadoDAO.getTipoEstadoNextVal(parentId));
        return insertTipoEstado(tipoEstado);
    }

    public TipoEstado updateTipoEstado(TipoEstado estado) {
        return tipoEstadoDAO.update(estado);
    }
}
