package es.uji.apps.cau.services.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Persona;
import es.uji.apps.cau.model.UbicacionFisica;
import es.uji.apps.cau.services.AreaService;
import es.uji.apps.cau.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("persona")
public class PersonaResources extends CoreBaseService {
    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private AreaService areaService;

    @GET
    @Path("public/search")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getsearchPublicExtPersona(@QueryParam("term") String search) {
        if (search != null) {
            List<Persona> personaList = personaService.searchPersona(search, 500);
            List<UIEntity> result = new ArrayList<>();
            for (Persona persona : personaList) {
                UIEntity ui = new UIEntity();
                ui.put("value", persona.getId());
                ui.put("label", persona.getNombreCompleto());
                result.add(ui);
            }
            ResponseMessage responseMessage = new ResponseMessage();
            responseMessage.setSuccess(true);
            responseMessage.setTotalCount(personaList.size());
            responseMessage.setData(result);
            return responseMessage;
        }
        return null;
    }

    private List<UIEntity> toUI(List<Persona> personaList) {
        List<UIEntity> result = new ArrayList<>();
        for (Persona persona : personaList) {
            UbicacionFisica ub = persona.getUbicacionFisica();
            UIEntity ui = UIEntity.toUI(persona);
            if (ub != null) {
                ui.put("ubicacionFisicaNombre", persona.getUbicacionFisica().getNombre());
                ui.put("departamento", persona.getUbicacionFisica().getNombreDepartamento());

            }
            result.add(ui);
        }
        return result;
    }

    @GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSearchPersona(@QueryParam("query") String search,
                                           @QueryParam("page") @DefaultValue("0") Integer page,
                                           @QueryParam("limit") @DefaultValue("25") Integer limit) {
        if (search != null) {

            List<Persona> personaList = personaService.searchPersona(search, 20);
            return toUI(personaList);
        }
        return Collections.singletonList(new UIEntity());
    }

    @GET
    @Path("cuenta")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSearchCuentaPersona(@QueryParam("query") String search,
                                                 @QueryParam("page") @DefaultValue("0") Integer page,
                                                 @QueryParam("limit") @DefaultValue("25") Integer limit) {
        if (search != null) {
            List<Persona> personaList = personaService.searchPersonaByCuenta(search, 20);
            return toUI(personaList);
        }
        return null;
    }

    @GET
    @Path("{personaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getExtPersona(@PathParam("personaId") Long personaId) {
        ResponseMessage responseMessage = new ResponseMessage();

        if (personaId != null) {
            Persona persona = personaService.getPersonaById(personaId);
            UIEntity uiEntity = UIEntity.toUI(persona);
            uiEntity.put("ubicacion", UIEntity.toUI(persona.getUbicacionFisica()));
            responseMessage.setSuccess(true);
            responseMessage.setData(Collections.singletonList(uiEntity));
            return responseMessage;
        }
        return null;
    }

    @GET
    @Path("tecnico")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTecnicos() {
        return UIEntity.toUI(personaService.getTecnicos());
    }

    @GET
    @Path("tecnico/{tecnicoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTecnicoById(@PathParam("tecnicoId") Long tecnicoId) {
        if (tecnicoId != null) {
            Persona personaList = personaService.getTecnicoById(tecnicoId);
            return UIEntity.toUI(personaList);
        }
        return null;
    }

    @GET
    @Path("tecnico/{tecnicoId}/areas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAreasByTecnicoId(@PathParam("tecnicoId") Long tecnicoId) {
        return UIEntity.toUI(areaService.getAreasByTecnicoId(tecnicoId));
    }

    @GET
    @Path("tecnico/search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getsearchTecnico(@QueryParam("query") String search,
                                           @QueryParam("page") @DefaultValue("0") Integer page,
                                           @QueryParam("limit") @DefaultValue("25") Integer limit) {
        if (ParamUtils.isNotNull(search)) {
            List<Persona> personaList = personaService.searchTecnicos(search, 20);
            return toUI(personaList);
        }
        return null;
    }

}
