package es.uji.apps.cau.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.cau.model.Persona;
import es.uji.apps.cau.utils.Estados;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.uji.apps.cau.dao.ParteDAO;
import es.uji.apps.cau.dao.PersonaDAO;
import es.uji.apps.cau.dao.TecnicoAreaDAO;
import es.uji.apps.cau.dao.TecnicoDAO;
import es.uji.apps.cau.model.AreaTecnico;
import es.uji.apps.cau.model.Parte;
import es.uji.apps.cau.model.ParteVW;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.apps.cau.utils.ItemSort;
import es.uji.commons.rest.ParamUtils;

@Service
public class PartesService {
    private final ParteDAO parteDAO;

    private final PersonaDAO personaDAO;

    private final TecnicoAreaDAO tecnicoAreaDAO;

    private final TecnicoDAO tecnicoDAO;

    @Autowired
    public PartesService(ParteDAO parteDAO, PersonaDAO personaDAO, TecnicoAreaDAO tecnicoAreaDAO, TecnicoDAO tecnicoDAO) {
        this.parteDAO = parteDAO;
        this.personaDAO = personaDAO;
        this.tecnicoAreaDAO = tecnicoAreaDAO;
        this.tecnicoDAO = tecnicoDAO;
    }


    public Parte insertParte(Parte parte) {
        return parteDAO.insert(parte);
    }

    public Parte updateParte(Parte parte) {
        return parteDAO.update(parte);
    }

    public void deleteParte(Long id) {
        parteDAO.delete(Parte.class, id);
    }

    public Page<Parte> getUserPublicPartes(Long userId, List<ItemFilter> filterList,
                                           List<ItemSort> sortList, Pageable page) {
        return parteDAO.getUserPublicPartes(userId, filterList, sortList, page);
    }

    public Page<ParteVW> getUserPublicPartesVW(Long userId, List<ItemFilter> filterList,
                                               List<ItemSort> sortList, Pageable page) {
        return parteDAO.getUserPublicPartesVW(userId, filterList, sortList, page);
    }

    public List<ParteVW> getPartesNuevo(Long userId, List<ItemFilter> filterList, List<ItemSort> sortList) {
        if (tecnicoAreaDAO.hasPerfil("ADMINISTRADOR", userId) || tecnicoAreaDAO.hasPerfil("PRIMERA", userId)) {
            return parteDAO.getPartesNuevos(filterList, sortList);
        }
        return new ArrayList<>();
    }

    public ParteVW getParteVWById(Long userId, Long parteId) {
        ParamUtils.checkNotNull(parteId, userId);
        if (!tecnicoAreaDAO.hasPerfil("ADMINISTRADOR", userId) && !tecnicoAreaDAO.hasPerfil("PRIMERA", userId)) {
            List<Long> areas = tecnicoDAO.getAreasTecnicoById(userId)
                    .stream().map(AreaTecnico::getAreaId).collect(Collectors.toList());
            return parteDAO.getParteVWByIdAndAreaId(userId, parteId, areas);
        }
        return parteDAO.getParteVWById(parteId);
    }

    public Parte getParteByIdAndUserId(Long parteId, Long userId) {
        return parteDAO.getParteByIdAndUserId(parteId, userId);
    }

    public Parte getParte(Long parteId) {
        return parteDAO.getParte(parteId);
    }

    public List<ParteVW> searchPartes(Long userId, List<ItemFilter> filterList,
                                      List<ItemSort> sortList, List<Long> seleccionados) throws ParseException {
        if (!tecnicoAreaDAO.hasPerfil("ADMINISTRADOR", userId) && !tecnicoAreaDAO.hasPerfil("PRIMERA", userId)) {

            List<Long> areas = tecnicoDAO.getAreasTecnicoById(userId)
                    .stream().map(AreaTecnico::getAreaId).collect(Collectors.toList());
            return parteDAO.searchPartes(userId, filterList, sortList, seleccionados, areas);
        }
        return parteDAO.searchPartes(userId, filterList, sortList, seleccionados);
    }

    public List<ParteVW> getParteByUserAfectado(Long userId, List<ItemFilter> filterList,
                                                List<ItemSort> sortList) throws ParseException {
        /*if (!tecnicoAreaDAO.hasPerfil("ADMINISTRADOR", userId) && !tecnicoAreaDAO.hasPerfil("PRIMERA", userId)) {
            List<Long> areas = tecnicoDAO.getAreasTecnicoById(userId)
                    .stream().map(AreaTecnico::getAreaId).collect(Collectors.toList());
            return parteDAO.getPartesByUserAfectado(userId, filterList, sortList, areas);
        }*/
        return parteDAO.getPartesByUserAfectado(userId, filterList, sortList);
    }

    public List<ParteVW> getParteByCodigoBarras(Long userId, String codigoBarras, List<ItemFilter> filterList,
                                                List<ItemSort> sortList) throws ParseException {

        /*if (!tecnicoAreaDAO.hasPerfil("ADMINISTRADOR", userId) && !tecnicoAreaDAO.hasPerfil("PRIMERA", userId)) {
            List<Long> areas = tecnicoDAO.getAreasTecnicoById(userId)
                    .stream().map(AreaTecnico::getAreaId).collect(Collectors.toList());
            return parteDAO.getParteByCodigoBarras(userId, codigoBarras, sortList, areas);
        }*/
        return parteDAO.getParteByCodigoBarras(userId, codigoBarras, sortList);
    }

    public List<ParteVW> getPartesEncolados(Long userId, List<ItemFilter> filterList,
                                            List<ItemSort> sortList) {
        if (tecnicoAreaDAO.hasPerfil("ADMINISTRADOR", userId) || tecnicoAreaDAO.hasPerfil("PRIMERA", userId)) {
            return parteDAO.getPartesEncolados(userId, filterList, sortList);
        }
        return new ArrayList<>();
    }

    public List<ParteVW> getPartesAsignados(Long userId, List<ItemFilter> filterList,
                                            List<ItemSort> sortList, List<Long> seleccionados) {
        return parteDAO.getPartesAsignados(userId, filterList, sortList, seleccionados);
    }

    public List<ParteVW> getPartesCreados(Long userId, List<ItemFilter> filterList,
                                          List<ItemSort> sortList, List<Long> seleccionados) {
        return parteDAO.getPartesCreados(userId, filterList, sortList, seleccionados);
    }

    public List<ParteVW> getPartesAsignadosByTecnicoId(Long tecnicoId, List<ItemFilter> filterList, List<ItemSort> sortList) {
        return parteDAO.getPartesAsignadosByTecnicoId(tecnicoId, filterList, sortList);
    }

    public List<ParteVW> getSubParteVWById(Long parteId) {
        return parteDAO.getSubPartesVWById(parteId);
    }

    public void updateParteNotificaciones(long parte, int notifica) {
        parteDAO.updateParteNotificaciones(parte, notifica);
    }
}
