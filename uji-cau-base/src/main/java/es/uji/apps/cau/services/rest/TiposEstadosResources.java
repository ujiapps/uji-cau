package es.uji.apps.cau.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.exceptions.GeneralCAUException;
import es.uji.apps.cau.model.TipoEstado;
import es.uji.apps.cau.services.TiposService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;


public class TiposEstadosResources extends CoreBaseService {

    @InjectParam
    private TiposService tiposService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoEstados() {
        return UIEntity.toUI(tiposService.getTipoEstados());
    }

    @GET
    @Path("parent")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoEstadosPrincipales() {
        return UIEntity.toUI(tiposService.getTiposEstadosParent());
    }

    @GET
    @Path("pendiente")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoEstadoPendientes() {
        return UIEntity.toUI(tiposService.getTipoEstadoPendientes());
    }

    @GET
    @Path("{parentId}/secundarios")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoEstadosecundarios(@PathParam("parentId") Long id) {
        return UIEntity.toUI(tiposService.getTipoEstadosecundario(id));
    }

    @POST
    @Path("{parentId}/secundarios")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(@PathParam("parentId") Long parentId, UIEntity uiEntity) throws GeneralCAUException {
        String nombre = uiEntity.get("nombre");
        ParamUtils.checkNotNull(nombre);
        return UIEntity.toUI(tiposService.addSecundario(parentId, nombre));

    }

    @PUT
    @Path("{parentId}/secundarios/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("parentId") Long parentId, @PathParam("id") Long id, UIEntity uiEntity) {
        String nombre = uiEntity.get("nombre");
        ParamUtils.checkNotNull(nombre);
        TipoEstado estado = tiposService.getTipoEstadoById(id);
        estado.setNombre(nombre);

        return UIEntity.toUI(tiposService.updateTipoEstado(estado));

    }
}
