package es.uji.apps.cau.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.ParteEstado;
import es.uji.apps.cau.model.QParte;
import es.uji.apps.cau.model.QParteEstado;
import es.uji.apps.cau.utils.Estados;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ParteEstadoDAO extends BaseDAODatabaseImpl
{
    QParteEstado qParteEstado = QParteEstado.parteEstado1;
    QParte qParte = QParte.parte;

    public List<ParteEstado> getPartesEstado(Long parteId, Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (userId != null && parteId != null)
        {
            query.from(qParteEstado).leftJoin(qParteEstado.parteEstado, qParte)
                    .where(qParteEstado.parteEstado.id.eq(parteId));
        }

        return query.list(qParteEstado);
    }

    public ParteEstado getParteEstado(Long parteId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (parteId != null)
        {
            query.from(qParteEstado)
                    .leftJoin(qParteEstado.parteEstado, qParte)
                    .where(qParteEstado.parteEstado.id.eq(parteId).and(
                            qParteEstado.actual.eq(1)));
        }

        List<ParteEstado> parteEstadoList = query.list(qParteEstado);
        if (parteEstadoList.size() > 0)
        {
            return parteEstadoList.get(0);
        }
        return null;
    }

    public ParteEstado getEstadoActual(Long parteId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (parteId != null)
        {
            query.from(qParteEstado).join(qParteEstado.tipoEstado).fetch().where(
                    qParteEstado.parteEstado.id.eq(parteId).and(qParteEstado.actual.eq(1)));

        }

        List<ParteEstado> parteList = query.list(qParteEstado);
        if (parteList.size() > 0)
        {
            return parteList.get(0);
        }
        return null;
    }

    public ParteEstado getParteEstadoCreacion(Long parteId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (parteId != null)
        {
            query.from(qParteEstado).where(
                    qParteEstado.parteEstado.id.eq(parteId).and(qParteEstado.tipoEstado.id.eq(Estados.NUEVO.getValue()))).orderBy(qParteEstado.fechaInicio.asc());
        }
        List<ParteEstado> parte = query.list(qParteEstado);
        if (parte != null)
        {
            return parte.get(0);
        }
        return null;
    }

    public Date getFechaAsignacion(Long parteId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (parteId != null)
        {
            query.from(qParteEstado).where(
                    qParteEstado.parteEstado.id.eq(parteId).and(qParteEstado.tipoEstado.id.eq(Estados.ASIGNADO.getValue()))).orderBy(qParteEstado.fechaInicio.desc());

        }

        List<ParteEstado> parte = query.list(qParteEstado);
        if (parte != null && !parte.isEmpty())
        {
            return parte.get(0).getFechaInicio();
        }
        return null;
    }

    public List<ParteEstado> setParteAutoResuelto()
    {
        JPAQuery query = new JPAQuery(entityManager);
        Date fecha = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, -15);
        query.from(qParteEstado).where(
                qParteEstado.tipoEstado.id.eq(Estados.RESUELTO.getValue()).and(qParteEstado.actual.eq(1)).and(qParteEstado.fechaInicio.lt(calendar.getTime())));

       return query.list(qParteEstado);
    }

    public Long getTotalIncidenciasResueltas(){
        Date todayLastYear = new Date();
        todayLastYear.setHours(0);
        todayLastYear.setMinutes(0);
        todayLastYear.setSeconds(0);
        todayLastYear.setYear(todayLastYear.getYear()-1);
        return new JPAQuery(entityManager)
                .from(qParteEstado)
                .where(qParteEstado.tipoEstado.id.eq(6000L)
                        .and(qParteEstado.fechaInicio.goe(todayLastYear)))
                .singleResult(qParteEstado.id.countDistinct());
    }
}
