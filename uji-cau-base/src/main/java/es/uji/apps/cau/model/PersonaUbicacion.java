package es.uji.apps.cau.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_EXT_PERSONAS_UBICACIONES")
public class PersonaUbicacion implements Serializable {

    @Id
    @Column(name = "PER_ID")
    private Long personaId;
    @Column(name = "UBI_ID")
    private Long ubicacionId;

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getUbicacionId() {
        return ubicacionId;
    }

    public void setUbicacionId(Long ubicacionId) {
        this.ubicacionId = ubicacionId;
    }
}
