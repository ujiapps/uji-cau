package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CAU_PARTES_COMUNICACIONES")
public class ParteComunicacion implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String descripcion;

    @Column(name = "F_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    private Long interno;

    // bi-directional many-to-one association to Persona
    @ManyToOne
    @JoinColumn(name = "PER_ID_CREACION")
    private Persona personaCreacion;

    // bi-directional many-to-one association to Parte
    @ManyToOne
    @JoinColumn(name = "PAR_ID")
    private Parte parteComunicacion;

    public ParteComunicacion()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion()
    {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getInterno()
    {
        return this.interno;
    }

    public void setInterno(Long interno)
    {
        this.interno = interno;
    }

    public Persona getPersonaCreacion()
    {
        return this.personaCreacion;
    }

    public void setPersonaCreacion(Persona cauPersona)
    {
        this.personaCreacion = cauPersona;
    }

    public Parte getParteComunicacion()
    {
        return this.parteComunicacion;
    }

    public void setParteComunicacion(Parte cauParte)
    {
        this.parteComunicacion = cauParte;
    }

}
