package es.uji.apps.cau.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_CONFIGURACION")
public class Configuracion implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "MAIL_INVENTARIO")
    private String inventario;

    @Column(name = "MAIL_SUGERENCIAS")
    private String sugerencias;

    @Column(name = "MAIL_ENCUESTAS")
    private String encuestas;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getInventario()
    {
        return inventario;
    }

    public void setInventario(String inventario)
    {
        this.inventario = inventario;
    }

    public String getSugerencias()
    {
        return sugerencias;
    }

    public void setSugerencias(String sugerencias)
    {
        this.sugerencias = sugerencias;
    }

    public String getEncuestas()
    {
        return encuestas;
    }

    public void setEncuestas(String encuestas)
    {
        this.encuestas = encuestas;
    }
}
