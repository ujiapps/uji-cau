package es.uji.apps.cau.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Area;
import es.uji.apps.cau.services.AreaService;
import es.uji.apps.cau.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("area")
public class AreaResources extends CoreBaseService {
    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private AreaService areaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAreas(@QueryParam("externa") @DefaultValue("0") Integer externa) {
        return UIEntity.toUI(areaService.getAreas(externa));
    }

    @GET
    @Path("{areaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getAreaById(@PathParam("areaId") Long areaId) {
        if (areaId != null) {
            Area area = areaService.getAreaById(areaId);
            return UIEntity.toUI(area);
        }
        return null;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addArea(UIEntity entity) {
        String nombre = entity.get("nombre");
        UIEntity ui = new UIEntity();

        if (ParamUtils.isNotNull(nombre)) {
            Area area = new Area();
            area.setNombre(nombre);
            area.setExterna(0);
            area = areaService.insertArea(area);

            ui = UIEntity.toUI(area);
        }
        return ui;

    }

    @PUT
    @Path("{areaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateArea(UIEntity entity) {
        String id = entity.get("id");
        String nombre = entity.get("nombre");
        UIEntity ui = new UIEntity();

        if (ParamUtils.isNotNull(nombre) && ParamUtils.isNotNull(id)) {
            Area area = areaService.getAreaById(Long.parseLong(id));
            area.setNombre(nombre);
            area = areaService.updateArea(area);
            ui = UIEntity.toUI(area);
        }
        return ui;

    }

    @DELETE
    @Path("{areaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteArea(@PathParam("areaId") Long areaId, UIEntity entity) {
        if (areaId != null) {
            areaService.deleteArea(areaId);
        }
    }

    @GET
    @Path("{areaId}/tecnicos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTecnicosByAreaId(@PathParam("areaId") Long areaId) {
        return personaService.getTecnicosByAreaId(areaId);
    }

}
