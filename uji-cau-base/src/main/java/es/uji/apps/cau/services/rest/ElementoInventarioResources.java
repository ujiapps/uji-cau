package es.uji.apps.cau.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.exceptions.ErrorAlLeerPlantilla;
import es.uji.apps.cau.model.ElementoInventario;
import es.uji.apps.cau.model.Persona;
import es.uji.apps.cau.services.ElementoInventarioService;
import es.uji.apps.cau.services.NotificacionService;
import es.uji.apps.cau.services.PersonaService;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("inventario")
public class ElementoInventarioResources extends CoreBaseService
{
    @InjectParam
    private ElementoInventarioService elementoInventarioService;
    @InjectParam
    private PersonaService personaService;

    @GET
    @Path("{cb}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getExtElementoInventarioCB(@PathParam("cb") String cb)
    {
        if (cb != null)
        {
            return UIEntity.toUI(elementoInventarioService.getExtElementoInventarioCB(cb));
        }

        return null;
    }

    @GET
    @Path("linea")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> searchInventarioById(@QueryParam("query") Long lineaId)
    {
        if (lineaId != null)
        {
            List<ElementoInventario> elementoInventarioList = elementoInventarioService
                    .searchInventarioById(lineaId);
            return UIEntity.toUI(elementoInventarioList);
        }

        return null;
    }

    @GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> searchExtElementoInventarioCB(@QueryParam("search") String search)
    {
        if (search != null)
        {
            List<ElementoInventario> elementoInventarioList = elementoInventarioService
                    .searchExtElementoInventarioCB(search);
            return UIEntity.toUI(elementoInventarioList);
        }

        return null;
    }

    @GET
    @Path("ubicacion/{ubicacionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInventarioByUbicacionId(@PathParam("ubicacionId") String ubicacionId)
    {
        if (ubicacionId != null)
        {
            List<ElementoInventario> elementoInventarioList = elementoInventarioService
                    .getInventarioByUbicacionId(ubicacionId);
            return UIEntity.toUI(elementoInventarioList);
        }

        return null;
    }

    @POST
    @Path("notificacion")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage sendNotificacion(@FormParam("nombre") String nombre, @FormParam("login") String login,
                                            @FormParam("mail") String mail, @FormParam("departamento") String departamento,
                                            @FormParam("ubicacion") String ubicacion, @FormParam("codigoBarras") String codigoBarras,
                                            @FormParam("ubicacionEquipo") String ubicacionEquipo, @FormParam("ip") String ip,
                                            @FormParam("tipoEquipo") String tipoEquipo, @FormParam("marcaEquipo") String marcaEquipo,
                                            @FormParam("modeloEquipo") String modeloEquipo, @FormParam("numeroSerie") String numeroSerie,
                                            @FormParam("fechaCompra") String fechaCompra, @FormParam("fechaAltaUsuario") String fechaAltaUsuario,
                                            @FormParam("descripcion") String descripcion
    ) throws ErrorAlLeerPlantilla, MessageNotSentException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaById(userId);
        ParamUtils.checkNotNull(nombre, login, mail, codigoBarras, descripcion);
        NotificacionService.enviarInventario(persona, nombre, login, mail, departamento, ubicacion, codigoBarras,
                ubicacionEquipo, tipoEquipo, marcaEquipo, modeloEquipo, numeroSerie, fechaCompra,
                fechaAltaUsuario, descripcion);
        return new ResponseMessage(true);
    }

}
