package es.uji.apps.cau.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cau.dao.ConfiguracionDAO;
import es.uji.apps.cau.model.Configuracion;

@Service
public class ConfiguracionService {
    @Autowired
    private ConfiguracionDAO configuracionDAO;

    @Transactional
    public Configuracion insertConfiguracion(Configuracion configuracion) {
        return configuracionDAO.insert(configuracion);
    }

    @Transactional
    public Configuracion updateConfiguracion(Configuracion configuracion) {
        return configuracionDAO.update(configuracion);
    }

    @Transactional
    public void deleteConfiguracion(Long id) {
        configuracionDAO.delete(Configuracion.class, id);
    }

    public Configuracion getConfiguracion() {
        return configuracionDAO.getConfiguracion();
    }

}
