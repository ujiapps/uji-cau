package es.uji.apps.cau.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "CAU_ALERTAS_PARTES")
public class AlertaParte
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "PARTE_ID")
    private Long parteId;

    @Column(name = "FECHA_ALERTA")
    private Date fechaAlerta;

    private String envios;

    @ManyToOne
    @JoinColumn(name = "ALERTA_ID")
    private Alerta alerta;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getParteId()
    {
        return parteId;
    }

    public void setParteId(Long parteId)
    {
        this.parteId = parteId;
    }

    public Date getFechaAlerta()
    {
        return fechaAlerta;
    }

    public void setFechaAlerta(Date fechaAlerta)
    {
        this.fechaAlerta = fechaAlerta;
    }

    public String getEnvios()
    {
        return envios;
    }

    public void setEnvios(String envios)
    {
        this.envios = envios;
    }

    public Alerta getAlerta()
    {
        return alerta;
    }

    public void setAlerta(Alerta alerta)
    {
        this.alerta = alerta;
    }
}
