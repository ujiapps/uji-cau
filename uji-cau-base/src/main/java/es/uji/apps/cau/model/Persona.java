package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "CAU_VMC_PERSONAS")
public class Persona implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String apellido1;

    private String apellido2;

    private String cuenta;

    private String dni;

    private String nombre;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;

    @Column(name = "APELLIDOS_NOMBRE_BUS")
    private String apellidosNombreBus;

    @Column(name = "NOMBRE_COMPLETO_BUS")
    private String nombreCompletoBusqueda;

    @Column(name = "EXTENSION_TELEFONO")
    private String extensionTelefono;

    private String tipo;

    @ManyToOne
    @JoinColumn(name = "ID_UBICACION_FISICA")
    @NotFound(action = NotFoundAction.IGNORE)
    private UbicacionFisica ubicacionFisica;

    @OneToMany(mappedBy = "personaCreacion")
    private Set<Parte> partesPersonaCreacion;

    @OneToMany(mappedBy = "personaSolicitante")
    private Set<Parte> partesPersonaSolicitante;

    @OneToMany(mappedBy = "personaAfectado")
    private Set<Parte> partesPersonaAfectado;

    @OneToMany(mappedBy = "personaAsignado")
    private Set<Parte> partesPersonaAsignado;

    @OneToMany(mappedBy = "personaCreacion")
    private Set<ParteComunicacion> partesComunicaciones;

    // bi-directional many-to-one association to ParteEstado
    @OneToMany(mappedBy = "personaFin")
    private Set<ParteEstado> partesEstadosFin;

    // bi-directional many-to-one association to ParteEstado
    @OneToMany(mappedBy = "personaIni")
    private Set<ParteEstado> partesEstadoIni;

    // bi-directional many-to-one association to TecnicoArea
    @OneToMany(mappedBy = "tecnicoArea")
    private Set<TecnicoArea> tecnicosArea;

    @OneToMany(mappedBy = "tecnico")
    private Set<Tecnico> tecnicos;


    public Persona() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApellido1() {
        return this.apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return this.apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getCuenta() {
        return this.cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getDni() {
        return this.dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCompleto() {
        return this.nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Set<Parte> getPartesPersonaCreacion() {
        return this.partesPersonaCreacion;
    }

    public void setPartesPersonaCreacion(Set<Parte> cauPartes1) {
        this.partesPersonaCreacion = cauPartes1;
    }

    public Set<Parte> getPartesPersonaSolicitante() {
        return this.partesPersonaSolicitante;
    }

    public void setPartesPersonaSolicitante(Set<Parte> cauPartes2) {
        this.partesPersonaSolicitante = cauPartes2;
    }

    public Set<Parte> getPartesPersonaAfectado() {
        return this.partesPersonaAfectado;
    }

    public void setPartesPersonaAfectado(Set<Parte> cauPartes3) {
        this.partesPersonaAfectado = cauPartes3;
    }

    public Set<Parte> getPartesPersonaAsignado() {
        return this.partesPersonaAsignado;
    }

    public void setPartesPersonaAsignado(Set<Parte> cauPartes4) {
        this.partesPersonaAsignado = cauPartes4;
    }

    public Set<ParteComunicacion> getPartesComunicaciones() {
        return this.partesComunicaciones;
    }

    public void setPartesComunicaciones(Set<ParteComunicacion> cauPartesComunicaciones) {
        this.partesComunicaciones = cauPartesComunicaciones;
    }

    public Set<ParteEstado> getPartesEstadosFin() {
        return this.partesEstadosFin;
    }

    public void setPartesEstadosFin(Set<ParteEstado> cauPartesEstados1) {
        this.partesEstadosFin = cauPartesEstados1;
    }

    public Set<ParteEstado> getPartesEstadoIni() {
        return this.partesEstadoIni;
    }

    public void setPartesEstadoIni(Set<ParteEstado> cauPartesEstados2) {
        this.partesEstadoIni = cauPartesEstados2;
    }

    public Set<TecnicoArea> getTecnicosArea() {
        return this.tecnicosArea;
    }

    public void setTecnicosArea(Set<TecnicoArea> cauTecnicosAreas) {
        this.tecnicosArea = cauTecnicosAreas;
    }

    public String getExtensionTelefono() {
        return extensionTelefono;
    }

    public void setExtensionTelefono(String extensionTelefono) {
        this.extensionTelefono = extensionTelefono;
    }

    public UbicacionFisica getUbicacionFisica() {
        return ubicacionFisica;
    }

    public void setUbicacionFisica(UbicacionFisica ubicacionFisica) {
        this.ubicacionFisica = ubicacionFisica;
    }

    public String getNombreCompletoBusqueda() {
        return nombreCompletoBusqueda;
    }

    public void setNombreCompletoBusqueda(String nombreCompletoBusqueda) {
        this.nombreCompletoBusqueda = nombreCompletoBusqueda;
    }

    public String getApellidosNombreBus() {
        return apellidosNombreBus;
    }

    public void setApellidosNombreBus(String apellidosNombreBus) {
        this.apellidosNombreBus = apellidosNombreBus;
    }

    public Set<Tecnico> getTecnicos() {
        return tecnicos;
    }

    public void setTecnicos(Set<Tecnico> tecnicos) {
        this.tecnicos = tecnicos;
    }
}
