package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;

import es.uji.apps.cau.model.QParteTag;
import es.uji.apps.cau.model.QTag;
import es.uji.apps.cau.model.Tag;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;

@Repository
public class TagDAO extends BaseDAODatabaseImpl
{

    private QTag qTag = QTag.tag;
    private QParteTag qParteTag = QParteTag.parteTag;

    public List<Tag> getAll(String search)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTag);

        if (ParamUtils.isNotNull(search))
        {
            String s = StringUtils.limpiaAcentos(search);
            query.where(limpiaAcentos(qTag.nombre).like('%' + s.toUpperCase() + '%'));
        }
        return query.list(qTag);



    }

    public List<Tag> getParteAll(Long parteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTag).join(qTag.parteTags,qParteTag).where(qParteTag.parte.id.eq(parteId));

        return query.list(qTag);



    }

    public static StringExpression limpiaAcentos(StringPath nombre)
    {
        return Expressions.stringTemplate("upper(translate({0}, 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC'))",
                nombre);
    }

    public Tag getTagByName(String tag)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTag);

        if (ParamUtils.isNotNull(tag))
        {
            query.where(qTag.nombre.like(tag));
        }
        return query.uniqueResult(qTag);
    }
}
