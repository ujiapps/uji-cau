package es.uji.apps.cau.services.rest;


import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.cau.services.MantenimientoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("mantenimiento")
public class MantenimientoResource extends CoreBaseService {

    @InjectParam
    private MantenimientoService mantenimientoService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHorariosAtencion() {
        return UIEntity.toUI(mantenimientoService.getMantenimientos());
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(@FormParam("fechaInicio") String fechaInicio,
                        @FormParam("fechaFin") String fechaFin,
                        @FormParam("titulo") String titulo,
                        @FormParam("categoriaId") Long categoria,
                        @FormParam("estadoId") Integer estado,
                        @FormParam("tituloEs") String tituloEs){
        Long userId = AccessManager.getConnectedUserId(request);

        return UIEntity.toUI(mantenimientoService.addMantenimiento(userId, fechaInicio,fechaFin, titulo, tituloEs, categoria, estado));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("id") Long id,
                           @FormParam("fechaInicio") String fechaInicio,
                           @FormParam("fechaFin") String fechaFin,
                           @FormParam("titulo") String titulo,
                           @FormParam("categoriaId") Long categoria,
                           @FormParam("estadoId") Integer estado,
                           @FormParam("tituloEs") String tituloEs) {
        ParamUtils.checkNotNull(id);
        Long userId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(mantenimientoService.updateMantenimiento(id,userId, fechaInicio,fechaFin, titulo, tituloEs, categoria));
    }



    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        mantenimientoService.deleteMantenimiento(id);

        return Response.noContent().build();
    }
}