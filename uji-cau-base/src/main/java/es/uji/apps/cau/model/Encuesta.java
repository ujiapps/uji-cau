package es.uji.apps.cau.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "CAU_ENCUESTAS")
public class Encuesta
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PARTE_ID")
    private ParteVW parte;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    private Integer atencion;
    private Integer informacion;
    private Integer solucion;
    private Integer tiempo;
    private String motivo;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ParteVW getParte()
    {
        return parte;
    }

    public void setParte(ParteVW parte)
    {
        this.parte = parte;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Integer getAtencion()
    {
        return atencion;
    }

    public void setAtencion(Integer atencion)
    {
        this.atencion = atencion;
    }

    public Integer getInformacion()
    {
        return informacion;
    }

    public void setInformacion(Integer informacion)
    {
        this.informacion = informacion;
    }

    public Integer getSolucion()
    {
        return solucion;
    }

    public void setSolucion(Integer solucion)
    {
        this.solucion = solucion;
    }

    public Integer getTiempo()
    {
        return tiempo;
    }

    public void setTiempo(Integer tiempo)
    {
        this.tiempo = tiempo;
    }

    public String getMotivo()
    {
        return motivo;
    }

    public void setMotivo(String motivo)
    {
        this.motivo = motivo;
    }
}
