package es.uji.apps.cau.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_VW_PARTES")
public class ParteVW {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "ASUNTO")
    private String asunto;

    @Column(name = "CODIGO_BARRAS")
    private String codigoBarras;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "PRESENTE")
    private Long presente;

    @Column(name = "LEIDO")
    private Integer leido;

    @Column(name = "SUSCRITO")
    private Integer suscrito;

    @Column(name = "EXTENSION_TELEFONO")
    private String extensionTelefono;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "ID_AREA")
    private Long areaId;

    @Column(name = "ID_CATEGORIA")
    private Long categoriaId;

    @Column(name = "PER_ID_ASIGNADO")
    private Long personaAsignadoId;

    @Column(name = "ASIGNADO_NOMBRE_COMPLETO")
    private String asignadoNombreCompleto;

    @Column(name = "ASIGNADO_CUENTA")
    private String asignadoCuenta;

    @Column(name = "ASIGNADO_EXTENSION_TELEFONO")
    private String asignadoExtensionTelefono;

    @Column(name = "ASIGNADO_DEPARTAMENTO")
    private String asignadoDepartamento;

    @Column(name = "ASIGNADO_UBICACION")
    private String asignadoUbicacion;

    @Column(name = "FECHA_ASIGNACION")
    private Date fechaAsignacion;

    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    @Column(name = "PER_ID_CREACION")
    private Long personaCreacionId;

    @Column(name = "CREADOR_NOMBRE_COMPLETO")
    private String creadorNombreCompleto;

    @Column(name = "CREADOR_CUENTA")
    private String creadorCuenta;

    @Column(name = "CREADOR_EXTENSION_TELEFONO")
    private String creadorExtensionTelefono;

    @Column(name = "CREADOR_DEPARTAMENTO")
    private String creadorDepartamento;

    @Column(name = "CREADOR_UBICACION")
    private String creadorUbicacion;

    @Column(name = "PER_ID_SOLICITANTE")
    private Long personaSolicitanteId;

    @Column(name = "SOLICITANTE_NOMBRE_COMPLETO")
    private String solicitanteNombreCompleto;

    @Column(name = "SOLICITANTE_CUENTA")
    private String solicitanteCuenta;

    @Column(name = "SOLICITANTE_EXTENSION_TELEFONO")
    private String solicitanteExtensionTelefono;

    @Column(name = "SOLICITANTE_DEPARTAMENTO")
    private String solicitanteDepartamento;

    @Column(name = "SOLICITANTE_UBICACION")
    private String solicitanteUbicacion;

    @Column(name = "PER_ID_AFECTADO")
    private Long personaAfectadoId;

    @Column(name = "AFECTADO_NOMBRE_COMPLETO")
    private String afectadoNombreCompleto;

    @Column(name = "AFECTADO_CUENTA")
    private String afectadoCuenta;

    @Column(name = "AFECTADO_EXTENSION_TELEFONO")
    private String afectadoExtensionTelefono;

    @Column(name = "AFECTADO_DEPARTAMENTO")
    private String afectadoDepartamento;

    @Column(name = "AFECTADO_UBICACION")
    private String afectadoUbicacion;

    @Column(name = "AFECTADO_TIPO")
    private String afectadoTipo;

    @Column(name = "ID_PRIORIDAD")
    private Long prioridadId;

    @Column(name = "PRIORIDAD_NOMBRE")
    private String prioridadNombre;

    @Column(name = "PRIORIDAD_COLOR")
    private String prioridadColor;

    @Column(name = "ID_UBICACION_FISICA")
    private Long ubicacionFisica;

    @Column(name = "UBICACION_NOMBRE")
    private String ubicacionNombre;

    @Column(name = "ID_IMPACTO")
    private Long impactoId;

    @Column(name = "ID_URGENCIA")
    private Long urgenciaId;

    @Column(name = "PAR_ID_EXTERNO")
    private Long parteExternoId;

    @Column(name = "ESTADO")
    private Long estado;

    @Column(name = "ESTADO_NOMBRE")
    private String estadoNombre;

    @Column(name = "ESTADO_PARENT")
    private Long estadoParent;

    @Column(name = "ESTADO_F_INICIO")
    private Date fechaInicio;

    @Column(name = "ESTADO_F_FIN")
    private Date fechaFin;
    @Column(name = "ESTADO_DESCRIPCION")
    private String estadoDescripcion;

    @Column(name = "IP_INVENTARIO")
    private String ipInventario;

    @Column(name = "TEXTO_RESOLUCION")
    private String textoResolucion;

    @Column(name = "HAS_ATTACHMENT")
    private Integer hasAttachment;

    @Column(name = "NOTIFICA_TECNICO")
    private Integer notificaTecnico;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPresente() {
        return presente;
    }

    public void setPresente(Long presente) {
        this.presente = presente;
    }

    public Integer getLeido()
    {
        return leido;
    }

    public void setLeido(Integer leido)
    {
        this.leido = leido;
    }

    public Integer getSuscrito()
    {
        return suscrito;
    }

    public void setSuscrito(Integer suscrito)
    {
        this.suscrito = suscrito;
    }

    public String getExtensionTelefono() {
        return extensionTelefono;
    }

    public void setExtensionTelefono(String extensionTelefono) {
        this.extensionTelefono = extensionTelefono;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public Long getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(Long categoriaId) {
        this.categoriaId = categoriaId;
    }

    public Long getPersonaAsignadoId() {
        return personaAsignadoId;
    }

    public void setPersonaAsignadoId(Long personaAsignadoId) {
        this.personaAsignadoId = personaAsignadoId;
    }

    public String getAsignadoNombreCompleto() {
        return asignadoNombreCompleto;
    }

    public void setAsignadoNombreCompleto(String asignadoNombreCompleto) {
        this.asignadoNombreCompleto = asignadoNombreCompleto;
    }

    public String getAsignadoCuenta() {
        return asignadoCuenta;
    }

    public void setAsignadoCuenta(String asignadoCuenta) {
        this.asignadoCuenta = asignadoCuenta;
    }

    public String getAsignadoExtensionTelefono() {
        return asignadoExtensionTelefono;
    }

    public void setAsignadoExtensionTelefono(String asignadoExtensionTelefono) {
        this.asignadoExtensionTelefono = asignadoExtensionTelefono;
    }

    public String getAsignadoDepartamento() {
        return asignadoDepartamento;
    }

    public void setAsignadoDepartamento(String asignadoDepartamento) {
        this.asignadoDepartamento = asignadoDepartamento;
    }

    public String getAsignadoUbicacion() {
        return asignadoUbicacion;
    }

    public void setAsignadoUbicacion(String asignadoUbicacion) {
        this.asignadoUbicacion = asignadoUbicacion;
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getPersonaCreacionId() {
        return personaCreacionId;
    }

    public void setPersonaCreacionId(Long personaCreacionId) {
        this.personaCreacionId = personaCreacionId;
    }

    public String getCreadorNombreCompleto() {
        return creadorNombreCompleto;
    }

    public void setCreadorNombreCompleto(String creadorNombreCompleto) {
        this.creadorNombreCompleto = creadorNombreCompleto;
    }

    public String getCreadorCuenta() {
        return creadorCuenta;
    }

    public void setCreadorCuenta(String creadorCuenta) {
        this.creadorCuenta = creadorCuenta;
    }

    public String getCreadorExtensionTelefono() {
        return creadorExtensionTelefono;
    }

    public void setCreadorExtensionTelefono(String creadorExtensionTelefono) {
        this.creadorExtensionTelefono = creadorExtensionTelefono;
    }

    public String getCreadorDepartamento() {
        return creadorDepartamento;
    }

    public void setCreadorDepartamento(String creadorDepartamento) {
        this.creadorDepartamento = creadorDepartamento;
    }

    public String getCreadorUbicacion() {
        return creadorUbicacion;
    }

    public void setCreadorUbicacion(String creadorUbicacion) {
        this.creadorUbicacion = creadorUbicacion;
    }

    public Long getPersonaSolicitanteId() {
        return personaSolicitanteId;
    }

    public void setPersonaSolicitanteId(Long personaSolicitanteId) {
        this.personaSolicitanteId = personaSolicitanteId;
    }

    public String getSolicitanteNombreCompleto() {
        return solicitanteNombreCompleto;
    }

    public void setSolicitanteNombreCompleto(String solicitanteNombreCompleto) {
        this.solicitanteNombreCompleto = solicitanteNombreCompleto;
    }

    public String getSolicitanteCuenta() {
        return solicitanteCuenta;
    }

    public void setSolicitanteCuenta(String solicitanteCuenta) {
        this.solicitanteCuenta = solicitanteCuenta;
    }

    public String getSolicitanteExtensionTelefono() {
        return solicitanteExtensionTelefono;
    }

    public void setSolicitanteExtensionTelefono(String solicitanteExtensionTelefono) {
        this.solicitanteExtensionTelefono = solicitanteExtensionTelefono;
    }

    public String getSolicitanteDepartamento() {
        return solicitanteDepartamento;
    }

    public void setSolicitanteDepartamento(String solicitanteDepartamento) {
        this.solicitanteDepartamento = solicitanteDepartamento;
    }

    public String getSolicitanteUbicacion() {
        return solicitanteUbicacion;
    }

    public void setSolicitanteUbicacion(String solicitanteUbicacion) {
        this.solicitanteUbicacion = solicitanteUbicacion;
    }

    public Long getPersonaAfectadoId() {
        return personaAfectadoId;
    }

    public void setPersonaAfectadoId(Long personaAfectadoId) {
        this.personaAfectadoId = personaAfectadoId;
    }

    public String getAfectadoNombreCompleto() {
        return afectadoNombreCompleto;
    }

    public void setAfectadoNombreCompleto(String afectadoNombreCompleto) {
        this.afectadoNombreCompleto = afectadoNombreCompleto;
    }

    public String getAfectadoCuenta() {
        return afectadoCuenta;
    }

    public void setAfectadoCuenta(String afectadoCuenta) {
        this.afectadoCuenta = afectadoCuenta;
    }

    public String getAfectadoExtensionTelefono() {
        return afectadoExtensionTelefono;
    }

    public void setAfectadoExtensionTelefono(String afectadoExtensionTelefono) {
        this.afectadoExtensionTelefono = afectadoExtensionTelefono;
    }

    public String getAfectadoDepartamento() {
        return afectadoDepartamento;
    }

    public void setAfectadoDepartamento(String afectadoDepartamento) {
        this.afectadoDepartamento = afectadoDepartamento;
    }

    public String getAfectadoUbicacion() {
        return afectadoUbicacion;
    }

    public void setAfectadoUbicacion(String afectadoUbicacion) {
        this.afectadoUbicacion = afectadoUbicacion;
    }

    public Long getPrioridadId() {
        return prioridadId;
    }

    public void setPrioridadId(Long prioridadId) {
        this.prioridadId = prioridadId;
    }

    public String getPrioridadNombre() {
        return prioridadNombre;
    }

    public void setPrioridadNombre(String prioridadNombre) {
        this.prioridadNombre = prioridadNombre;
    }

    public String getPrioridadColor() {
        return prioridadColor;
    }

    public void setPrioridadColor(String prioridadColor) {
        this.prioridadColor = prioridadColor;
    }

    public Long getUbicacionFisica() {
        return ubicacionFisica;
    }

    public void setUbicacionFisica(Long ubicacionFisica) {
        this.ubicacionFisica = ubicacionFisica;
    }

    public String getUbicacionNombre() {
        return ubicacionNombre;
    }

    public void setUbicacionNombre(String ubicacionNombre) {
        this.ubicacionNombre = ubicacionNombre;
    }

    public Long getImpactoId() {
        return impactoId;
    }

    public void setImpactoId(Long impactoId) {
        this.impactoId = impactoId;
    }

    public Long getUrgenciaId() {
        return urgenciaId;
    }

    public void setUrgenciaId(Long urgenciaId) {
        this.urgenciaId = urgenciaId;
    }

    public Long getParteExternoId() {
        return parteExternoId;
    }

    public void setParteExternoId(Long parteExternoId) {
        this.parteExternoId = parteExternoId;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public String getEstadoNombre() {
        return estadoNombre;
    }

    public void setEstadoNombre(String estadoNombre) {
        this.estadoNombre = estadoNombre;
    }

    public Long getEstadoParent() {
        return estadoParent;
    }

    public void setEstadoParent(Long estadoParent) {
        this.estadoParent = estadoParent;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getEstadoDescripcion()
    {
        return estadoDescripcion;
    }

    public void setEstadoDescripcion(String estadoDescripcion)
    {
        this.estadoDescripcion = estadoDescripcion;
    }

    public String getIpInventario()
    {
        return ipInventario;
    }

    public void setIpInventario(String ipInventario)
    {
        this.ipInventario = ipInventario;
    }

    public String getTextoResolucion()
    {
        return textoResolucion;
    }

    public void setTextoResolucion(String textoResolucion)
    {
        this.textoResolucion = textoResolucion;
    }

    public Integer getHasAttachment()
    {
        return hasAttachment;
    }

    public void setHasAttachment(Integer hasAttachment)
    {
        this.hasAttachment = hasAttachment;
    }

    public Integer getNotificaTecnico()
    {
        return notificaTecnico;
    }

    public void setNotificaTecnico(Integer notificaTecnico)
    {
        this.notificaTecnico = notificaTecnico;
    }

    public String getAfectadoTipo()
    {
        return afectadoTipo;
    }

    public void setAfectadoTipo(String afectadoTipo)
    {
        this.afectadoTipo = afectadoTipo;
    }
}
