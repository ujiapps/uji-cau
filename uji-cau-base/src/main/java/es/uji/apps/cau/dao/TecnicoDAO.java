package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.Area;
import es.uji.apps.cau.model.AreaTecnico;
import es.uji.apps.cau.model.QArea;
import es.uji.apps.cau.model.QAreaTecnico;
import es.uji.apps.cau.model.QPersona;
import es.uji.apps.cau.model.QTecnico;
import es.uji.apps.cau.model.Tecnico;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;

@Repository
public class TecnicoDAO extends BaseDAODatabaseImpl
{
    private QTecnico qTecnico = QTecnico.tecnico1;
    private QArea qArea = QArea.area;
    private QPersona qPersona = QPersona.persona;
    private QAreaTecnico qAreaTecnico = QAreaTecnico.areaTecnico;

    public List<Tecnico> getTecnicos(String querySearch)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTecnico).join(qTecnico.tecnico, qPersona).fetch();

        query.where(qTecnico.tecnico.nombreCompletoBusqueda.contains(StringUtils.limpiaAcentos(querySearch).toUpperCase())
                .or(qTecnico.tecnico.cuenta.contains(querySearch)));

        return query.list(qTecnico);
    }

    public List<Area> getAreasByTecnicoId(Long perId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qArea).where(qArea.id.in(new JPASubQuery().from(qAreaTecnico).where(qAreaTecnico.perIdTecnico.eq(perId)).list(qAreaTecnico.areaId)));
        return query.list(qArea);
    }

    public void deleteTecnicoArea(Long tecnicoId, Long areaId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qAreaTecnico);
        deleteClause.where(qAreaTecnico.perIdTecnico.eq(tecnicoId).and(qAreaTecnico.areaId.eq(areaId)));
        deleteClause.execute();
    }

    public Tecnico getTecnicoById(Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTecnico).join(qTecnico.tecnico, qPersona).fetch();


        query.where(qTecnico.tecnico.id.eq(userId));

        return query.singleResult(qTecnico);
    }

    public List<AreaTecnico> getAreasTecnicoById(Long userId)
    {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAreaTecnico);

        query.where(qAreaTecnico.perIdTecnico.eq(userId));

        return query.list(qAreaTecnico);
    }

    public AreaTecnico getAreaTecnico(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAreaTecnico);

        query.where(qAreaTecnico.id.eq(id));

        return query.singleResult(qAreaTecnico);
    }
}
