package es.uji.apps.cau.services.rest;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Tag;
import es.uji.apps.cau.services.TagService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

public class ParteTagResource
{
    @PathParam("id")
    private Long parteId;

    @InjectParam
    private TagService tagService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTag()
    {
        ParamUtils.checkNotNull(parteId);
        return UIEntity.toUI(tagService.getParteAll(parteId));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("buscar")
    public List<UIEntity> getSearchTag(@QueryParam("search") String query)
    {
        return UIEntity.toUI(tagService.getAll(query));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTag(@PathParam("id") Long id)
    {
        return UIEntity.toUI(tagService.getById(id));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity add(@FormParam("tag") String tag)
    {
        return UIEntity.toUI(tagService.addParteTag(parteId,tag));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTag(UIEntity entity)
    {
        Tag tipo = entity.toModel(Tag.class);
        return UIEntity.toUI(tagService.update(tipo));
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteParteTag(@PathParam("id") Long id)
    {
        tagService.deleteParteTag(parteId,id);
        return Response.ok().build();
    }
}