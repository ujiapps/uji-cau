package es.uji.apps.cau.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import es.uji.apps.cau.utils.TipoMensaje;


@Entity
@Table(name = "CAU_MENSAJES")
public class Mensaje
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "mensaje")
    private String mensaje;

    @Column(name = "tipo")
    @Enumerated(value=EnumType.STRING)
    private TipoMensaje tipo;

    @Column(name= "PERSONA_ID")
    private Long personaId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public TipoMensaje getTipo() {
        return tipo;
    }

    public void setTipo(TipoMensaje tipoMensaje) {
        this.tipo = tipoMensaje;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }
}