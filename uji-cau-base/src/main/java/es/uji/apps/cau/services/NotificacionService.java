package es.uji.apps.cau.services;

import es.uji.apps.cau.dao.ElementoInventarioDAO;
import es.uji.apps.cau.exceptions.ErrorAlLeerPlantilla;
import es.uji.apps.cau.exceptions.NotificacionSinDestinatarios;
import es.uji.apps.cau.model.*;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import javax.ws.rs.core.MediaType;
import java.io.FileInputStream;
import java.text.MessageFormat;
import java.util.List;

@Service
public class NotificacionService {

    private static final Logger log = LoggerFactory.getLogger(NotificacionService.class);


    private static final String URL_BASE = "https://ujiapps.uji.es/cau/";
    private static final String URL_BASE_PUBLICA = "https://ujiapps.uji.es/cau/rest/parte/public/";

    private static ElementoInventarioDAO elementoInventarioDAO;

    @Autowired
    public NotificacionService(ElementoInventarioDAO elementoInventarioDAO) {
        this.elementoInventarioDAO = elementoInventarioDAO;
    }

    private static void enviar(List<Persona> destinatarios, String cuerpo, StringBuffer asunto)
            throws MessageNotSentException {
        MailMessage mensaje = new MailMessage("CAU");
        mensaje.setTitle(asunto.toString());
        mensaje.setContentType(MediaType.TEXT_PLAIN);
        mensaje.setSender("CAU <cau@uji.es>");
        mensaje.setReplyTo("noreply@uji.es");
        mensaje.setContent(cuerpo);

        for (Persona persona : destinatarios) {
            mensaje.addToRecipient(persona.getCuenta());
        }

        MessagingClient client = new MessagingClient();
        client.send(mensaje);
    }

    public static void enviarNotificacion(Parte parte, String tipo) {
        try {
            enviarNotificacion(parte, tipo, "");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public static Integer enviarNotificacion(Parte parte, String tipo, String razon)
            throws NotificacionSinDestinatarios, ErrorAlLeerPlantilla {
        String mensaje = "";

        MailMessage email = new MailMessage("CAU");
        email.setSender("CAU <cau@uji.es>");
        email.setReplyTo("noreply@uji.es");
        email.setContentType("text/html; charset=UTF-8");
        if (parte != null) {
            String ub = "";
            String cb = parte.getCodigoBarras();
            String telefono = parte.getExtensionTelefono();
            if (parte.getUbicacionFisica() != null) {
                ub = parte.getUbicacionFisica().getNombre();
            }
            if (!ParamUtils.isNotNull(telefono)) {
                telefono = "";
            }
            if (!ParamUtils.isNotNull(cb)) {
                cb = "";
            }
            if (tipo.equals("ASIGNACIONTECNICO")) {
                String hostname = "";

                if (ParamUtils.isNotNull(parte.getCodigoBarras())) {
                    ElementoInventario e = elementoInventarioDAO.getInventarioByCB(parte.getCodigoBarras());
                    hostname = ParamUtils.isNotNull(e.getNombreDNS()) ? e.getNombreDNS() : "";
                }
                email.addToRecipient(parte.getPersonaAsignado().getCuenta());
                email.setTitle("[CAU] Incidència #" + parte.getId() + "- Tens assignada la incidència ");
                mensaje = cuerpoAsignacionTecnico(parte.getId(),
                        parte.getPrioridad().getNombre(),
                        parte.getPersonaSolicitante().getCuenta(),
                        parte.getPersonaSolicitante().getNombreCompleto(),
                        parte.getPersonaAfectado().getCuenta(),
                        parte.getPersonaAfectado().getNombreCompleto(),
                        cb,
                        ub,
                        telefono,
                        URL_BASE + "?incidencia=" + parte.getId(), parte.getAsunto(), parte.getDescripcion(), hostname);
            } else if (tipo.equals("ASIGNACIONUSUARIO")) {
                email.setTitle("[CAU] Incidència #" + parte.getId() + " - La vostra incidència ha sigut assignada");
                email.addToRecipient(parte.getPersonaAfectado().getCuenta());
                mensaje = cuerpoAsignacionUsuario(parte.getId(), parte.getAsunto(), URL_BASE_PUBLICA + parte.getId());
            } else if (tipo.equals("NUEVA")) {
                email.setTitle("[CAU] Incidència #" + parte.getId() + " - Confirmació de creació d’incidència en el CAU.");
                email.addToRecipient(parte.getPersonaAfectado().getCuenta());
                mensaje = cuerpoNueva(parte.getId(), parte.getAsunto(), URL_BASE_PUBLICA + parte.getId());
            } else if (tipo.equals("REABRIR")) {
                if (parte.getNotificaTecnico().equals(0)) {
                    return 0;
                }
                email.setTitle("[CAU] Incidència #" + parte.getId() + " - Tens reassignada la incidència per que s’ha rebujat per l’usuari.");
                email.addToRecipient(parte.getPersonaAsignado().getCuenta());
                mensaje = cuerpoReabrir(parte.getId(), razon,
                        parte.getPrioridad().getNombre(),
                        parte.getPersonaSolicitante().getCuenta(),
                        parte.getPersonaSolicitante().getNombreCompleto(),
                        parte.getPersonaAfectado().getCuenta(),
                        parte.getPersonaAfectado().getNombreCompleto(),
                        parte.getCodigoBarras(),
                        ub,
                        telefono,
                        URL_BASE + "?incidencia=" + parte.getId()
                );
            } else if (tipo.equals("RESOLVER")) {
                email.setTitle("[CAU] Incidència #" + parte.getId() + " - La vostra incidència ha sigut resolta.");
                email.addToRecipient(parte.getPersonaAfectado().getCuenta());
                mensaje = cuerpoResuelta(parte.getId(), parte.getAsunto(),
                        razon,
                        URL_BASE_PUBLICA + parte.getId());
            } else if (tipo.equals("SOLICITUDUSUARIO")) {
                email.setTitle("[CAU] Incidència #" + parte.getId() + " - Solicitud de información.");
                email.addToRecipient(parte.getPersonaAfectado().getCuenta());
                mensaje = cuerpoSolicitudUsuario(parte.getId(), razon,
                        URL_BASE_PUBLICA + parte.getId());
            } else if (tipo.equals("SOLICITUDTECNICO")) {
                if (parte.getNotificaTecnico().equals(0)) {
                    return 0;
                }
                email.setTitle("[CAU] Incidència #" + parte.getId() + " - Solicitud de información.");
                if (ParamUtils.isNotNull(parte.getPersonaAsignado())) {
                    email.addToRecipient(parte.getPersonaAsignado().getCuenta());
                } else {
                    email.addToRecipient("cau@uji.es");
                }
                mensaje = cuerpoSolicitudTecnico(parte.getId(), razon,
                        URL_BASE + "?incidencia=" + parte.getId());
            } else if (tipo.equals("ENCOLADA")) {
                email.setTitle("[CAU] Incidència #" + parte.getId() + " - La vostra incidència ha sigut tramitada.");
                email.addToRecipient(parte.getPersonaAfectado().getCuenta());
                mensaje = cuerpoEncolada(parte.getId());
            }
        } else {
            throw new NotificacionSinDestinatarios();
        }
        email.setContent(mensaje);
        try {
            MessagingClient emailClient = new MessagingClient();
            emailClient.send(email);

        }catch (Exception e){
            log.error("Error al enviar correo: "+ e.getMessage());
        }
        return 1;
    }

    private static String cuerpoNueva(Long parteId, String asunto, String url) throws ErrorAlLeerPlantilla {
        String cuerpo;

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/nueva.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte nou.");
        }

        return MessageFormat.format(cuerpo, parteId, HtmlUtils.htmlEscape(asunto), url);
    }

    private static String cuerpoAsignacionUsuario(Long parteId, String asunto,
                                                  String url) throws ErrorAlLeerPlantilla {
        String cuerpo;

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/asignacionUsuario.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte asignat.");
        }

        return MessageFormat.format(cuerpo, parteId, HtmlUtils.htmlEscape(asunto), url);
    }

    private static String cuerpoEncolada(Long parteId) throws ErrorAlLeerPlantilla {
        String cuerpo;

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/encolada.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte asignat.");
        }

        return MessageFormat.format(cuerpo, parteId);
    }

    private static String cuerpoAsignacionTecnico(Long parteId, String prioridad,
                                                  String cuentaSolicitante, String nombrePersonaSolicita,
                                                  String cuentaAfectado, String nombrePersonaAfectado,
                                                  String codigoBarras, String ubicacion, String extension,
                                                  String url, String asunto, String descripcion, String hostname)
            throws ErrorAlLeerPlantilla {
        String cuerpo;

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/asignacionTecnico.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte asignat.");
        }

        return MessageFormat.format(cuerpo, parteId, prioridad, cuentaSolicitante, nombrePersonaSolicita, cuentaAfectado,
                nombrePersonaAfectado, codigoBarras, ubicacion, extension, url, HtmlUtils.htmlEscape(asunto), HtmlUtils.htmlEscape(descripcion), hostname);
    }

    private static String cuerpoReabrir(Long parteId, String razon, String prioridad,
                                        String cuentaSolicitante, String nombrePersonaSolicita,
                                        String cuentaAfectado, String nombrePersonaAfectado,
                                        String codigoBarras, String ubicacion, String extension,
                                        String url) throws ErrorAlLeerPlantilla {
        String cuerpo;

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/reabrir.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte reobert.");
        }

        return MessageFormat.format(cuerpo, parteId, HtmlUtils.htmlEscape(razon), prioridad, cuentaSolicitante, nombrePersonaSolicita, cuentaAfectado,
                nombrePersonaAfectado, codigoBarras, ubicacion, extension, url);
    }

    private static String cuerpoResuelta(Long parteId, String asunto, String razon,
                                         String url) throws ErrorAlLeerPlantilla {
        String cuerpo;
        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/encuesta.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte resolt.");
        }

        return MessageFormat.format(cuerpo, parteId, HtmlUtils.htmlEscape(asunto), HtmlUtils.htmlEscape(razon), url);
    }

    private static String cuerpoSolicitudUsuario(Long parteId, String razon, String url)
            throws ErrorAlLeerPlantilla {
        String cuerpo;

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/solicitudUsuario.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte solicitud.");
        }

        return MessageFormat.format(cuerpo, parteId, HtmlUtils.htmlEscape(razon), url);
    }

    private static String cuerpoSolicitudTecnico(Long parteId, String razon, String url)
            throws ErrorAlLeerPlantilla {
        String cuerpo;

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/solicitudTecnico.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte solicitud.");
        }

        return MessageFormat.format(cuerpo, parteId, HtmlUtils.htmlEscape(razon), url);
    }

    public static Integer enviarEncuestaTecnico(ParteVW parte, Encuesta encuesta)
            throws MessageNotSentException, ErrorAlLeerPlantilla {
        String mensaje;

        MailMessage email = new MailMessage("CAU");
        email.setSender("CAU <cau@uji.es>");
        email.setReplyTo("noreply@uji.es");
        email.setContentType("text/html; charset=UTF-8");
        email.setTitle("[CAU] Incidència #" + parte.getId() + " - La incidència ha estat valorada per l'usuari.");
        email.addToRecipient(parte.getAsignadoCuenta());
        mensaje = cuerpoEncuesta(parte.getId(), encuesta);
        email.setContent(mensaje);
        MessagingClient emailClient = new MessagingClient();
        emailClient.send(email);
        return 1;
    }

    private static String cuerpoEncuesta(Long parteId, Encuesta encuesta) throws ErrorAlLeerPlantilla {
        String cuerpo;
        String atencion = transformValue(encuesta.getAtencion());
        String informacion = transformValue(encuesta.getInformacion());
        String solucion = transformValue(encuesta.getSolucion());
        String tiempo = transformValue(encuesta.getTiempo());
        String observaciones = HtmlUtils.htmlEscape(encuesta.getMotivo());

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/cerrar.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte nou.");
        }

        return MessageFormat.format(cuerpo, parteId, atencion, informacion, solucion, tiempo, observaciones);
    }

    private static String transformValue(Integer valor) {
        if (valor == null || valor == 0) {
            return "NS/NC";
        } else if (valor.equals(1)) {
            return "Molt Mala";
        } else if (valor.equals(2)) {
            return "Mala";
        } else if (valor.equals(3)) {
            return "Bona";
        } else if (valor.equals(4)) {
            return "Molt Bona";
        } else {
            return "Excel·lent";
        }

    }

    public static void enviar(List<String> destinatarios, String asunto, String cuerpo)
            throws MessageNotSentException {
        for (String destinatario : destinatarios) {
            MailMessage email = new MailMessage("CAU");
            email.setTitle(asunto);
            email.setSender("CAU <cau@uji.es>");
            email.setReplyTo("noreply@uji.es");
            email.setContentType("text/html; charset=UTF-8");
            email.addToRecipient(destinatario);
            email.setContent(cuerpo);

            MessagingClient emailClient = new MessagingClient();
            emailClient.send(email);
        }

    }

    public static void enviarUno(String destinatario, String asunto, String cuerpo) {
        MailMessage email = new MailMessage("CAU");
        email.setTitle("[CAU]" + asunto);
        email.setSender("CAU <cau@uji.es>");
        email.setReplyTo("noreply@uji.es");
        email.setContentType("text/html; charset=UTF-8");
        email.addToRecipient(destinatario.trim());
        email.setContent(cuerpo);

        try {
            MessagingClient emailClient = new MessagingClient();
            emailClient.send(email);
        } catch (MessageNotSentException e) {
            log.error(e.getMessage(), e);
        }
    }

    public static void enviarSugerencia(Persona persona, String asunto, String mensaje) throws MessageNotSentException {
        MailMessage mailMessage = new MailMessage("CAU");
        mailMessage.setTitle("[CAU] Sugerencia: " + asunto);
        mailMessage.setContentType(MediaType.TEXT_PLAIN);
        mailMessage.setSender(persona.getCuenta());
        mailMessage.setContent(mensaje);
        mailMessage.addToRecipient("cau@uji.es");


        MessagingClient client = new MessagingClient();
        client.send(mailMessage);
    }

    public static void enviarInventario(Persona persona, String nombre, String login, String mail,
                                        String departamento, String ubicacion, String codigoBarras,
                                        String ubicacionEquipo, String tipoEquipo, String marcaEquipo,
                                        String modeloEquipo, String numeroSerie, String fechaCompra,
                                        String fechaAltaUsuario, String descripcion)
            throws MessageNotSentException, ErrorAlLeerPlantilla {
        MailMessage mailMessage = new MailMessage("CAU");
        mailMessage.setTitle("[CAU] Inventario ");
        mailMessage.setContentType(MediaType.TEXT_PLAIN);
        mailMessage.setSender(persona.getCuenta());
        mailMessage.setContent(cuerpoNotificacionInventario(nombre, login, mail, departamento, ubicacion, codigoBarras,
                ubicacionEquipo, tipoEquipo, marcaEquipo, modeloEquipo, numeroSerie, fechaCompra,
                fechaAltaUsuario, descripcion));
        mailMessage.addToRecipient("cau@uji.es");


        MessagingClient client = new MessagingClient();
        client.send(mailMessage);
    }

    private static String cuerpoNotificacionInventario(String nombre, String login, String mail,
                                                       String departamento, String ubicacion, String codigoBarras,
                                                       String ubicacionEquipo, String tipoEquipo, String marcaEquipo,
                                                       String modeloEquipo, String numeroSerie, String fechaCompra,
                                                       String fechaAltaUsuario, String descripcion)
            throws ErrorAlLeerPlantilla {
        String cuerpo;

        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cau/templates/notificacionInventario.template")));
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte solicitud.");
        }

        return MessageFormat.format(cuerpo, nombre, login, mail, departamento, ubicacion, codigoBarras,
                ubicacionEquipo, tipoEquipo, marcaEquipo, modeloEquipo, numeroSerie, fechaCompra,
                fechaAltaUsuario, descripcion);
    }
}
