package es.uji.apps.cau.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ErrorEnBorradoDeDocumentoException extends CoreDataBaseException
{
    public ErrorEnBorradoDeDocumentoException()
    {
        super("No s'ha pogut borrar el document");
    }

    public ErrorEnBorradoDeDocumentoException(String message)
    {
        super(message);
    }
}