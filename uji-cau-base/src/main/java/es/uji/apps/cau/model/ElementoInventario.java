package es.uji.apps.cau.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_EXT_ELEMENTOS_INVENTARIOS")
public class ElementoInventario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CODIGO_BARRAS")
    private String codigoBarras;

    @Column(name = "ID")
    private Long id;

    private String descripcion;

    private String alta;
    private String marca;
    private String modelo;

    @Column(name = "N_SERIE")
    private String numSerie;

    @Column(name = "IP_EQUIPO")
    private String ip;

    @Column(name = "NOMBRE_DNS")
    private String nombreDNS;

    @Column(name = "SIS_SISTEMA")
    private String sistema;

    private String mantenimiento;

    @Column(name = "QUIEN_ADMINISTRA")
    private String administrado;

    @Column(name = "USU_ADMIN")
    private String administrador;

    @Column(name = "EMPRESA_MANTENIMENTO")
    private String empresaMantenimiento;

    @Column(name = "DESCRIPCION_CONTRATO")
    private String descripcionContrato;

    @Column(name = "ID_UBICACION_FISICA")
    private String ubicacionFisica;

    public ElementoInventario() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoBarras() {
        return this.codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUbicacionFisica() {
        return this.ubicacionFisica;
    }

    public void setUbicacionFisica(String cauExtUbicacionesFisica) {
        this.ubicacionFisica = cauExtUbicacionesFisica;
    }

    public String getAlta() {
        return alta;
    }

    public void setAlta(String alta) {
        this.alta = alta;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getMantenimiento() {
        return mantenimiento;
    }

    public void setMantenimiento(String mantenimiento) {
        this.mantenimiento = mantenimiento;
    }

    public String getAdministrado() {
        return administrado;
    }

    public void setAdministrado(String administrado) {
        this.administrado = administrado;
    }

    public String getEmpresaMantenimiento() {
        return empresaMantenimiento;
    }

    public void setEmpresaMantenimiento(String empresaMantenimiento) {
        this.empresaMantenimiento = empresaMantenimiento;
    }

    public String getDescripcionContrato() {
        return descripcionContrato;
    }

    public void setDescripcionContrato(String descripcionContrato) {
        this.descripcionContrato = descripcionContrato;
    }

    public String getNombreDNS() {
        return nombreDNS;
    }

    public void setNombreDNS(String nombreDNS) {
        this.nombreDNS = nombreDNS;
    }

    public String getAdministrador() {
        return administrador;
    }

    public void setAdministrador(String administrador) {
        this.administrador = administrador;
    }
}
