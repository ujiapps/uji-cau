package es.uji.apps.cau.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;

import es.uji.apps.cau.model.ParteComunicacion;
import es.uji.apps.cau.model.QParte;
import es.uji.apps.cau.model.QParteComunicacion;
import es.uji.apps.cau.utils.DynamicFilter;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.apps.cau.utils.ItemSort;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ParteComunicacionDAO extends BaseDAODatabaseImpl
{
    QParte qParte = QParte.parte;
    QParteComunicacion qParteComunicacion = QParteComunicacion.parteComunicacion1;


    public List<ParteComunicacion> getPublicParteComunicados(Long parteId, Long userId,
                                                             List<ItemFilter> filterList, List<ItemSort> sortList) {
        JPAQuery query = new JPAQuery(entityManager);

        if (userId != null && parteId != null) {
            query.from(qParteComunicacion)
                    .leftJoin(qParteComunicacion.parteComunicacion, qParte)
                    .where(qParteComunicacion.parteComunicacion.id.eq(parteId).and(
                            qParteComunicacion.interno.ne((long) 1)).and(qParte.personaAfectado.id.eq(userId)
                            .or(qParte.personaSolicitante.id.eq(userId))));
        }
        Map<String, Object> validSorts = new HashMap<String, Object>();

        validSorts.put("fechaCreacion", qParteComunicacion.fechaCreacion);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts)) {
            query.orderBy(o);
        }

        return query.list(qParteComunicacion);

    }

    public List<ParteComunicacion> getParteComunicados(Long parteId, Long userId,
                                                       List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (userId != null && parteId != null)
        {
                query.from(qParteComunicacion)
                        .leftJoin(qParteComunicacion.parteComunicacion, qParte)
                        .where(qParteComunicacion.parteComunicacion.id.eq(parteId));

        }
        Map<String, Object> validSorts = new HashMap<String, Object>();

        validSorts.put("fechaCreacion", qParteComunicacion.fechaCreacion);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }


        return query.list(qParteComunicacion);

    }
}
