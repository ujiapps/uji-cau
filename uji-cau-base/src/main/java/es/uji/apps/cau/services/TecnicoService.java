package es.uji.apps.cau.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cau.dao.TecnicoDAO;
import es.uji.apps.cau.model.Area;
import es.uji.apps.cau.model.AreaTecnico;
import es.uji.apps.cau.model.Tecnico;

@Service
public class TecnicoService
{
    private TecnicoDAO tecnicoDAO;

    @Autowired
    public TecnicoService(TecnicoDAO tecnicoDAO)
    {
        this.tecnicoDAO = tecnicoDAO;
    }

    @Transactional
    public Tecnico insertTecnico(Tecnico tecnico)
    {
        return tecnicoDAO.insert(tecnico);
    }

    @Transactional
    public Tecnico updateTecnico(Tecnico tecnico)
    {
        return tecnicoDAO.update(tecnico);
    }

    public List<Tecnico> getTecnicos(String querySearch)
    {
        return tecnicoDAO.getTecnicos(querySearch);
    }

    public List<Area> getAreasByTecnicoId(Long perId)
    {
        return tecnicoDAO.getAreasByTecnicoId(perId);
    }

    public List<Long> getIdAreasByTecnico(Long perId)
    {
        return tecnicoDAO.getAreasTecnicoById(perId)
                .stream().map(AreaTecnico::getAreaId).collect(Collectors.toList());
    }

    @Transactional
    public AreaTecnico insertTecnico(AreaTecnico areaTecnico)
    {
        return tecnicoDAO.insert(areaTecnico);
    }

    @Transactional
    public AreaTecnico updateTecnico(AreaTecnico areaTecnico)
    {
        return tecnicoDAO.update(areaTecnico);
    }


    @Transactional
    public void deleteTecnicoArea(Long id)
    {
        tecnicoDAO.delete(AreaTecnico.class, id);
    }

    public Tecnico getTecnicoById(Long userId)
    {
        return tecnicoDAO.getTecnicoById(userId);
    }

    public List<AreaTecnico> getAreasTecnico(Long perId)
    {
        return tecnicoDAO.getAreasTecnicoById(perId);

    }

    public AreaTecnico getAreaTecnico(Long id)
    {
        return tecnicoDAO.getAreaTecnico(id);
    }
}
