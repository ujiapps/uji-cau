package es.uji.apps.cau.services;

import es.uji.apps.cau.dao.HorarioAtencionDAO;
import es.uji.apps.cau.model.HorariosAtencion;
import es.uji.apps.cau.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
@Service
public class HorariosAtencionService {

    private HorarioAtencionDAO horarioAtencionDAO;

    @Autowired
    public HorariosAtencionService(HorarioAtencionDAO horarioAtencionDAO) {
        this.horarioAtencionDAO = horarioAtencionDAO;
    }

    public List<HorariosAtencion> getHorariosAtencion() {
        return  horarioAtencionDAO.getHorariosAtencion();
    }


    public HorariosAtencion addHorarios(String fechaInicio, String fechaFin, String horaInicio, String horaFin, List<String> diasSemana) {
        HorariosAtencion horariosAtencion = new HorariosAtencion();
        horariosAtencion.setFechaInicio(DateUtils.formatHorarioFecha(fechaInicio));
        horariosAtencion.setFechaFin(DateUtils.formatHorarioFecha(fechaFin));
        horariosAtencion.setHoraInicio(DateUtils.formatHorarioHora(horaInicio));
        horariosAtencion.setHoraFin(DateUtils.formatHorarioHora(horaFin));
        horariosAtencion.setDiasSemana(diasSemana.stream().sorted().collect(Collectors.joining(",")));

        return horarioAtencionDAO.insert(horariosAtencion);
    }

    public HorariosAtencion updateHorarios(Long id, String fechaInicio, String fechaFin, String horaInicio, String horaFin, List<String> diasSemana) {
        HorariosAtencion horariosAtencion = horarioAtencionDAO.getById(id);
        horariosAtencion.setFechaInicio(DateUtils.formatHorarioFecha(fechaInicio));
        horariosAtencion.setFechaFin(DateUtils.formatHorarioFecha(fechaFin));
        horariosAtencion.setHoraInicio(DateUtils.formatHorarioHora(horaInicio));
        horariosAtencion.setHoraFin(DateUtils.formatHorarioHora(horaFin));
        horariosAtencion.setDiasSemana(diasSemana.stream().sorted().collect(Collectors.joining(",")));

        return horarioAtencionDAO.update(horariosAtencion);
    }

    public void deleteHorarioAtencion(Long id) {
        horarioAtencionDAO.delete(HorariosAtencion.class, id);
    }

}