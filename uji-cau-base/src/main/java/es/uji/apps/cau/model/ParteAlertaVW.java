package es.uji.apps.cau.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_VW_PARTES_ALERTAS")
public class ParteAlertaVW {

    @Id
    @Column(name = "PARTE_ID")
    private Long parteId;
    @Column(name = "NIVEL")
    private Long nivel;

    @Column(name= "PER_ID_ASIGNADO")
    private Long asignadoPerId;

    @Column(name= "ASIGNADO_CUENTA")
    private String asignadoCuenta;

    public Long getParteId() {
        return parteId;
    }

    public void setParteId(Long parteId) {
        this.parteId = parteId;
    }

    public Long getNivel() {
        return nivel;
    }

    public void setNivel(Long nivel) {
        this.nivel = nivel;
    }

    public String getAsignadoCuenta() {
        return asignadoCuenta;
    }

    public void setAsignadoCuenta(String asignadoCuenta) {
        this.asignadoCuenta = asignadoCuenta;
    }

    public Long getAsignadoPerId() {
        return asignadoPerId;
    }

    public void setAsignadoId(Long asignadoPerId) {
        this.asignadoPerId = asignadoPerId;
    }
}
