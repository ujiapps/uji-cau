package es.uji.apps.cau.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Area;
import es.uji.apps.cau.model.Categoria;
import es.uji.apps.cau.services.CategoriaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("categoria")
public class CategoriaResources extends CoreBaseService
{
    @InjectParam
    private CategoriaService categoriaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCategorias()
    {
        List<Categoria> categoriaList = categoriaService.getCategorias();
        return ModelToUI(categoriaList);
    }

    @GET
    @Path("{categoriaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCategoriaById(@PathParam("categoriaId") Long categoriaId)
    {
        Categoria categoria = categoriaService.getCategoriaById(categoriaId);
        return ModelToUI(categoria);
    }

    @GET
    @Path("area/{areaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCategoriasByAreaId(@PathParam("areaId") Long areaId, @QueryParam("all") @DefaultValue("false") Boolean all)
    {
        List<Categoria> categoriaList = categoriaService.getCategoriasByAreaId(areaId,all);
        return ModelToUI(categoriaList);
    }

    @GET
    @Path("area/{areaId}/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAllCategoriasByAreaId(@PathParam("areaId") Long areaId)
    {
        List<Categoria> categoriaList = categoriaService.getCategoriasByAreaId(areaId,true);
        return ModelToUI(categoriaList);
    }

    @POST
    @Path("area/{areaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertCategoriaByAreaId(@PathParam("areaId") Long areaId,UIEntity entity)
    {

        Categoria categoria = entity.toModel(Categoria.class);
        Area area= new Area();
        area.setId(areaId);
        categoria.setArea(area);
        categoriaService.insertCategoria(categoria);
        return UIEntity.toUI(categoria);
    }

    @PUT
    @Path("area/{areaId}/{categoriaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCategoriaByAreaId(@PathParam("areaId") Long areaId,UIEntity entity)
    {
        Categoria categoria = entity.toModel(Categoria.class);
        categoriaService.updateCategoria(categoria);
        return UIEntity.toUI(categoria);
    }

    @DELETE
    @Path("area/{categoriaId}")
    public Response deleteCategoria(@PathParam("categoriaId") Long id)
    {

        categoriaService.deleteCategoria(id);
        return Response.noContent().build();
    }

    private UIEntity ModelToUI(Categoria categoria)
    {

        UIEntity entity = UIEntity.toUI(categoria);

        entity.put("alta", categoria.getAlta());

        return entity;
    }

    private List<UIEntity> ModelToUI(List<Categoria> categorias)
    {

        List<UIEntity> uiEntities = new ArrayList<UIEntity>();

        for (Categoria categoria : categorias)
        {
            uiEntities.add(ModelToUI(categoria));
        }
        return uiEntities;
    }

    /*private Categoria UIToModel(UIEntity entity)
    {
        Categoria categoria = entity.toModel(Categoria.class);

        Area area = new Area();
        area.setId(entity.getLong("areaId"));
        categoria.setArea(area);

        return categoria;
    }*/
}
