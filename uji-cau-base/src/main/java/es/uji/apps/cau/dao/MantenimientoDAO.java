package es.uji.apps.cau.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.cau.model.Mantenimiento;
import es.uji.apps.cau.model.QMantenimiento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class MantenimientoDAO extends BaseDAODatabaseImpl {

    private QMantenimiento qMantenimiento = QMantenimiento.mantenimiento;

    public List<Mantenimiento> getMantenimientos() {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qMantenimiento).list(qMantenimiento);
    }

    public Mantenimiento getById(Long id) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMantenimiento).where(qMantenimiento.id.eq(id)).uniqueResult(qMantenimiento);
    }

    public List<Mantenimiento> getMantenimientosActivos(){
        Date hoy = new Date();
        return new JPAQuery(entityManager)
                .from(qMantenimiento)
                .where(qMantenimiento.fechaInicio.loe(hoy)
                        .and(qMantenimiento.fechaFin.goe(hoy)))
                .list(qMantenimiento);
    }
}
