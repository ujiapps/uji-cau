package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.ParteFichero;
import es.uji.apps.cau.model.QParte;
import es.uji.apps.cau.model.QParteFichero;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ParteFicheroDAO extends BaseDAODatabaseImpl
{
    private QParteFichero qParteFichero = QParteFichero.parteFichero1;
    private QParte qParte = QParte.parte;

    public List<ParteFichero> getParteFicheros(Long parteId, Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (userId != null && parteId != null)
        {
            query.from(qParteFichero).leftJoin(qParteFichero.parteFichero, qParte)
                    .where(qParteFichero.parteFichero.id.eq(parteId));
        }
        return query
                .list(QParteFichero.create(qParteFichero.id,qParteFichero.nombreFichero,qParteFichero.mime));

    }

    public ParteFichero getParteFicheroById(Long ficheroId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (ficheroId != null)
        {
            query.from(qParteFichero).where(qParteFichero.id.eq(ficheroId));
        }

        List<ParteFichero> parteList = query.list(qParteFichero);
        if (parteList.size() > 0)
        {
            return parteList.get(0);
        }
        return null;

    }
}
