package es.uji.apps.cau.exceptions;

public class CodigoDeBarrasNoNumericoException extends GeneralCAUException
{
    public CodigoDeBarrasNoNumericoException()
    {
        super("El código de barras debe ser numerico");
    }

    public CodigoDeBarrasNoNumericoException(String message)
    {
        super(message);
    }
}
