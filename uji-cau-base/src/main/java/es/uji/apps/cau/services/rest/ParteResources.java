package es.uji.apps.cau.services.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.FormDataParam;

import es.uji.apps.cau.exceptions.AccesoNoPermitido;
import es.uji.apps.cau.exceptions.CodigoDeBarrasIncorrectoException;
import es.uji.apps.cau.exceptions.CodigoDeBarrasNoNumericoException;
import es.uji.apps.cau.exceptions.ErrorAlLeerPlantilla;
import es.uji.apps.cau.exceptions.NotificacionSinDestinatarios;
import es.uji.apps.cau.model.Area;
import es.uji.apps.cau.model.Categoria;
import es.uji.apps.cau.model.ElementoInventario;
import es.uji.apps.cau.model.Parte;
import es.uji.apps.cau.model.ParteComunicacion;
import es.uji.apps.cau.model.ParteEstado;
import es.uji.apps.cau.model.ParteFichero;
import es.uji.apps.cau.model.ParteVW;
import es.uji.apps.cau.model.Persona;
import es.uji.apps.cau.model.TecnicoArea;
import es.uji.apps.cau.model.TipoEstado;
import es.uji.apps.cau.model.TipoPrioridad;
import es.uji.apps.cau.model.UbicacionFisica;
import es.uji.apps.cau.services.ElementoInventarioService;
import es.uji.apps.cau.services.EncuestaService;
import es.uji.apps.cau.services.NotificacionService;
import es.uji.apps.cau.services.PartesComunicacionService;
import es.uji.apps.cau.services.PartesEstadoService;
import es.uji.apps.cau.services.PartesFicheroService;
import es.uji.apps.cau.services.PartesService;
import es.uji.apps.cau.services.PersonaService;
import es.uji.apps.cau.services.TecnicoAreaService;
import es.uji.apps.cau.services.TecnicoService;
import es.uji.apps.cau.services.TiposService;
import es.uji.apps.cau.services.UbicacionFisicaService;
import es.uji.apps.cau.ui.ComunicacionUI;
import es.uji.apps.cau.ui.ParteUI;
import es.uji.apps.cau.ui.ResponseItemUI;
import es.uji.apps.cau.ui.ResponseListUI;
import es.uji.apps.cau.utils.Estados;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.apps.cau.utils.ItemSort;
import es.uji.apps.cau.utils.ParteLeido;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.model.FieldValidationException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.GrupoMenu;
import es.uji.commons.web.template.model.ItemMenu;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;

@Path("parte")
public class ParteResources extends CoreBaseService {
    public final static Logger log = LoggerFactory.getLogger(ParteResources.class);

    @Context
    private ServletContext servletContext;

    @InjectParam
    private PartesService partesService;

    @InjectParam
    private PartesEstadoService partesEstadoService;

    @InjectParam
    private PartesFicheroService partesFicheroService;

    @InjectParam
    private PartesComunicacionService partesComunicacionService;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private TecnicoAreaService tecnicoAreaService;

    @InjectParam
    private TecnicoService tecnicoService;

    @InjectParam
    private ElementoInventarioService elementoInventarioService;

    @InjectParam
    private UbicacionFisicaService ubicacionFisicaService;

    @InjectParam
    private NotificacionService notificacionService;

    @InjectParam
    private TiposService tiposService;

    @InjectParam
    private EncuestaService encuestaService;

    @Path("{id}/tags")
    public ParteTagResource getPlatformItem(
            @InjectParam ParteTagResource parteTagResource) {
        return parteTagResource;
    }

    private List<UIEntity> toUI(List<Parte> parteList) {
        List<UIEntity> parteListUI = new ArrayList<>();

        for (Parte parte : parteList) {
            UIEntity parteUI = toUI(parte);
            parteListUI.add(parteUI);
        }

        return parteListUI;
    }

    public UIEntity toUI(Parte parte) {
        UIEntity parteUI = UIEntity.toUI(parte);

        setPersona(parteUI, parte.getPersonaSolicitante(), "solicitante");
        setPersona(parteUI, parte.getPersonaAfectado(), "afectado");
        setPersona(parteUI, parte.getPersonaCreacion(), "creador");

        if (parte.getPersonaAsignado() != null) {
            setPersona(parteUI, parte.getPersonaAsignado(), "asignado");
            parteUI.put("fechaAsignacion", partesEstadoService.getFechaAsignacion(parte.getId()));

        }

        UbicacionFisica ub = parte.getUbicacionFisica();
        if (ub != null) {
            parteUI.put("ubicacionFisica", ub.getNombre());
        }
        TipoPrioridad prioridad = parte.getPrioridad();
        if (prioridad != null) {
            parteUI.put("colorPrioridad", parte.getPrioridad().getColor());
            parteUI.put("nombrePrioridad", parte.getPrioridad().getNombre());

        }

        parteUI.put("nombreCompleto", parte.getPersonaSolicitante().getNombreCompleto());
        parteUI.put("fechaCreacion", partesEstadoService.getParteEstadoCreacion(parte.getId())
                .getFechaInicio());

        ParteEstado estadoParte = partesEstadoService.getEstadoActual(parte.getId());

        parteUI.put("nombreCompleto", parte.getPersonaSolicitante().getNombreCompleto());
        parteUI.put("estado", estadoParte.getTipoEstado().getId());
        parteUI.put("estadoNombre", estadoParte.getTipoEstado().getNombre());
        parteUI.put("fechaInicio", estadoParte.getFechaInicio());
        parteUI.put("fechaFin", estadoParte.getFechaFin());
        return parteUI;

    }

    private void setPersona(UIEntity parteUI, Persona persona, String tipo) {
        if (persona != null) {
            parteUI.put(tipo + "NombreCompleto", persona.getNombreCompleto());
            parteUI.put(tipo + "Cuenta", persona.getCuenta());
            if (persona.getUbicacionFisica() != null) {
                parteUI.put(tipo + "Ubicacion", persona.getUbicacionFisica().getNombre());
                parteUI.put(tipo + "Departamento", persona.getUbicacionFisica()
                        .getNombreDepartamento());
            }
            if (persona.getExtensionTelefono() != null) {
                parteUI.put(tipo + "Extension", persona.getExtensionTelefono());
            }
        }
    }

    @GET
    @Path("public/listado")
    @Produces(MediaType.TEXT_HTML)
    public Template getPartesListado(@QueryParam("filter") @DefaultValue("") String filter,
                                     @QueryParam("sort") @DefaultValue("") String sort,
                                     @QueryParam("page") @DefaultValue("0") Integer page,
                                     @QueryParam("limit") @DefaultValue("25") Integer limit,
                                     @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException,
            ParseException {
        Long userId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        ItemSort itemSort = new ItemSort("estado.fechaInicio", "ASC");
        sortList.add(itemSort);
        Pageable pageRequest = new PageRequest(page, limit);

        Page<ParteVW> parteList = partesService.getUserPublicPartesVW(userId, filterList, sortList,
                pageRequest);

        Template template = new HTMLTemplate("cau/base", new Locale(idioma), "cau");
        template.put("pagina", getPaginaPublicacion(idioma));
        template.put("seccion", "cau/listado");
        template.put("list", parteList);
        template.put("filter", URLEncoder.encode(filter, StandardCharsets.UTF_8.toString()));
        template.put("sort", sort);
        template.put("limit", limit);
        template.put("busqueda", true);
        template.put("firstPage", parteList.isFirst());
        template.put("lastPage", parteList.isLast());
        template.put("hasPreviousPage", parteList.hasPrevious());
        template.put("hasNextPage", parteList.hasNext());

        return template;
    }

    private Pagina getPaginaPublicacion(String idioma) throws ParseException {
        GrupoMenu grupo = new GrupoMenu("Centro Atención a Usuarios");
        grupo.addItem(new ItemMenu("Crear incidencía", "/cau/rest/parte/public/new"));
        grupo.addItem(new ItemMenu("Llistat incidencía", "/cau/rest/parte/public/listado"));
        grupo.addItem(new ItemMenu("Fes suggeriments", "/cau/rest/parte/public/sugerencia"));

        Menu menu = new Menu();
        menu.addGrupo(grupo);

        Pagina pagina = new Pagina("", "", idioma, "CAU");
        pagina.setTitulo("CAU");
        pagina.setMenu(menu);

        return pagina;
    }

    @GET
    @Path("public/new")
    @Produces(MediaType.TEXT_HTML)
    public Template newParte(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaById(userId);

        Template template = new HTMLTemplate("cau/base", new Locale(idioma), "cau");
        template.put("pagina", getPaginaPublicacion(idioma));
        template.put("seccion", "cau/new");
        template.put("nombreCompleto", persona.getNombreCompleto());
        template.put("userId", persona.getId());
        template.put("busqueda", false);
        template.put("apellido1", persona.getApellido1());
        template.put("apellido2", persona.getApellido2());
        template.put("cuentaValida", ParamUtils.isNotNull(persona.getCuenta()));

        return template;
    }

    @GET
    @Path("public/sugerencia")
    @Produces(MediaType.TEXT_HTML)
    public Template getSugerencias(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        Template template = new HTMLTemplate("cau/base", new Locale(idioma), "cau");
        template.put("pagina", getPaginaPublicacion(idioma));
        template.put("busqueda", false);
        template.put("seccion", "cau/sugerencia");

        return template;
    }

    @GET
    @Path("public/{parteId}")
    @Produces(MediaType.TEXT_HTML)
    public Template showParte(@PathParam("parteId") Long parteId,
                              @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException, AccesoNoPermitido, RegistroNoEncontradoException {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaById(userId);

        Parte parte = partesService.getParteByIdAndUserId(parteId, userId);
        if (parte != null) {
            parte.setLeido(ParteLeido.update(parte.getLeido(), true));
            parte = partesService.updateParte(parte);
        } else {
            throw new AccesoNoPermitido();
        }
        ParteVW parteVW = partesService.getParteVWById(userId, parte.getId());
        ParteEstado parteEstado = partesEstadoService.getEstadoActual(parteId);
        Template template = new HTMLTemplate("cau/base", new Locale(idioma), "cau");
        template.put("pagina", getPaginaPublicacion(idioma));
        template.put("seccion", "cau/parte");
        template.put("userId", persona.getId());
        template.put("parte", parteVW);
        template.put("busqueda", false);
        template.put("parteEstadoId", parteEstado.getTipoEstado().getId());
        template.put("parteEstadoDesc", parteEstado.getDescripcion());
        template.put("parteEstadoNombre", parteEstado.getTipoEstado().getNombre());
        template.put("apellido1", persona.getApellido1());
        template.put("apellido2", persona.getApellido2());

        return template;
    }

    @PUT
    @Path("public/{parteId}/suscribir")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage suscribir(@PathParam("parteId") Long parteId, @FormParam("suscrito") Integer suscrito) {
        Long userId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(suscrito);
        Parte parte = partesService.getParteByIdAndUserId(parteId, userId);
        if (parte != null) {
            parte.setSuscrito(suscrito);
            partesService.updateParte(parte);
            return new ResponseMessage(true);
        }
        return new ResponseMessage(false);
    }

    @POST
    @Path("public/{parteId}/rechazar")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response rechazarPartePublic(@PathParam("parteId") Long parteId, UIEntity entity)
            throws URISyntaxException, FieldValidationException, ErrorAlLeerPlantilla,
            MessageNotSentException, NotificacionSinDestinatarios {
        Long userId = AccessManager.getConnectedUserId(request);
        String rechazar = entity.get("rechazarMotivo");
        if (rechazar == null || rechazar.length() <= 0) {
            rechazar = "Incidència rebutjada";
        }
        Parte parte = partesService.getParteByIdAndUserId(parteId, userId);

        Persona persona = new Persona();
        persona.setId(userId);

        partesEstadoService.changeParteEstado(parte, persona, Estados.RECHAZADO.getValue(),
                "Incidència rebutjada: " + rechazar);

        NotificacionService.enviarNotificacion(parte, "REABRIR", rechazar);
        ParteComunicacion parteComunicacion = new ParteComunicacion();

        parteComunicacion.setDescripcion("Incidència rebutjada: " + rechazar);
        parteComunicacion.setFechaCreacion(new Date());
        parteComunicacion.setPersonaCreacion(persona);
        parteComunicacion.setParteComunicacion(parte);
        parteComunicacion.setInterno(0L);

        partesComunicacionService.insertParteComunicacion(parteComunicacion);


        return Response.status(Response.Status.SEE_OTHER).location(new URI("parte/public/listado"))
                .build();
    }

    @POST
    @Path("public/{parteId}/cerrar")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response cerrarPartePublic(@PathParam("parteId") Long parteId,
                                      @FormParam("atencion") Integer atencion,
                                      @FormParam("informacion") Integer informacion,
                                      @FormParam("solucion") Integer solucion,
                                      @FormParam("tiempo") Integer tiempo,
                                      @FormParam("motivo") String motivo
    ) throws
            URISyntaxException, AccesoNoPermitido {
        Long userId = AccessManager.getConnectedUserId(request);
        Parte parte = partesService.getParteByIdAndUserId(parteId, userId);
        TipoEstado tipo = partesEstadoService.getEstadoActual(parte.getId()).getTipoEstado();

        if (parte != null && tipo.getId().equals(Estados.RESUELTO.getValue())) {
            Persona persona = new Persona();
            persona.setId(userId);
            partesEstadoService.changeParteEstado(parte, persona, Estados.CERRADO.getValue(),
                    "Incidència tancada");
            if (ParamUtils.isNotNull(motivo)) {
                encuestaService.addEncuestas(parte, userId, atencion, informacion, solucion, tiempo, motivo);
            }
        } else {
            throw new AccesoNoPermitido();
        }

        return Response.status(Response.Status.SEE_OTHER).location(new URI("parte/public/listado"))
                .build();
    }

    @POST
    @Path("public")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response addPublicParte(@FormDataParam("asunto") String asunto,
                                   @FormDataParam("descrip") String desc, @FormDataParam("cb") String cb,
                                   @FormDataParam("presente") @DefaultValue("0") Long presente,
                                   @FormDataParam("afectadoId") String afectadoId,
                                   @FormDataParam("telefonoContacto") String telefonoContacto,
                                   FormDataMultiPart multiPart)
            throws IOException, URISyntaxException, CodigoDeBarrasIncorrectoException,
            CodigoDeBarrasNoNumericoException, AccesoNoPermitido {
        Long userId = AccessManager.getConnectedUserId(request);

        Persona persona = personaService.getPersonaById(userId);

        Parte parte = new Parte();
        parte.setAsunto(asunto);
        parte.setDescripcion(desc);
        if (ParamUtils.isNotNull(telefonoContacto)) {
            parte.setDescripcion(desc.concat("\n\nTeléfon Contacte:\n").concat(telefonoContacto));
        }
        parte.setCodigoBarras(cb);
        parte.setPresente(presente);
        parte.setPersonaCreacion(persona);
        parte.setLeido(ParteLeido.NOLEIDO.getValue());
        parte.setSuscrito(1);
        parte.setNotificaTecnico(1);
        parte.setTipo("1");
        parte.setUrgencia(tiposService.getTipoUrgencia(0L));
        parte.setImpacto(tiposService.getTipoImpacto(0L));
        parte.setPrioridad(tiposService.getTipoPrioridad(0L));
        if (afectadoId != null && !afectadoId.isEmpty()) {
            parte.setPersonaSolicitante(persona);
            parte.setPersonaAfectado(personaService.getPersonaById(Long.parseLong(afectadoId)));
        } else {
            parte.setPersonaAfectado(persona);
            parte.setPersonaSolicitante(persona);
        }
        String extension = parte.getPersonaAfectado().getExtensionTelefono();
        UbicacionFisica ubicacion = parte.getPersonaAfectado().getUbicacionFisica();
        if (extension != null) {
            parte.setExtensionTelefono(extension);
        }
        if (ubicacion != null) {
            parte.setUbicacionFisica(ubicacion);
        }

        parte = partesService.insertParte(parte);

        partesEstadoService.iniciarParteEstado(parte, persona, Estados.NUEVO.getValue(),
                "Incidència Creada");

        List<FormDataBodyPart> fields = multiPart.getFields("datafile");
        if (fields != null) {
            for (FormDataBodyPart field : fields) {
                String nombre = field.getFormDataContentDisposition().getFileName();
                if (!nombre.isEmpty() && nombre.length() >= 1) {
                    ParteFichero parteFichero = new ParteFichero();

                    InputStream file = field.getValueAs(InputStream.class);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    int read;
                    byte[] buf = new byte[4096];
                    while ((read = file.read(buf)) != -1) {
                        byteArrayOutputStream.write(buf, 0, read);
                    }
                    parteFichero.setNombreFichero(field.getFormDataContentDisposition()
                            .getFileName());
                    parteFichero.setParteFichero(parte);
                    parteFichero.setMime(field.getMediaType().toString());
                    parteFichero.setFichero(byteArrayOutputStream.toByteArray());

                    partesFicheroService.insertFichero(parteFichero);
                }
            }
        }
//        NotificacionService.enviarNotificacion(parte, "NUEVA");
        return Response.status(Response.Status.SEE_OTHER).location(new URI("parte/public/listado"))
                .build();
    }

    @GET
    @Path("public/{parteId}/comunicaciones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPublicParteComunicaciones(
            @QueryParam("filter") @DefaultValue("") String filter,
            @PathParam("parteId") Long parteId)
            throws IOException {
        Long userId = AccessManager.getConnectedUserId(request);
        List<UIEntity> parteListUI = new ArrayList<>();
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        ItemSort sort = new ItemSort("fechaCreacion", "ASC");
        List<ItemSort> sortList = new ArrayList<>();
        sortList.add(sort);

        List<ParteComunicacion> parteComunicacionList = partesComunicacionService
                .getPublicParteComunicados(parteId, userId, filterList, sortList);

        for (ParteComunicacion parteComunicacion : parteComunicacionList) {
            UIEntity parteUI = UIEntity.toUI(parteComunicacion);

            parteUI.put("nombreCompleto", parteComunicacion.getPersonaCreacion()
                    .getNombreCompleto());

            parteListUI.add(parteUI);
        }

        return parteListUI;

    }


    @GET
    @Path("user/{afectadoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPartesByUser(@PathParam("afectadoId") String afectadoId,
                                          @QueryParam("filter") @DefaultValue("") String filter,
                                          @QueryParam("sort") @DefaultValue("") String sort)
            throws IOException, ParseException {
        Long userId = AccessManager.getConnectedUserId(request);
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        if (ParamUtils.isNotNull(afectadoId)) {
            ItemFilter i = new ItemFilter("personaAfectado", afectadoId);
            filterList.add(i);
        }
        List<ParteVW> parteVWList = partesService.getParteByUserAfectado(userId, filterList, sortList);
        List<UIEntity> entityList = new ArrayList<>();
        for (ParteVW parteVW : parteVWList) {
            UIEntity ui = UIEntity.toUI(parteVW);
            if (ParamUtils.isNotNull(parteVW.getCodigoBarras())) {
                ElementoInventario elementoInventario = elementoInventarioService.getExtElementoInventarioCB(parteVW.getCodigoBarras());
                if (elementoInventario != null) {
                    ui.put("descripcionInventario", elementoInventario.getDescripcion());
                }
            }
            entityList.add(ui);
        }
        return entityList;
    }

    @GET
    @Path("codigobarras/{codigoBarras}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPartesByCodigoBarras(@PathParam("codigoBarras") String codigoBarras,
                                                  @QueryParam("filter") @DefaultValue("") String filter,
                                                  @QueryParam("sort") @DefaultValue("") String sort)
            throws IOException, ParseException, RegistroNoEncontradoException {
        Long userId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        ParamUtils.checkNotNull(codigoBarras);
        List<ParteVW> parteVWList = partesService.getParteByCodigoBarras(userId, codigoBarras, filterList, sortList);
        List<UIEntity> entityList = new ArrayList<>();
        for (ParteVW parteVW : parteVWList) {
            UIEntity ui = UIEntity.toUI(parteVW);
            if (ParamUtils.isNotNull(parteVW.getCodigoBarras())) {
                ElementoInventario elementoInventario = elementoInventarioService.getExtElementoInventarioCB(parteVW.getCodigoBarras());
                if (elementoInventario != null) {
                    ui.put("descripcionInventario", elementoInventario.getDescripcion());
                }
            }
            entityList.add(ui);
        }
        return entityList;
    }

    @GET
    @Path("nuevos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPartesNuevos(@QueryParam("filter") @DefaultValue("") String filter,
                                          @QueryParam("sort") @DefaultValue("") String sort,
                                          @QueryParam("page") @DefaultValue("0") Integer page,
                                          @QueryParam("limit") @DefaultValue("25") Integer limit,
                                          @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws IOException, RegistroNoEncontradoException {
        Long userId = AccessManager.getConnectedUserId(request);
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        return UIEntity.toUI(partesService.getPartesNuevo(userId, filterList, sortList));

    }

    @GET
    @Path("encolados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPartesEncolados(@QueryParam("filter") @DefaultValue("") String filter,
                                             @QueryParam("sort") @DefaultValue("") String sort,
                                             @QueryParam("page") @DefaultValue("0") Integer page,
                                             @QueryParam("limit") @DefaultValue("25") Integer limit,
                                             @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws IOException {
        Long userId = AccessManager.getConnectedUserId(request);
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        return UIEntity.toUI(partesService.getPartesEncolados(userId, filterList, sortList));

    }

    @GET
    @Path("asignados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPartesAsignados(@QueryParam("filter") @DefaultValue("") String filter,
                                             @QueryParam("sort") @DefaultValue("") String sort,
                                             @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws IOException {
        Long userId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        return UIEntity.toUI(partesService.getPartesAsignados(userId, filterList, sortList, null));

    }

    @GET
    @Path("asignados/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPartesAsignadosByTecnicoId(@PathParam("id") Long tecnicoId,
                                                        @QueryParam("filter") @DefaultValue("") String filter,
                                                        @QueryParam("sort") @DefaultValue("") String sort,
                                                        @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws IOException {
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);

        return UIEntity.toUI(partesService.getPartesAsignadosByTecnicoId(tecnicoId, filterList,
                sortList));
    }

    @GET
    @Path("creados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPartesCreados(@QueryParam("filter") @DefaultValue("") String filter,
                                           @QueryParam("sort") @DefaultValue("") String sort,
                                           @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws IOException {
        Long userId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);

        return UIEntity.toUI(partesService.getPartesCreados(userId, filterList, sortList, null));

    }

    @GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> searchPartes(@QueryParam("filter") @DefaultValue("") String filter,
                                       @QueryParam("sort") @DefaultValue("") String sort,
                                       @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException,
            ParseException {
        Long userId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);

        return UIEntity.toUI(partesService.searchPartes(userId, filterList, sortList, null));

    }

    @POST
    @Path("export/pdf")
    @Produces("application/pdf")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response exportPartesform(@FormParam("filter") @DefaultValue("") String filter,
                                     @FormParam("sort") @DefaultValue("") String sort,
                                     @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @FormParam("solicitante") @DefaultValue("0") Integer solicitante,
                                     @FormParam("creador") @DefaultValue("0") Integer creador,
                                     @FormParam("inventario") @DefaultValue("0") Integer inventario,
                                     @FormParam("conversacion") @DefaultValue("0") Integer conversacion,
                                     @FormParam("asignado") @DefaultValue("0") Integer asignado,
                                     @FormParam("parteselec") List<Long> parteSelect
    ) throws IOException,
            ParseException {
        Long userId = AccessManager.getConnectedUserId(request);
        List<ParteUI> parteList = new ArrayList<>();
        Template template = new PDFTemplate("cau/exportarPdf", new Locale("ES"), "cau");
        template.put("titulo", "Listado de incidencias");
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = new ArrayList<>();
        ItemSort s = new ItemSort("estado", "ASC");
        sortList.add(s);
        List<ParteVW> parteVWList = partesService.searchPartes(userId, filterList, sortList, parteSelect);
        sortList.clear();
        ItemSort iSort = new ItemSort("fechaCreacion", "ASC");
        sortList.add(iSort);
        Integer asignados = 0;
        Integer progreso = 0;
        Integer pendientes = 0;
        for (ParteVW parte : parteVWList) {
            ParteUI parteUI = new ParteUI(parte);
            if (parte.getEstado() >= 3000 && parte.getEstado() < 4000) {
                asignados++;
            } else if (parte.getEstado() == 4000) {
                progreso++;
            } else if (parte.getEstado() >= 5000 && parte.getEstado() < 6000) {
                pendientes++;
            }
            if (inventario.equals(1)) {
                parteUI.setInventario(elementoInventarioService.getInventarioByCB(parte.getCodigoBarras()));
            }
            if (conversacion.equals(1)) {

                List<ParteComunicacion> parteComunicacionList = partesComunicacionService
                        .getParteComunicados(parte.getId(), userId, filterList, sortList);

                List<ParteEstado> parteEstadoList = partesEstadoService
                        .getParteEstados(parte.getId(), userId);

                List<ComunicacionUI> comunicacionUIList = ComunicacionUI.toUI(parteComunicacionList,
                        parteEstadoList);

                parteUI.setComunicacion(comunicacionUIList.size() > 0 ? comunicacionUIList : null);
            }
            parteList.add(parteUI);
        }
        template.put("solicitante", solicitante);
        template.put("creador", creador);
        template.put("asignado", asignado);
        template.put("inventario", inventario);
        template.put("conversacion", conversacion);
        template.put("asignados", asignados);
        template.put("progreso", progreso);
        template.put("pendientes", pendientes);
        template.put("partes", parteList);
        LocalDateTime now = LocalDateTime.now();
        String nowFormat = now.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        return Response.ok(template).type("application/pdf").header("Content-Disposition", "attachment; filename = incidencias-" + nowFormat + ".pdf").build();
//         return template;
//        return UIEntity.toUI(partesService.searchPartes(userId, filterList, sortList));

    }

    @GET
    @Path("asignados/pdf")
    @Produces("application/pdf")
    public Response exportPartesAsignados(@QueryParam("filter") @DefaultValue("") String filter,
                                          @QueryParam("sort") @DefaultValue("") String sort,
                                          @QueryParam("solicitante") @DefaultValue("0") Integer solicitante,
                                          @QueryParam("creador") @DefaultValue("0") Integer creador,
                                          @QueryParam("inventario") @DefaultValue("0") Integer inventario,
                                          @QueryParam("conversacion") @DefaultValue("0") Integer conversacion,
                                          @QueryParam("asignado") @DefaultValue("0") Integer asignado,
                                          @QueryParam("parteselec") List<Long> parteSelect) throws IOException {
        Long userId = AccessManager.getConnectedUserId(request);
        List<ParteUI> parteList = new ArrayList<>();
        Template template = new PDFTemplate("cau/exportarPdf", new Locale("ES"), "cau");
        template.put("titulo", "Listado de incidencias");
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = new ArrayList<>();
        ItemSort s = new ItemSort("estado", "ASC");
        sortList.add(s);
        List<ParteVW> parteVWList = partesService.getPartesAsignados(userId, filterList, sortList, parteSelect);
        ItemSort iSort = new ItemSort("fechaCreacion", "ASC");
        sortList.add(iSort);
        Integer asignados = 0;
        Integer progreso = 0;
        Integer pendientes = 0;
        for (ParteVW parte : parteVWList) {
            ParteUI parteUI = new ParteUI(parte);
            if (parte.getEstado() >= 3000 && parte.getEstado() < 4000) {
                asignados++;
            } else if (parte.getEstado() == 4000) {
                progreso++;
            } else if (parte.getEstado() >= 5000 && parte.getEstado() < 6000) {
                pendientes++;
            }
            if (inventario.equals(1)) {
                parteUI.setInventario(elementoInventarioService.getInventarioByCB(parte.getCodigoBarras()));
            }
            if (conversacion.equals(1)) {
                List<ParteComunicacion> parteComunicacionList = partesComunicacionService
                        .getParteComunicados(parte.getId(), userId, filterList, sortList);

                List<ParteEstado> parteEstadoList = partesEstadoService
                        .getParteEstados(parte.getId(), userId);

                List<ComunicacionUI> comunicacionUIList = ComunicacionUI.toUI(parteComunicacionList,
                        parteEstadoList);
                parteUI.setComunicacion(comunicacionUIList.size() > 0 ? comunicacionUIList : null);
            }
            parteList.add(parteUI);
        }
        template.put("solicitante", solicitante);
        template.put("creador", creador);
        template.put("asignado", asignado);
        template.put("inventario", inventario);
        template.put("conversacion", conversacion);
        template.put("asignados", asignados);
        template.put("progreso", progreso);
        template.put("pendientes", pendientes);
        template.put("partes", parteList);
        LocalDateTime now = LocalDateTime.now();
        String nowFormat = now.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        return Response.ok(template).type("application/pdf").header("Content-Disposition", "attachment; filename = CAUAsignats-" + nowFormat + ".pdf").build();
//         return template;
//        return UIEntity.toUI(partesService.searchPartes(userId, filterList, sortList));

    }

    @GET
    @Path("{parteId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getUserParte(@PathParam("parteId") Long parteId) throws RegistroNoEncontradoException {
        Long userId = AccessManager.getConnectedUserId(request);
        Parte parte = partesService.getParte(parteId);
        try {
            if (parte.getPersonaAfectado().getId().equals(userId)) {
                parte.setLeido(ParteLeido.update(parte.getLeido(), true));
                partesService.updateParte(parte);
            }
            Persona asignado = parte.getPersonaAsignado();
            if (ParamUtils.isNotNull(asignado)) {
                if (asignado.getId().equals(userId)) {
                    parte.setLeido(ParteLeido.update(parte.getLeido(), false));
                    partesService.updateParte(parte);
                }
            }else{
                if(tecnicoAreaService.hasPerfiles(Arrays.asList("PRIMERA","ADMINISTRADOR"), userId)){
                    parte.setLeido(ParteLeido.LEIDOPRIMERA.getValue());
                    partesService.updateParte(parte);
                }
            }
        }catch (Exception e){
             throw new RegistroNoEncontradoException();
        }
        return UIEntity.toUI(partesService.getParteVWById(userId, parteId));
    }

    @GET
    @Path("{parteId}/sub")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSubParte(@PathParam("parteId") Long parteId) {
        List<ParteVW> parteVWList = partesService.getSubParteVWById(parteId);
        List<UIEntity> entityList = new ArrayList<>();
        for (ParteVW parteVW : parteVWList) {
            UIEntity ui = UIEntity.toUI(parteVW);
            if (ParamUtils.isNotNull(parteVW.getCodigoBarras())) {
                ElementoInventario elementoInventario = elementoInventarioService.getExtElementoInventarioCB(parteVW.getCodigoBarras());
                ui.put("descripcionInventario", elementoInventario.getDescripcion());
            }
            entityList.add(ui);
        }
        return entityList;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addParte(@FormParam("parteExterno") Long parteExterno,
                             @FormParam("asunto") String asunto,
                             @FormParam("urgenciaId") Long urgenciaId,
                             @FormParam("impactoId") Long impactoId,
                             @FormParam("prioridadId") Long prioridadId,
                             @FormParam("personaSolicitanteId") Long personaSolicitanteId,
                             @FormParam("afectadoUbicacion") Long afectadoUbicacion,
                             @FormParam("afectadoExtensionTelefono") String afectadoExtension,
                             @FormParam("personaCreacionId") Long personaCreacionId,
                             @FormParam("personaAfectadoId") Long personaAfectadoId,
                             @FormParam("descripcion") String descripcion,
                             @FormParam("codigoBarras") String codigoBarras,
                             @FormParam("tipo") String tipo,
                             @FormParam("areaId") Long area,
                             @FormParam("tecnicos") Long tecnicoId,
                             @FormParam("categoriaId") Long categorias)
            throws CodigoDeBarrasIncorrectoException, CodigoDeBarrasNoNumericoException {
        ParamUtils.checkNotNull(asunto, descripcion, personaAfectadoId);

        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaById(userId);

        Parte parte = new Parte();
        parte.setAsunto(asunto);
        parte.setDescripcion(descripcion);
        parte.setCodigoBarras(codigoBarras);
        parte.setPresente(0L);
        parte.setPersonaCreacion(persona);
        parte.setLeido(ParteLeido.NOLEIDO.getValue());
        parte.setSuscrito(1);
        parte.setNotificaTecnico(1);

        if (ParamUtils.isNotNull(urgenciaId)) {
            parte.setUrgencia(tiposService.getTipoUrgencia(urgenciaId));
        } else {
            parte.setUrgencia(tiposService.getTipoUrgencia(0L));
        }
        if (ParamUtils.isNotNull(impactoId)) {
            parte.setImpacto(tiposService.getTipoImpacto(impactoId));
        } else {
            parte.setImpacto(tiposService.getTipoImpacto(0L));
        }
        if (ParamUtils.isNotNull(prioridadId)) {
            parte.setPrioridad(tiposService.getTipoPrioridad(prioridadId));
        } else if (ParamUtils.isNotNull(urgenciaId) && ParamUtils.isNotNull(impactoId)) {
            parte.setPrioridad(tiposService.getTipoPrioridadByImpactoAndUrgencia(impactoId, urgenciaId));
        }

        if (ParamUtils.isNotNull(afectadoUbicacion)) {
            UbicacionFisica ubicacionFisica = new UbicacionFisica();
            ubicacionFisica.setId(afectadoUbicacion);
            parte.setUbicacionFisica(ubicacionFisica);
        }
        if (ParamUtils.isNotNull(afectadoExtension)) {
            parte.setExtensionTelefono(afectadoExtension);
        }
        if (ParamUtils.isNotNull(personaSolicitanteId)) {
            parte.setPersonaSolicitante(personaService.getPersonaById(personaSolicitanteId));
        } else {
            parte.setPersonaSolicitante(persona);
        }
        if (ParamUtils.isNotNull(personaCreacionId)) {
            parte.setPersonaCreacion(personaService.getPersonaById(personaCreacionId));
        } else {
            parte.setPersonaCreacion(persona);

        }
        if (ParamUtils.isNotNull(personaAfectadoId)) {
            parte.setPersonaAfectado(personaService.getPersonaById(personaAfectadoId));
        } else {
            parte.setPersonaAfectado(persona);
        }
        String descripcionEstado = "Incidència creada";
        if (ParamUtils.isNotNull(parteExterno)) {
            Parte externo = new Parte();
            externo.setId(parteExterno);
            parte.setParteExterno(externo);
            descripcionEstado = "Incidència creada a partir de #" + parteExterno.toString();

        }
        if (ParamUtils.isNotNull(tipo)) {
            parte.setTipo(tipo);
        } else {
            parte.setTipo("1");
        }
        if (ParamUtils.isNotNull(area)) {
            Area a = new Area();
            a.setId(area);
            parte.setArea(a);
        }
        if (ParamUtils.isNotNull(categorias)) {
            Categoria categoria = new Categoria();
            categoria.setId(categorias);
            parte.setCategoria(categoria);
        }

        if (ParamUtils.isNotNull(tecnicoId)) {
            parte.setPersonaAsignado(personaService.getPersonaById(tecnicoId));
        }

        parte = partesService.insertParte(parte);
        partesEstadoService.iniciarParteEstado(parte, parte.getPersonaCreacion(),
                Estados.NUEVO.getValue(), descripcionEstado);
        if (ParamUtils.isNotNull(parte.getTipo(), parte.getArea(), parte.getCategoria())) {
            if (ParamUtils.isNotNull(parte.getPersonaAsignado())) {
                Persona tecnico = parte.getPersonaAsignado();
                partesEstadoService.changeParteEstado(parte, tecnico, Estados.ASIGNADO.getValue(),
                        "Incidència assignada a " + tecnico.getNombreCompleto());

                NotificacionService.enviarNotificacion(parte, "ASIGNACIONTECNICO");
                NotificacionService.enviarNotificacion(parte, "ASIGNACIONUSUARIO");
            } else {
                partesEstadoService.changeParteEstado(parte, persona,
                        Estados.ENCOLADO.getValue(), "Incidència encolada");
                ParteComunicacion parteComunicacion = new ParteComunicacion();

                parteComunicacion.setDescripcion("La seua incidència ha sigut tramitada, actualment està a la espera de ser assignada a un tècnic perquè la resolga al més prompte possible.");
                parteComunicacion.setFechaCreacion(new Date());
                parteComunicacion.setPersonaCreacion(persona);
                parteComunicacion.setParteComunicacion(parte);
                parteComunicacion.setInterno(0L);

                partesComunicacionService.insertParteComunicacion(parteComunicacion);
                NotificacionService.enviarNotificacion(parte, "ENCOLADA");
            }
        }
        return UIEntity.toUI(partesService.getParteVWById(userId, parte.getId()));
    }

    @PUT
    @Path("{parteId}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateParte(@PathParam("parteId") Long parteId,
                                @FormParam("asunto") String asunto,
                                @FormParam("urgenciaId") Long urgenciaId,
                                @FormParam("impactoId") Long impactoId,
                                @FormParam("prioridadId") Long prioridadId,
                                @FormParam("personaAfectadoId") Long personaAfectadoId,
                                @FormParam("personaSolicitanteId") Long personaSolicitanteId,
                                @FormParam("personaCreacionId") Long personaCreacionId,
                                @FormParam("afectadoUbicacion") String ubicacion,
                                @FormParam("afectadoExtensionTelefono") String extension,
                                @FormParam("descripcion") String descripcion,
                                @FormParam("codigoBarras") String codigoBarras,
                                @FormParam("tipo") String tipo,
                                @FormParam("areaId") Long area,
                                @FormParam("personaAsignadoId") Long tecnicos,
                                @FormParam("categoriaId") Long categorias)
            throws CodigoDeBarrasIncorrectoException,
            CodigoDeBarrasNoNumericoException {

        Long userId = AccessManager.getConnectedUserId(request);
        String notificar = "";
        Parte parte = partesService.getParte(parteId);
        parte.setLeido(ParteLeido.NOLEIDO.getValue());
        if (asunto != null) {
            parte.setAsunto(asunto);
        }
        if (extension != null) {
            parte.setExtensionTelefono(extension);
        }

        if (descripcion != null) {
            parte.setDescripcion(descripcion);
        }

        if (codigoBarras != null) {
            parte.setCodigoBarras(codigoBarras);
        }

        if (urgenciaId != null) {
            parte.setUrgencia(tiposService.getTipoUrgencia(urgenciaId));
        }

        if (impactoId != null) {
            parte.setImpacto(tiposService.getTipoImpacto(impactoId));
        }

        if (prioridadId != null) {
            parte.setPrioridad(tiposService.getTipoPrioridad(prioridadId));
        }
        if (ubicacion != null) {
            UbicacionFisica ubicacionFisica = ubicacionFisicaService
                    .getExtUbicacionFisica(ubicacion);
            if (ubicacionFisica != null) {
                parte.setUbicacionFisica(ubicacionFisica);
            }
        }
        if (personaAfectadoId != null) {
            Persona persona = personaService.getPersonaById(personaAfectadoId);
            parte.setPersonaAfectado(persona);
            if (parte.getUbicacionFisica() == null && persona.getUbicacionFisica() != null) {
                parte.setUbicacionFisica(persona.getUbicacionFisica());
            }
        }
        if (personaSolicitanteId != null) {
            Persona persona =  personaService.getPersonaById(personaSolicitanteId);
            persona.setId(personaSolicitanteId);
            parte.setPersonaSolicitante(persona);
        }
        if (personaCreacionId != null) {
            Persona persona = new Persona();
            persona.setId(personaCreacionId);
            parte.setPersonaCreacion(persona);
        }
        if (extension != null) {
            parte.setExtensionTelefono(extension);
        }
        if (ParamUtils.isNotNull(tipo)) {
            parte.setTipo(tipo);
            if (area != null) {
                Persona user = new Persona();
                user.setId(userId);
                Area a = new Area();
                a.setId(area);
                parte.setArea(a);
                if (categorias != null) {
                    Categoria categoria = new Categoria();
                    categoria.setId(categorias);
                    parte.setCategoria(categoria);
                }
                if (tecnicos != null) {
                    ParteEstado estadoParte = partesEstadoService.getEstadoActual(parte.getId());
                    if (estadoParte.getTipoEstado().getId() < Estados.ASIGNADO.getValue()) {
                        Persona persona = personaService.getPersonaById(tecnicos);
                        persona.setId(tecnicos);
                        parte.setPersonaAsignado(persona);

                        partesEstadoService.changeParteEstado(parte, user, Estados.ASIGNADO.getValue(),
                                "Incidència assignada a " + persona.getNombreCompleto());
                        if (!estadoParte.getTipoEstado().getId().equals(Estados.DEVUELTO.getValue())) {
                            notificar = "asignado";
                        }
                    } else if (
                            tecnicoAreaService.hasPerfiles(Arrays.asList("JEFE_AREA","PRIMERA","ADMINISTRADOR","SEGUNDA_ASIGNAR"), userId)) {
                        Persona persona = personaService.getPersonaById(tecnicos);
                        persona.setId(tecnicos);
                        parte.setPersonaAsignado(persona);
                        partesEstadoService.changeParteEstado(parte, user, Estados.ASIGNADO.getValue(),
                                "Incidència assignada a " + persona.getNombreCompleto());
                        if (!estadoParte.getTipoEstado().getId().equals(Estados.DEVUELTO.getValue())) {
                            notificar = "asignado";
                        }
                    }
                    NotificacionService.enviarNotificacion(parte, "ASIGNACIONTECNICO");
                } else {
                    if (parte.getUrgencia() != null && parte.getImpacto() != null && ParamUtils.isNotNull(parte.getCategoria())) {
                        ParteEstado estadoParte = partesEstadoService.getEstadoActual(parte.getId());
                        Long estado = estadoParte.getTipoEstado().getId();
                        Long estadoParent = estadoParte.getTipoEstado().getParent();
                        parte.setPersonaAsignado(null);
                        if (!estado.equals(Estados.ENCOLADO.getValue()) || (ParamUtils.isNotNull(estadoParent) && !estadoParent.equals(Estados.ENCOLADO.getValue()))) {
                            partesEstadoService.changeParteEstado(parte, user,
                                    Estados.ENCOLADO.getValue(), "Incidència encolada");
                            parte.setLeido(ParteLeido.LEIDOPRIMERA.getValue());
                            ParteComunicacion parteComunicacion = new ParteComunicacion();

                            parteComunicacion.setDescripcion("La seua incidència ha sigut tramitada, actualment està a la espera de ser assignada a un tècnic perquè la resolga al més prompte possible.");
                            parteComunicacion.setFechaCreacion(new Date());
                            parteComunicacion.setPersonaCreacion(user);
                            parteComunicacion.setParteComunicacion(parte);
                            parteComunicacion.setInterno(0L);

                            partesComunicacionService.insertParteComunicacion(parteComunicacion);
                            NotificacionService.enviarNotificacion(parte, "ENCOLADA");
                        }

                    }
                }
            }
        }
        parte = partesService.updateParte(parte);
        if (notificar.equals("asignado")) {
            NotificacionService.enviarNotificacion(parte, "ASIGNACIONUSUARIO");
        }
        return toUI(parte);
    }

    @POST
    @Path("{parteId}/devolver")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity devolverParte(@PathParam("parteId") Long parteId,
                                  @FormParam("devolver") String devolver) throws FieldValidationException,
            ErrorAlLeerPlantilla, MessageNotSentException, NotificacionSinDestinatarios {
        Long userId = AccessManager.getConnectedUserId(request);

        Parte parte = partesService.getParte(parteId);
        ParteComunicacion parteComunicacion = new ParteComunicacion();

        if (devolver != null) {
            Persona user = new Persona();
            user.setId(userId);

            Persona persona = new Persona();
            persona.setId(userId);
            parte.setPersonaAsignado(null);
            partesEstadoService.changeParteEstado(parte, persona, Estados.DEVUELTO.getValue(),
                    "Incidència retornada: " + devolver);

            partesService.updateParte(parte);
        }

        return UIEntity.toUI(parteComunicacion);
    }

    @POST
    @Path("{parteId}/rechazar")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity rechazarParte(@PathParam("parteId") Long parteId,
                                  @FormParam("rechazar") String rechazar)
            throws IOException, URISyntaxException, FieldValidationException, ErrorAlLeerPlantilla,
            MessageNotSentException, NotificacionSinDestinatarios {
        Long userId = AccessManager.getConnectedUserId(request);

        if (rechazar == null || rechazar.length() <= 0) {
            rechazar = "Incidència rebutjada";
        }
        Parte parte = partesService.getParte(parteId);

        Persona persona = new Persona();
        persona.setId(userId);

        partesEstadoService.changeParteEstado(parte, persona, Estados.RECHAZADO.getValue(),
                "Incidència rebutjada: " + rechazar);

        NotificacionService.enviarNotificacion(parte, "REABRIR", rechazar);
        ParteComunicacion parteComunicacion = new ParteComunicacion();

        parteComunicacion.setDescripcion("Incidència rebutjada: " + rechazar);
        parteComunicacion.setFechaCreacion(new Date());
        parteComunicacion.setPersonaCreacion(persona);
        parteComunicacion.setParteComunicacion(parte);
        parteComunicacion.setInterno(0L);

        return UIEntity.toUI(partesComunicacionService.insertParteComunicacion(parteComunicacion));
    }

    @POST
    @Path("{parteId}/cerrar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage cerrarParte(@PathParam("parteId") Long parteId) {
        Long userId = AccessManager.getConnectedUserId(request);
        Parte parte = partesService.getParte(parteId);
        Persona persona = new Persona();
        persona.setId(userId);
        partesEstadoService.changeParteEstado(parte, persona, Estados.CERRADO.getValue(),
                "Incidència tancada");

        return new ResponseMessage(true);
    }

    @POST
    @Path("{parteId}/resolver")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity resolverParte(@PathParam("parteId") Long parteId, FormDataMultiPart multiPart)
            throws IOException, FieldValidationException, ErrorAlLeerPlantilla,
            MessageNotSentException, NotificacionSinDestinatarios, RegistroNoEncontradoException {
        Long userId = AccessManager.getConnectedUserId(request);
        Parte parte = partesService.getParte(parteId);

        List<FormDataBodyPart> fields = multiPart.getFields("archivo");
        String resolver = multiPart.getField("resolver").getValue();

        if (resolver != null) {
            Persona user = new Persona();
            user.setId(userId);
            if (tecnicoAreaService.hasPerfiles(Arrays.asList("ADMINISTRADOR","PRIMERA"), userId)) {
                TecnicoArea tecnicoArea = tecnicoAreaService.getTecnicoAreaByPerId(userId);
//                Tecnico tecnico = tecnicoService.getTecnicoById(userId);
                if (parte.getPersonaAsignado() == null) {
                    parte.setPersonaAsignado(user);
                    parte.setArea(tecnicoArea.getArea());
                    parte = partesService.updateParte(parte);
                    partesEstadoService.changeParteEstado(parte, user, Estados.ASIGNADO.getValue(),
                            "Incidència assignat a " + tecnicoArea.getTecnicoArea().getNombreCompleto());
                }
            }


            partesEstadoService.changeParteEstado(parte, user, Estados.RESUELTO.getValue(),
                    "Incidència resolta: " + resolver);
            ParteComunicacion parteComunicacion = new ParteComunicacion();

            parteComunicacion.setDescripcion("Incidència resolta: " + resolver);
            parteComunicacion.setFechaCreacion(new Date());
            parteComunicacion.setPersonaCreacion(user);
            parteComunicacion.setParteComunicacion(parte);
            parteComunicacion.setInterno(3L);

            if (fields != null) {
                parteComunicacion.setDescripcion("Incidència resolta: " + resolver);
                for (FormDataBodyPart field : fields) {
                    ParteFichero parteFichero = new ParteFichero();
                    if (!field.getFormDataContentDisposition().getFileName().isEmpty()
                            && field.getFormDataContentDisposition().getFileName().length() >= 1) {
                        InputStream file = field.getValueAs(InputStream.class);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        int read;
                        byte[] buf = new byte[4096];
                        while ((read = file.read(buf)) != -1) {
                            byteArrayOutputStream.write(buf, 0, read);
                        }
                        parteFichero.setNombreFichero(field.getFormDataContentDisposition()
                                .getFileName());
                        parteFichero.setParteFichero(parte);
                        parteFichero.setMime(field.getMediaType().toString());

                        parteFichero.setFichero(byteArrayOutputStream.toByteArray());

                        partesFicheroService.insertFichero(parteFichero);
                        parteComunicacion.setDescripcion("Incidència resolta: " + resolver
                                + "\n(Adjuntado fichero)");
                    }
                }
            }
            partesComunicacionService
                    .insertParteComunicacion(parteComunicacion);
            NotificacionService.enviarNotificacion(parte, "RESOLVER", resolver);
        }
        return toUI(parte);
    }

    @GET
    @Path("{parteId}/estados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getParteEstados(@PathParam("parteId") Long parteId) {
        Long userId = AccessManager.getConnectedUserId(request);

        List<ParteEstado> parteEstadoList = partesEstadoService.getParteEstados(parteId, userId);
        List<UIEntity> uiEntityList = new ArrayList<>();

        for (ParteEstado parteEstado : parteEstadoList) {
            UIEntity uiEntity = UIEntity.toUI(parteEstado);
            uiEntity.put("nombreCreador", parteEstado.getPersonaIni().getNombreCompleto());
            uiEntityList.add(uiEntity);
        }
        return uiEntityList;
    }


    @POST
    @Path("{parteId}/progreso")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity setParteProgreso(@PathParam("parteId") Long parteId,
                                     @FormParam("estado") Long estado) {
        Long userId = AccessManager.getConnectedUserId(request);

        ParteEstado parteEstadoNew = partesEstadoService.getEstadoActual(parteId);

        if (!parteEstadoNew.getTipoEstado().getId().equals(Estados.ENPROGRESO.getValue())) {
            Persona user = new Persona();
            user.setId(userId);
            Parte parte = partesService.getParte(parteId);

            parteEstadoNew = partesEstadoService.changeParteEstado(parte, user, Estados.ENPROGRESO.getValue(), "Incidència "
                    + tiposService.getTipoEstadoById(Estados.ENPROGRESO.getValue()).getNombre());

            return UIEntity.toUI(parteEstadoNew);
        }
        return UIEntity.toUI(parteEstadoNew);
    }

    @POST
    @Path("{parteId}/estados")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addParteEstados(@PathParam("parteId") Long parteId,
                                    @FormParam("estado") Long estado) {
        Long userId = AccessManager.getConnectedUserId(request);

        Parte parte = partesService.getParte(parteId);
        ParteEstado parteEstadoNew = new ParteEstado();
        Persona user = new Persona();
        user.setId(userId);

        if (estado != null) {
            parteEstadoNew = partesEstadoService.changeParteEstado(parte, user, estado, "Incidència "
                    + tiposService.getTipoEstadoById(estado).getNombre());
            return UIEntity.toUI(parteEstadoNew);
        }
        return UIEntity.toUI(parteEstadoNew);
    }

    @GET
    @Path("{parteId}/ficheros")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getParteFicheros(@PathParam("parteId") Long parteId) {
        Long userId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(partesFicheroService.getParteFicheros(parteId, userId));
    }

    @GET
    @Path("{parteId}/ficheros/{ficheroId}/descargar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response decargarFicheros(@PathParam("parteId") Long parteId,
                                     @PathParam("ficheroId") Long ficheroId) {
        ParteFichero parteFichero = partesFicheroService.getParteFicheroById(ficheroId);

        String nombreFichero = null;
        String contentType = null;
        Integer size = 0;

        byte[] data = parteFichero.getFichero();

        if (data != null) {
            nombreFichero = parteFichero.getNombreFichero();
            contentType = parteFichero.getMime();
            size = data.length;
        }

        return Response.ok(data)
                .header("Content-Disposition", "attachment; filename = \"" + nombreFichero + "\"")
                .header("Content-Length", size).header("Content-Type", contentType).build();
    }

    @POST
    @Path("{parteId}/ficheros/")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response addFichero(@PathParam("parteId") Long parteId, FormDataMultiPart multiPart) {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaById(userId);
        Parte parte = partesService.getParte(parteId);
        String idFichero = "";
        String nombreFichero = "";

        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType != null && !mimeType.isEmpty()) {
                String fileName = "";
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches()) {
                    fileName = m.group(1);
                    fileName = new String(fileName.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

                }
                if (fileName.length() > 0) {
                    ParteFichero parteFichero = new ParteFichero();
                    BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                    InputStream documento = bpe.getInputStream();
                    parteFichero.setNombreFichero(fileName);
                    parteFichero.setParteFichero(parte);
                    parteFichero.setMime(mimeType);
                    parteFichero.setFichero(partesFicheroService.inputStreamToByteArray(documento));
                    parteFichero = partesFicheroService.insertFichero(parteFichero);
                    idFichero = parteFichero.getId().toString();
                    nombreFichero = parteFichero.getNombreFichero();

                }
            }
        }
        String res = "{\"success\":true,\"data\": {\"id\": " + idFichero + ",\"nombreFichero\": \"" + nombreFichero + "\"}}";

        ParteComunicacion parteComunicacion = new ParteComunicacion();

        parteComunicacion.setDescripcion("Fitxer adjuntat: " + nombreFichero);
        parteComunicacion.setFechaCreacion(new Date());
        parteComunicacion.setPersonaCreacion(persona);
        parteComunicacion.setParteComunicacion(parte);
        parteComunicacion.setInterno(0L);
        partesComunicacionService.insertParteComunicacion(parteComunicacion);

        return Response.ok().entity(res).build();
    }

    @DELETE
    @Path("fichero/{ficheroId}/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteFichero(@PathParam("ficheroId") Long ficheroId) {
        partesFicheroService.deleteFichero(ficheroId);
    }

    @DELETE
    @Path("{parteId}/ficheros/{ficheroId}/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void delFichero(@PathParam("ficheroId") Long ficheroId) {
        partesFicheroService.deleteFichero(ficheroId);
    }

    @GET
    @Path("{parteId}/comunicaciones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getParteComunicaciones(
            @QueryParam("filter") @DefaultValue("") String filter,
            @QueryParam("sort") @DefaultValue("") String sort, @PathParam("parteId") Long parteId)
            throws IOException {
        Long userId = AccessManager.getConnectedUserId(request);
        List<UIEntity> parteListUI = new ArrayList<>();
        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);

        List<ParteComunicacion> parteComunicacionList = partesComunicacionService
                .getParteComunicados(parteId, userId, filterList, sortList);

        for (ParteComunicacion parteComunicacion : parteComunicacionList) {
            UIEntity parteUI = UIEntity.toUI(parteComunicacion);

            parteUI.put("nombreCompleto", parteComunicacion.getPersonaCreacion()
                    .getNombreCompleto());

            parteListUI.add(parteUI);
        }

        return parteListUI;

    }

    @GET
    @Path("{parteId}/com")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<ComunicacionUI> getCom(
            @QueryParam("filter") @DefaultValue("") String filter,
            @QueryParam("sort") @DefaultValue("") String sort, @PathParam("parteId") Long parteId) {
        ResponseListUI<ComunicacionUI> response = new ResponseListUI<>();
        Boolean success = true;
        try {
            Long userId = AccessManager.getConnectedUserId(request);

            List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
            List<ItemSort> sortList = ItemSort.jsonDecode(sort);

            List<ParteComunicacion> parteComunicacionList = partesComunicacionService
                    .getParteComunicados(parteId, userId, filterList, sortList);

            List<ParteEstado> parteEstadoList = partesEstadoService
                    .getParteEstados(parteId, userId);

            List<ComunicacionUI> comunicacionUIList = ComunicacionUI.toUI(parteComunicacionList,
                    parteEstadoList);

            response.setData(comunicacionUIList);
            response.setMessage("");
            response.setCount(comunicacionUIList.size());
        } catch (Exception e) {
            log.error(e.getMessage());
            success = false;
        }
        response.setSuccess(success);
        return response;

    }

    @POST
    @Path("{parteId}/comunicaciones")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseItemUI<ComunicacionUI> addParteComunicaciones(
            @PathParam("parteId") Long parteId, ComunicacionUI comunicacionUI) {
        ResponseItemUI<ComunicacionUI> response = new ResponseItemUI<>();
        Boolean success = true;
        try {
            Long userId = AccessManager.getConnectedUserId(request);
            String descripcion = comunicacionUI.getDescripcion();
            Long interno = comunicacionUI.getTipo();
            Parte parte = partesService.getParte(comunicacionUI.getParteId());
            Persona user = personaService.getPersonaById(userId);
            ParteEstado parteEstado = partesEstadoService.getEstadoActual(comunicacionUI.getParteId());
            if (parteEstado.getTipoEstado().getId() >= Estados.RESUELTO.getValue()) {

                response.setSuccess(false);
                response.setMessage("No es permet comunicació en l'estat actual.");
                return response;
            }

            ParteComunicacion parteComunicacion = new ParteComunicacion();

            parteComunicacion.setDescripcion(descripcion);
            parteComunicacion.setFechaCreacion(new Date());
            parteComunicacion.setPersonaCreacion(user);
            parteComunicacion.setParteComunicacion(parte);
            parteComunicacion.setInterno(interno);

            parteComunicacion = partesComunicacionService
                    .insertParteComunicacion(parteComunicacion);

            if (parte.getPersonaAfectado().getId().equals(userId)) {
                NotificacionService.enviarNotificacion(parte, "SOLICITUDTECNICO", descripcion);
                parte.setLeido(ParteLeido.LEIDOUSER.getValue());

            } else if (!parte.getPersonaAfectado().getId().equals(userId) && parte.getSuscrito().equals(1) && interno.equals(0L)) {
                NotificacionService.enviarNotificacion(parte, "SOLICITUDUSUARIO", descripcion);
                if(tecnicoAreaService.hasPerfiles(Arrays.asList("PRIMERA","ADMINISTRADOR"), userId)){
                    parte.setLeido(ParteLeido.LEIDOPRIMERA.getValue());
                }else {
                    parte.setLeido(ParteLeido.LEIDOTECNICO.getValue());
                }
            }
            partesService.updateParte(parte);
            response.setData(ComunicacionUI.toUI(parteComunicacion));
            response.setMessage("");
        } catch (Exception e) {
            log.error(e.getMessage());
            success = false;
        }

        response.setSuccess(success);
        return response;
    }

    @PUT
    @Path("{parteId}/cb")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateParte(@PathParam("parteId") Long parteId, @FormParam("cb") String codigoBarras)
            throws CodigoDeBarrasIncorrectoException, CodigoDeBarrasNoNumericoException {
//        Long userId = AccessManager.getConnectedUserId(request);
        Parte parte = partesService.getParte(parteId);

        if (codigoBarras != null) {
            parte.setCodigoBarras(codigoBarras);
        }
        partesService.updateParte(parte);
        return Response.ok().build();
    }

    @POST
    @Path("notificaciones")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateParteNotificaciones(@FormParam("partes") JSONArray partes) {
        if (ParamUtils.isNotNull(partes)) {
            for (int i = 0; i < partes.length(); i++) {
                JSONObject jsonObject;
                try {
                    jsonObject = partes.getJSONObject(i);
                    partesService.updateParteNotificaciones(Long.parseLong(jsonObject.getString("parte")), Integer.parseInt(jsonObject.getString("notifica")));
                } catch (JSONException e) {
                    log.error(e.getMessage());
                }
            }
        }

        return Response.ok().build();
    }

    @GET
    @Path("{parteId}/albaran")
    @Produces("application/pdf")
    public Response generarEtiqueta(@PathParam("parteId") Long parteId) throws
            ParseException {
        Long userId = AccessManager.getConnectedUserId(request);

        Template template = new PDFTemplate("cau/albaran", new Locale("ES"), "cau");

        template.put("titulo", "Albaran ".concat(parteId.toString()));
        List<ParteVW> parte = partesService.searchPartes(userId, new ArrayList<>(), new ArrayList<>(), Collections.singletonList(parteId));
        if (ParamUtils.isNotNull(parte.get(0).getCodigoBarras())) {
            template.put("inventario", elementoInventarioService.getExtElementoInventarioCB(parte.get(0).getCodigoBarras()));
        }
        template.put("parte", parte.get(0));
        return Response.ok(template).type("application/pdf").header("Content-Disposition", "attachment; filename = Albaran-" + parteId + ".pdf").build();
    }

    @DELETE
    @Path("{parteId}/cdc")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage cerrarIncidenciaCdC(@PathParam("parteId") Long parteId) {
        Long userId = AccessManager.getConnectedUserId(request);

        partesEstadoService.cerrarIncidenciaCdC(userId, parteId);
        return new ResponseMessage(true);
    }

}
