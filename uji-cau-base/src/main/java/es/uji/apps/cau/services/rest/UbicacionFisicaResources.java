package es.uji.apps.cau.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.UbicacionFisica;
import es.uji.apps.cau.services.UbicacionFisicaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("ubicacion")
public class UbicacionFisicaResources extends CoreBaseService {
    @InjectParam
    private UbicacionFisicaService ubicacionFisicaService;

    @GET
    @Path("/nombre/{ubicacion}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getExtUbicacionFisica(@PathParam("ubicacion") String ubicacion) {
        UIEntity ui = new UIEntity();
        if (ParamUtils.isNotNull(ubicacion)) {
            ui = UIEntity.toUI(ubicacionFisicaService.getExtUbicacionFisica(ubicacion));
        }

        return ui;
    }

    @GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> searchExtUbicacionFisica(@QueryParam("query") String search) {
        List<UIEntity> list = new ArrayList<>();
        if (ParamUtils.isNotNull(search)) {
            List<UbicacionFisica> ubicacionFisicaList = ubicacionFisicaService
                    .searchExtUbicacionFisica(search);
            list = UIEntity.toUI(ubicacionFisicaList);
        }

        return list;
    }

    @GET
    @Path("{ubicacionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getInventarioByUbicacionId(@PathParam("ubicacionId") Long ubicacionId) {
        UIEntity ui = new UIEntity();
        if (ParamUtils.isNotNull(ubicacionId)) {
            ui = UIEntity.toUI(ubicacionFisicaService.getExtUbicacionFisicaById(ubicacionId));
        }

        return ui;
    }

    @GET
    @Path("persona/{perId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInventarioByPerId(@PathParam("perId") Long perId) {
        if (perId != null) {
            return UIEntity.toUI(ubicacionFisicaService.getUbicacionFisicaByperId(perId));
        }

        return null;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getExtUbicacionFisica() {
        List<UbicacionFisica> ubicacionFisicaList = ubicacionFisicaService.getExtUbicacionFisica();
        return UIEntity.toUI(ubicacionFisicaList);
    }
}
