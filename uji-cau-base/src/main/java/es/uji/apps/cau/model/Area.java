package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_AREAS")
public class Area implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;

    private Integer externa;

    // bi-directional many-to-one association to Parte
    @OneToMany(mappedBy = "area")
    private Set<Parte> partesArea;

    // bi-directional many-to-one association to TecnicoArea
    @OneToMany(mappedBy = "area")
    private Set<TecnicoArea> tecnicos;

    // bi-directional many-to-one association to ResolucionArea
    @OneToMany(mappedBy = "area")
    private Set<Categoria> categoriaArea;

    public Set<Categoria> getCategoriaArea() {
        return categoriaArea;
    }

    public void setCategoriaArea(Set<Categoria> categoriaArea) {
        this.categoriaArea = categoriaArea;
    }

    public Area() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getExterna() {
        return externa;
    }

    public void setExterna(Integer externa) {
        this.externa = externa;
    }

    public Set<Parte> getPartesArea() {
        return this.partesArea;
    }

    public void setPartesArea(Set<Parte> cauPartes) {
        this.partesArea = cauPartes;
    }

    public Set<TecnicoArea> getTecnicos() {
        return this.tecnicos;
    }

    public void setTecnicos(Set<TecnicoArea> cauTecnicosAreas) {
        this.tecnicos = cauTecnicosAreas;
    }

}
