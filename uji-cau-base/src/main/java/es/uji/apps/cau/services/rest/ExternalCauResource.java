package es.uji.apps.cau.services.rest;
import java.io.IOException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.services.ExternalCauService;
import es.uji.commons.rest.CoreBaseService;

@Path("externalCau")
public class ExternalCauResource extends CoreBaseService{

    @InjectParam
    private ExternalCauService externalCauService;

    @GET
    @Path("getInfo")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExternalCauResourceData() throws IOException{
        return externalCauService.getExternalCauResourceData();
    }

}
