package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cau.dao.CategoriaDAO;
import es.uji.apps.cau.model.Categoria;

@Service
public class CategoriaService {
    @Autowired
    private CategoriaDAO categoriaDAO;

    @Transactional
    public Categoria insertCategoria(Categoria categoria) {
        return categoriaDAO.insert(categoria);
    }

    @Transactional
    public Categoria updateCategoria(Categoria categoria) {
        return categoriaDAO.update(categoria);
    }

    @Transactional
    public void deleteCategoria(Long id) {
        categoriaDAO.delete(Categoria.class, id);
    }

    public List<Categoria> getCategorias() {
        return categoriaDAO.getCategorias();
    }

    public Categoria getCategoriaById(Long categoriaId) {
        return categoriaDAO.getCategoriaById(categoriaId);
    }

    public List<Categoria> getCategoriasByAreaId(Long areaId,Boolean all) {
        return categoriaDAO.getCategoriasByAreaId(areaId,all);
    }
}
