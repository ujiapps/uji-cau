package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "CAU_TECNICOS_AREAS")
public class TecnicoArea implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "F_INICIO_INACTIVIDAD")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicioInactividad;

    @Column(name = "F_FIN_INACTIVIDAD")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinInactividad;

    @ManyToOne
    @JoinColumn(name = "ID_AREA")
    private Area area;

    @ManyToOne
    @JoinColumn(name = "PER_ID_TECNICO")
    private Persona tecnicoArea;

    @Column(name = "AREA_PRINCIPAL")
    private Long areaPrincipal;

    public TecnicoArea() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFechaInicioInactividad() {
        return fechaInicioInactividad;
    }

    public void setFechaInicioInactividad(Date fechaInicioInactividad) {
        this.fechaInicioInactividad = fechaInicioInactividad;
    }

    public Date getFechaFinInactividad() {
        return fechaFinInactividad;
    }

    public void setFechaFinInactividad(Date fechaFinInactividad) {
        this.fechaFinInactividad = fechaFinInactividad;
    }

    public Area getArea() {
        return this.area;
    }

    public void setArea(Area cauArea) {
        this.area = cauArea;
    }

    public Persona getTecnicoArea() {
        return this.tecnicoArea;
    }

    public void setTecnicoArea(Persona cauPersona) {
        this.tecnicoArea = cauPersona;
    }

    public Long getAreaPrincipal() {
        return areaPrincipal;
    }

    public void setAreaPrincipal(Long areaPrincipal) {
        this.areaPrincipal = areaPrincipal;
    }
}
