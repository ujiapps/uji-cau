package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cau.dao.PersonaDAO;
import es.uji.apps.cau.model.Persona;
import es.uji.commons.rest.UIEntity;

@Service
public class PersonaService {
    @Autowired
    private PersonaDAO personaDAO;

    public Persona getPersonaById(Long userId) {
        return personaDAO.getPersonaById(userId);
    }

    public List<Persona> searchPersona(String search, Integer limit) {

        return personaDAO.searchPersona(search, limit);
    }

    public List<Persona> searchPersonaByCuenta(String cuenta, Integer limit) {
        return personaDAO.searchPersonaByCuenta(cuenta, limit);
    }

    public List<Persona> getTecnicos() {
        return personaDAO.getTecnicos();
    }

    public Persona getTecnicoById(Long tecnicoId) {
        return personaDAO.getTecnicoById(tecnicoId);
    }

    public List<UIEntity> getTecnicosByAreaId(Long areaId) {
        return personaDAO.getTecnicosByAreaId(areaId);
    }

    public List<Persona> searchTecnicos(String cuenta, Integer limit) {
        return personaDAO.searchTecnicos(cuenta, limit);
    }

    public Boolean hasPerfil(String perfil, Long userId) {
        return personaDAO.hasPerfil(perfil, userId);
    }
}
