package es.uji.apps.cau.utils;

public enum Perfiles {
    ADMINISTRADOR("ADMINISTRADOR"), EXTERNO("EXTERNO"), JEFE_AREA("JEFE_AREA"), PRIMERA("PRIMERA"), SEGUNDA("PRIMERA"), SEGUNDA_ASIGNAR("SEGUNDA_ASIGNAR");

    private final String descripcion;

    private Perfiles(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}

