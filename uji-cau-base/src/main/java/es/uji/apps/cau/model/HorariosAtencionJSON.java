package es.uji.apps.cau.model;
import org.codehaus.jackson.annotate.JsonProperty;

public class HorariosAtencionJSON {

    @JsonProperty("viernesInicioHora")
    private String viernesInicioHora;

    @JsonProperty("viernesInicioMinutos")
    private String viernesInicioMinutos;
    @JsonProperty("viernesFinHora")
    private String viernesFinHora;
    @JsonProperty("viernesFinMinutos")
    private String viernesFinMinutos;
    @JsonProperty("restoInicioHora")
    private String restoInicioHora;
    @JsonProperty("restoInicioMinutos")
    private String restoInicioMinutos;
    @JsonProperty("restoFinHora")
    private String restoFinHora;
    @JsonProperty("restoFinMinutos")
    private String restoFinMinutos;


    public HorariosAtencionJSON(HorariosAtencion horariosAtencionViernes, HorariosAtencion horariosAtencionLunesAJueves) {
        if (horariosAtencionViernes != null){
            this.viernesInicioHora = horariosAtencionViernes.getHoraInicio().getHours() < 10 ? "0"+horariosAtencionViernes.getHoraInicio().getHours(): String.valueOf(horariosAtencionViernes.getHoraInicio().getHours());
            this.viernesInicioMinutos = horariosAtencionViernes.getHoraInicio().getMinutes() < 10 ? "0" + horariosAtencionViernes.getHoraInicio().getMinutes() : String.valueOf(horariosAtencionViernes.getHoraInicio().getMinutes());
            this.viernesFinHora = horariosAtencionViernes.getHoraFin().getHours() < 10 ? "0"+horariosAtencionViernes.getHoraFin().getHours(): String.valueOf(horariosAtencionViernes.getHoraFin().getHours());
            this.viernesFinMinutos = horariosAtencionViernes.getHoraFin().getMinutes() < 10 ? "0" + horariosAtencionViernes.getHoraFin().getMinutes() : String.valueOf(horariosAtencionViernes.getHoraFin().getMinutes());
        }
        else{
            this.viernesInicioHora = "";
            this.viernesInicioMinutos = "";
            this.viernesFinHora = "";
            this.viernesFinMinutos = "";
        }
        if (horariosAtencionLunesAJueves != null) {

            this.restoInicioHora = horariosAtencionLunesAJueves.getHoraInicio().getHours() < 10 ? "0"+horariosAtencionLunesAJueves.getHoraInicio().getHours(): String.valueOf(horariosAtencionLunesAJueves.getHoraInicio().getHours());
            this.restoInicioMinutos = horariosAtencionLunesAJueves.getHoraInicio().getMinutes() < 10 ? "0" + horariosAtencionLunesAJueves.getHoraInicio().getMinutes() : String.valueOf(horariosAtencionLunesAJueves.getHoraInicio().getMinutes());
            this.restoFinHora = horariosAtencionLunesAJueves.getHoraFin().getHours() < 10 ? "0"+horariosAtencionLunesAJueves.getHoraFin().getHours(): String.valueOf(horariosAtencionLunesAJueves.getHoraFin().getHours());
            this.restoFinMinutos = horariosAtencionLunesAJueves.getHoraFin().getMinutes() < 10 ? "0" + horariosAtencionLunesAJueves.getHoraFin().getMinutes() : String.valueOf(horariosAtencionLunesAJueves.getHoraFin().getMinutes());
        }
        else{
            this.restoInicioHora = "";
            this.restoInicioMinutos = "";
            this.restoFinHora = "";
            this.restoFinMinutos = "";
        }

    }

    public HorariosAtencionJSON() {
    }

    public String getViernesInicioHora() {
        return viernesInicioHora;
    }

    public void setViernesInicioHora(String viernesInicioHora) {
        this.viernesInicioHora = viernesInicioHora;
    }

    public String getViernesInicioMinutos() {
        return viernesInicioMinutos;
    }

    public void setViernesInicioMinutos(String viernesInicioMinutos) {
        this.viernesInicioMinutos = viernesInicioMinutos;
    }

    public String getViernesFinHora() {
        return viernesFinHora;
    }

    public void setViernesFinHora(String viernesFinHora) {
        this.viernesFinHora = viernesFinHora;
    }

    public String getViernesFinMinutos() {
        return viernesFinMinutos;
    }

    public void setViernesFinMinutos(String viernesFinMinutos) {
        this.viernesFinMinutos = viernesFinMinutos;
    }

    public String getRestoInicioHora() {
        return restoInicioHora;
    }

    public void setRestoInicioHora(String restoInicioHora) {
        this.restoInicioHora = restoInicioHora;
    }

    public String getRestoInicioMinutos() {
        return restoInicioMinutos;
    }

    public void setRestoInicioMinutos(String restoInicioMinutos) {
        this.restoInicioMinutos = restoInicioMinutos;
    }

    public String getRestoFinHora() {
        return restoFinHora;
    }

    public void setRestoFinHora(String restoFinHora) {
        this.restoFinHora = restoFinHora;
    }

    public String getRestoFinMinutos() {
        return restoFinMinutos;
    }

    public void setRestoFinMinutos(String restoFinMinutos) {
        this.restoFinMinutos = restoFinMinutos;
    }
}
