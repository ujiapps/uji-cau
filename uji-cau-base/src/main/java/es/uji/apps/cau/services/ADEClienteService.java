package es.uji.apps.cau.services;

import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.cau.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.cau.exceptions.ErrorSubiendoDocumentoException;


public class ADEClienteService
{

    private String authToken;
    private URL adeURL;

    public ADEClienteService(String authToken, String adeURL) throws MalformedURLException
    {
        this.adeURL = new URL(adeURL);
        this.authToken = authToken;
    }

    public String addDocumento(String nombre, String mimetype, byte[] contenido)
            throws ErrorSubiendoDocumentoException
    {
        WebResource adePostClient = Client.create().resource(adeURL.toString());

        FormDataMultiPart responseMultipart = new FormDataMultiPart();
        responseMultipart.field("name", nombre);
        responseMultipart.field("mimetype", mimetype);
        responseMultipart.field("contents", contenido, MediaType.valueOf(mimetype));

        ClientResponse response = adePostClient.type(MediaType.MULTIPART_FORM_DATA)
                .header("X-UJI-AuthToken", authToken).post(ClientResponse.class, responseMultipart);

        JsonNode responseMessage = response.getEntity(JsonNode.class);
        JsonNode payload = responseMessage.path("data");

        if (response.getStatus() == 200 && responseMessage.path("success").asBoolean())
        {
            return payload.path("reference").asText();
        }

        throw new ErrorSubiendoDocumentoException();
    }

    public void deleteDocumento(String reference) throws ErrorEnBorradoDeDocumentoException,
            MalformedURLException
    {

        URL url = new URL(adeURL, reference);
        WebResource adePostClient = Client.create().resource(url.toString());

        ClientResponse response = adePostClient.header("X-UJI-AuthToken", authToken).delete(
                ClientResponse.class);

        if (response.getStatus() != 200)
        {
            throw new ErrorEnBorradoDeDocumentoException();
        }
    }

}