package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "CAU_TAGS")
public class Tag implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TAG")
    private String nombre;


    @OneToMany(mappedBy = "tag")
    private Set<ParteTag> parteTags;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<ParteTag> getParteTags()
    {
        return parteTags;
    }

    public void setParteTags(Set<ParteTag> parteTags)
    {
        this.parteTags = parteTags;
    }
}
