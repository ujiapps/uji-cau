package es.uji.apps.cau.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Alerta;
import es.uji.apps.cau.services.AlertaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("alerta")
public class AlertaResources extends CoreBaseService
{

    @InjectParam
    private AlertaService alertaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll()
    {
        List<UIEntity> entities = new ArrayList<>();

        for (Alerta agrupacion : alertaService.getAllAlertas())
        {
            entities.add(UIEntity.toUI(agrupacion));
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(alertaService.getAlerta(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(UIEntity entity)
    {
        Alerta agrupacion = entity.toModel(Alerta.class);

        alertaService.addAlerta(agrupacion);

        return UIEntity.toUI(agrupacion);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(UIEntity entity)
    {
        Alerta agrupacion = entity.toModel(Alerta.class);

        alertaService.updateAlerta(agrupacion);

        return UIEntity.toUI(agrupacion);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        alertaService.deleteAlerta(id);

        return Response.noContent().build();
    }
}
