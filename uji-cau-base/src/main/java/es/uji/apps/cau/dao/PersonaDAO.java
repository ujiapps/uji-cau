package es.uji.apps.cau.dao;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.Persona;
import es.uji.apps.cau.model.QArea;
import es.uji.apps.cau.model.QPersona;
import es.uji.apps.cau.model.QTecnico;
import es.uji.apps.cau.model.QTecnicoArea;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.json.lookup.LookupItem;
import es.uji.commons.sso.dao.ApaDAO;

@Repository
public class PersonaDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {
    private QPersona qPersona = QPersona.persona;

    @Autowired
    private ApaDAO apaDAO;

    public boolean esAdmin(Long personaId) {
        return apaDAO.hasPerfil("CAU", "ADMIN", personaId);
    }

    public boolean hasPerfil(String perfil, Long id) {
        return apaDAO.hasPerfil("CAU", perfil, id);
    }

    public Persona getPersonaById(Long userId) {

        JPAQuery query = new JPAQuery(entityManager);

        if (userId != null) {
            query.from(qPersona).where(qPersona.id.eq(userId));

        }
        List<Persona> personaList = query.list(qPersona);

        if (personaList.size() > 0) {
            return personaList.get(0);
        }
        return null;

    }

    public List<Persona> searchPersona(String search, Integer limit) {
        JPAQuery query = new JPAQuery(entityManager);

        if (search != null && !search.isEmpty()) {
            String s = StringUtils.limpiaAcentos(search);
            query.from(qPersona).where(qPersona.cuenta.isNotNull(),
                    qPersona.nombreCompletoBusqueda.contains(s.toUpperCase())
                            .or(qPersona.apellidosNombreBus.contains(s.toUpperCase()))
                            .or(qPersona.cuenta.toLowerCase().contains(s.toLowerCase()))
            );
        }
        query.limit(limit);
        query.orderBy(qPersona.tipo.asc());
        return query.list(qPersona);

    }

    public List<Persona> searchPersonaByCuenta(String cuenta, Integer limit) {
        JPAQuery query = new JPAQuery(entityManager);

        if (cuenta != null && !cuenta.isEmpty()) {
            String s = StringUtils.limpiaAcentos(cuenta);
            query.from(qPersona).where(
                    qPersona.cuenta.toLowerCase().startsWith(s.toLowerCase()));
        }
        query.limit(limit);
        query.orderBy(qPersona.tipo.asc());
        return query.list(qPersona);

    }

    public List<Persona> getTecnicos() {
        QTecnicoArea qTecnicoArea = QTecnicoArea.tecnicoArea1;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPersona).leftJoin(qPersona.ubicacionFisica).fetch().
                join(qPersona.tecnicosArea, qTecnicoArea);

        return query.list(qPersona);
    }

    public Persona getTecnicoById(Long userId) {
        JPAQuery query = new JPAQuery(entityManager);

        if (userId != null) {
            query.from(qPersona).where(qPersona.id.eq(userId));

        }
        List<Persona> personaList = query.list(qPersona);

        if (personaList.size() > 0) {
            return personaList.get(0);
        }
        return null;
    }

    public List<UIEntity> getTecnicosByAreaId(Long areaId) {
        QTecnicoArea qTecnicoArea = QTecnicoArea.tecnicoArea1;
        QArea qArea = QArea.area;
        QTecnico qTecnico = QTecnico.tecnico1;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPersona).leftJoin(qPersona.tecnicosArea, qTecnicoArea)
                .leftJoin(qTecnicoArea.area, qArea)
                .join(qPersona.tecnicos, qTecnico).fetch()
                .where((qArea.id.eq(areaId).or(qArea.nombre.eq("TODAS")))
                        .and(qTecnicoArea.fechaFinInactividad.gt(new Date()).or(qTecnicoArea.fechaFinInactividad.isNull())))
                .orderBy(qPersona.nombreCompleto.asc());

        return query.list(qPersona, qTecnico.empresa).stream().map(a -> {
            UIEntity ui = UIEntity.toUI(a.get(qPersona));
            ui.put("empresa", a.get(qTecnico.empresa));
            return ui;
        }).collect(Collectors.toList());
    }

    public List<Persona> searchTecnicos(String search, Integer limit) {
        JPAQuery query = new JPAQuery(entityManager);

        String s = StringUtils.limpiaAcentos(search);
        query.from(qPersona).join(qPersona.tecnicosArea).where(
                qPersona.nombreCompletoBusqueda.like("%" + s.toUpperCase() + "%")
                        .or(qPersona.apellidosNombreBus.like("%" + s.toUpperCase() + "%"))
                        .or(qPersona.cuenta.toLowerCase().like(s.toLowerCase() + "%"))
        );
        query.limit(limit);
        return query.list(qPersona);

    }

    @Override
    public List<LookupItem> search(String queryString) {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(queryString);
        Long queryStringLong = 0L;

        try {
            queryStringLong = Long.parseLong(queryString);
        } catch (Exception e) {
        }

        return query.from(qPersona).where(
                qPersona.nombreCompletoBusqueda.contains(cadenaLimpia.toUpperCase())
                        .or(qPersona.apellidosNombreBus.contains(cadenaLimpia.toUpperCase()))
                        .or(qPersona.cuenta.toLowerCase().contains(cadenaLimpia.toLowerCase()))
                        .or(qPersona.id.eq(queryStringLong)))
                .orderBy(qPersona.nombre.asc())
                .list(qPersona.id, qPersona.nombreCompleto, qPersona.cuenta).stream().map(persona -> {
                    LookupItem item = new LookupItem();
                    item.setId(String.valueOf(persona.get(qPersona.id)));
                    item.setNombre(persona.get(qPersona.nombreCompleto));
                    item.addExtraParam("cuenta", persona.get(qPersona.cuenta));
                    return item;
                }).collect(Collectors.toList());
    }
}
