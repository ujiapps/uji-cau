package es.uji.apps.cau.services;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cau.dao.ParteFicheroDAO;
import es.uji.apps.cau.model.ParteFichero;

@Service
public class PartesFicheroService
{
    private static final Logger log = LoggerFactory.getLogger(PartesFicheroService.class);

    @Autowired
    private ParteFicheroDAO parteFicheroDAO;

    @Transactional
    public ParteFichero insertFichero(ParteFichero parteFichero)
    {
        return parteFicheroDAO.insert(parteFichero);
    }

    @Transactional
    public ParteFichero updateFichero(ParteFichero parteFichero)
    {
        return parteFicheroDAO.update(parteFichero);
    }

    @Transactional
    public void deleteFichero(Long id)
    {
        parteFicheroDAO.delete(ParteFichero.class, id);
    }

    public List<ParteFichero> getParteFicheros(Long parteId, Long userId)
    {
        return parteFicheroDAO.getParteFicheros(parteId, userId);
    }

    public ParteFichero getParteFicheroById(Long ficheroId)
    {
        return parteFicheroDAO.getParteFicheroById(ficheroId);
    }

    public byte[] inputStreamToByteArray(InputStream inputStream)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int size = 1024;
        byte[] buffer = new byte[size];
        int len;

        try
        {
            while ((len = inputStream.read(buffer, 0, size)) != -1)
            {
                outputStream.write(buffer, 0, len);
            }

            return outputStream.toByteArray();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(),e);
        }

        return null;
    }

}
