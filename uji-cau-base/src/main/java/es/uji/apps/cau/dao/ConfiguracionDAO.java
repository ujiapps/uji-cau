package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.Configuracion;
import es.uji.apps.cau.model.QConfiguracion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ConfiguracionDAO extends BaseDAODatabaseImpl {
    private QConfiguracion qConfiguracion = QConfiguracion.configuracion;

    public Configuracion getConfiguracion() {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qConfiguracion).where(qConfiguracion.id.eq(0L));

        List<Configuracion> configuracionList = query.list(qConfiguracion);
        if (configuracionList.size() > 0) {
            return configuracionList.get(0);
        }
        return null;
    }
}
