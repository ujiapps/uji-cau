package es.uji.apps.cau.model;

import es.uji.apps.cau.utils.CategoriaMantenimiento;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CAU_MANTENIMIENTOS")
public class Mantenimiento {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CREADOR_ID")
    private Long creadorId;

    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Column(name = "TITULO")
    private String titulo;

    @Column(name = "TITULO_ES")
    private String tituloEs;

    @Basic
    @Column(name = "CATEGORIA")
    private Long categoriaId;

    @Transient
    private CategoriaMantenimiento categoria;

    @PostLoad
    void fillTransient() {
        if (categoriaId > -1) {
            this.categoria = CategoriaMantenimiento.valueOfId(categoriaId);
        }
    }

    @PrePersist
    void fillPersistent() {
        if (categoria != null) {
            this.categoriaId = categoria.getId();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreadorId() {
        return creadorId;
    }

    public void setCreadorId(Long creadorId) {
        this.creadorId = creadorId;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTituloEs() {
        return tituloEs;
    }

    public void setTituloEs(String tituloEs) {
        this.tituloEs = tituloEs;
    }

    public Long getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(Long categoriaId) {
        this.categoriaId = categoriaId;
    }

    public CategoriaMantenimiento getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaMantenimiento categoria) {
        this.categoria = categoria;
    }
}
