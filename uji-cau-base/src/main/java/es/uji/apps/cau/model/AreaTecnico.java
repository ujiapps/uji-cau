package es.uji.apps.cau.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "CAU_AREAS_TECNICO")
public class AreaTecnico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "PER_ID_TECNICO")
    private Long perIdTecnico;

    @Column(name = "AREA_ID")
    private Long areaId;

    @Column(name = "AREA_PRINCIPAL")
    private Long areaPrincipal;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getPerIdTecnico()
    {
        return perIdTecnico;
    }

    public void setPerIdTecnico(Long perIdTecnico)
    {
        this.perIdTecnico = perIdTecnico;
    }

    public Long getAreaId()
    {
        return areaId;
    }

    public void setAreaId(Long areaId)
    {
        this.areaId = areaId;
    }

    public Long getAreaPrincipal()
    {
        return areaPrincipal;
    }

    public void setAreaPrincipal(Long areaPrincipal)
    {
        this.areaPrincipal = areaPrincipal;
    }
}
