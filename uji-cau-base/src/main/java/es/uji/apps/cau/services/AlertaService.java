package es.uji.apps.cau.services;

import java.io.FileInputStream;
import java.text.MessageFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cau.dao.AlertaDAO;
import es.uji.apps.cau.dao.AlertaParteDAO;
import es.uji.apps.cau.dao.CalendarioDAO;
import es.uji.apps.cau.exceptions.ErrorAlLeerPlantilla;
import es.uji.apps.cau.model.Alerta;
import es.uji.apps.cau.model.AlertaParte;
import es.uji.apps.cau.model.ParteAlertaVW;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.model.FieldValidationException;
import es.uji.commons.rest.StreamUtils;

@Service
public class AlertaService {

    @Autowired
    private AlertaDAO alertaDAO;
    @Autowired
    private AlertaParteDAO alertaParteDAO;
    @Autowired
    private CalendarioDAO calendarioDAO;


    public Alerta getAlerta(Long id) {
        List<Alerta> alertas = alertaDAO.get(Alerta.class, id);

        if (alertas != null && !alertas.isEmpty()) {
            return alertas.get(0);
        }

        return null;
    }

    public Alerta addAlerta(Alerta alerta) {
        return alertaDAO.insert(alerta);
    }

    public Alerta updateAlerta(Alerta alerta) {
        return alertaDAO.update(alerta);
    }

    public void deleteAlerta(Long id) {
        alertaDAO.delete(Alerta.class, id);
    }

    public List<Alerta> getAllAlertas() {
        return alertaDAO.get(Alerta.class);
    }

    public AlertaParte addAlertaParte(AlertaParte alertaParte) {
        return alertaParteDAO.insert(alertaParte);
    }

    public List<ParteAlertaVW> getAllPartesAlertas() {
        return alertaDAO.get(ParteAlertaVW.class);
    }

    public void enviarAlerta(List<String> destinatarios, Long parteId, Long nivel) throws FieldValidationException, MessageNotSentException, ErrorAlLeerPlantilla {
        NotificacionService.enviar(destinatarios, asuntoAviso(parteId, nivel), cuerpoAviso(parteId, nivel));
    }

    private static String asuntoAviso(Long parteId, Long nivel) {
        String asunto = "";
        if (nivel.equals(1L)) {
            asunto = "[CAU - Alerta de Nivell 1] Incidència #" + parteId;
        } else if (nivel.equals(2L)) {
            asunto = "[CAU - Alerta de Nivell 2] Incidència #" + parteId;
        } else if (nivel.equals(3L)) {
            asunto = "[CAU - Alerta de Nivell 3] Incidència #" + parteId;
        }
        return asunto;
    }

    private static String cuerpoAviso(Long parteId, Long nivel) throws ErrorAlLeerPlantilla {
        String cuerpo = "";
        try {
            if (nivel.equals(1L)) {
                cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                        "/etc/uji/cau/templates/avisoNivel1.template")));
            } else if (nivel.equals(2L)) {
                cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                        "/etc/uji/cau/templates/avisoNivel2.template")));
            } else if (nivel.equals(3L)) {
                cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                        "/etc/uji/cau/templates/avisoNivel3.template")));
            }
        } catch (Exception e) {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'parte nou.");
        }
        return MessageFormat.format(cuerpo, parteId);
    }
}