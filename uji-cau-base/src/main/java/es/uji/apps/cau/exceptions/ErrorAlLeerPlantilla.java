package es.uji.apps.cau.exceptions;

public class ErrorAlLeerPlantilla extends GeneralCAUException
{
    public ErrorAlLeerPlantilla()
    {
        super("No s\'han pogut carregar el fitxer de plantilla.");
    }

    public ErrorAlLeerPlantilla(String message)
    {
        super(message);
    }

}