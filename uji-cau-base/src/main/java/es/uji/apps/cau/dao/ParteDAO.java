package es.uji.apps.cau.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.OrderSpecifier;

import es.uji.apps.cau.model.Parte;
import es.uji.apps.cau.model.ParteVW;
import es.uji.apps.cau.model.QParte;
import es.uji.apps.cau.model.QParteEstado;
import es.uji.apps.cau.model.QParteTag;
import es.uji.apps.cau.model.QParteVW;
import es.uji.apps.cau.utils.DynamicFilter;
import es.uji.apps.cau.utils.Estados;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.apps.cau.utils.ItemSort;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class ParteDAO extends BaseDAODatabaseImpl
{
    private static Logger log = LoggerFactory.getLogger(ParteDAO.class);

    private QParte qParte = QParte.parte;
    private QParteVW qParteVW = QParteVW.parteVW;
    private QParteTag qParteTag = QParteTag.parteTag;

    private QParteEstado qParteEstado = QParteEstado.parteEstado1;

    public Page<Parte> getUserPublicPartes(Long userId, List<ItemFilter> filterList,
                                           List<ItemSort> sortList, Pageable page)
    {
        JPAQuery query = new JPAQuery(entityManager);
        Boolean quitarCerrados = true;
        if (userId != null)
        {
            query.from(qParte)
                    .leftJoin(qParte.estados, qParteEstado)
                    .fetch()
                    .where(qParte.personaAfectado.id.eq(userId)
                            .or(qParte.personaSolicitante.id.eq(userId))
                            .and(qParteEstado.actual.eq(1)))
                    .orderBy(qParteEstado.fechaInicio.desc());
        }

        if (filterList.size() > 0 && filterList != null)
        {
            BooleanBuilder result = new BooleanBuilder();
            for (ItemFilter f : filterList)
            {
                if (f.getField().equals("fechaInicio") || f.getField().equals("fechaFin"))
                {
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try
                    {
                        Date dateformat = format.parse(f.getValue());
                        if (f.getField().equals("fechaInicio"))
                        {
                            result.and(qParteEstado.fechaInicio.goe(dateformat));
                        }
                        else
                        {
                            result.and(qParteEstado.fechaInicio.loe(dateformat));
                        }
                    }
                    catch (ParseException ex)
                    {
                        log.error("Parse exception", ex);
                    }
                }
                if (f.getField().equals("adjuntos"))
                {
                    result.and(qParte.ficheros.isNotEmpty());
                }
                if (f.getField().equals("estado"))
                {
                    result.or(qParteEstado.tipoEstado.id.eq(Long.valueOf(f.getValue())));
                    if (Long.valueOf(f.getValue()).equals(Estados.CERRADO.getValue()))
                    {
                        quitarCerrados = false;
                    }
                }
            }
            query.where(result);
        }
        if (quitarCerrados)
        {
            query.where(qParteEstado.tipoEstado.id.ne(Estados.CERRADO.getValue()));
        }

        query.offset(page.getOffset());
        query.limit(page.getPageSize());

        return new PageImpl<Parte>(query.list(qParte), page, query.count());
    }

    public Page<ParteVW> getUserPublicPartesVW(Long userId, List<ItemFilter> filterList,
                                               List<ItemSort> sortList, Pageable page)
    {
        JPAQuery query = new JPAQuery(entityManager);
        Boolean quitarCerrados = true;
        if (userId != null)
        {
            query.from(qParteVW)
                    .where(qParteVW.personaAfectadoId.eq(userId)
                            .or(qParteVW.personaSolicitanteId.eq(userId)))
                    .orderBy(qParteVW.fechaInicio.desc());
        }

        if (filterList.size() > 0 && filterList != null)
        {
            BooleanBuilder result = new BooleanBuilder();
            BooleanBuilder estados = new BooleanBuilder();
            for (ItemFilter f : filterList)
            {
                if (f.getField().equals("fechaInicio") || f.getField().equals("fechaFin"))
                {
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try
                    {
                        Date dateformat = format.parse(f.getValue());
                        if (f.getField().equals("fechaInicio"))
                        {
                            result.and(qParteVW.fechaInicio.goe(dateformat));
                        }
                        else
                        {
                            result.and(qParteVW.fechaInicio.loe(dateformat));
                        }
                    }
                    catch (ParseException ex)
                    {
                        log.error("Parse exception", ex);
                    }
                }
                quitarCerrados = false;
                if (f.getField().equals("estado"))
                {
                    estados.or(qParteVW.estado.eq(Long.valueOf(f.getValue())).or(qParteVW.estadoParent.eq(Long.valueOf(f.getValue()))));
                }
                if (f.getField().equals("descripcion"))
                {
                    result.and(Utils.limpia(qParteVW.descripcion).containsIgnoreCase(StringUtils.limpiaAcentos(f.getValue()))
                            .or(Utils.limpia(qParteVW.asunto).containsIgnoreCase(StringUtils.limpiaAcentos(f.getValue()))
                                    .or(qParteVW.id.like(f.getValue()))));
                }
            }
            query.where(result.and(estados));
        }
        if (quitarCerrados)
        {
            query.where(qParteVW.estado.ne(Estados.CERRADO.getValue()));
        }

        query.offset(page.getOffset());
        query.limit(page.getPageSize());

        return new PageImpl<ParteVW>(query.list(qParteVW), page, query.count());
    }

    public List<ParteVW> getPartesNuevos(List<ItemFilter> filterList,
                                         List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qParteVW).where(
                qParteVW.estado.eq(Estados.NUEVO.getValue()));

        return query.list(qParteVW);
    }

    public List<ParteVW> getPartesEncolados(Long userId, List<ItemFilter> filterList,
                                            List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (userId != null)
        {
            query.from(qParteVW).where(
                    qParteVW.estado.eq(Estados.ENCOLADO.getValue()).or(
                            qParteVW.estadoParent.eq(Estados.ENCOLADO.getValue()))
            );
        }
        return query.list(qParteVW);
    }

    public Parte getParteByIdAndUserId(Long parteId, Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (userId != null && parteId != null)
        {
            query.from(qParte).where(
                    qParte.id.eq(parteId).and(
                            qParte.personaAfectado.id.eq(userId).or(
                                    qParte.personaSolicitante.id.eq(userId))
                    )
            );
        }
        List<Parte> parteList = query.list(qParte);
        if (parteList.size() > 0)
        {
            return parteList.get(0);
        }
        return null;
    }

    public ParteVW getParteVWById(Long parteId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (parteId != null)
        {
            query.from(qParteVW).where(qParteVW.id.eq(parteId));
        }
        List<ParteVW> parteList = query.list(qParteVW);
        if (parteList.size() > 0)
        {
            return parteList.get(0);
        }
        return null;
    }

    public List<ParteVW> getPartesAsignados(Long userId, List<ItemFilter> filterList,
                                            List<ItemSort> sortList,List<Long> seleccionados)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (userId != null)
        {
            query.from(qParteVW).where(qParteVW.personaAsignadoId.eq(userId).and(qParteVW.estado.lt(Estados.RESUELTO.getValue())));
            if(ParamUtils.isNotNull(seleccionados)){
                query.where(qParteVW.id.in(seleccionados));
            }
            return query.orderBy(qParteVW.id.desc()).list(qParteVW);
        }
        else
        {
            return null;
        }
    }

    public List<ParteVW> getPartesCreados(Long userId, List<ItemFilter> filterList,
                                          List<ItemSort> sortList,List<Long> seleccionados)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (userId != null)
        {
            query.from(qParteVW).where(qParteVW.personaAfectadoId.eq(userId)
                    .or(qParteVW.personaCreacionId.eq(userId)).and(qParteVW.estado.ne(Estados.CERRADO.getValue())));
            if(ParamUtils.isNotNull(seleccionados)){
                query.where(qParteVW.id.in(seleccionados));
            }
            return query.orderBy(qParteVW.id.desc()).list(qParteVW);
        }
        else
        {
            return null;
        }

    }

    public Parte getParte(Long parteId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (parteId != null)
        {
            query.from(qParte).where(qParte.id.eq(parteId));
        }

        List<Parte> parteList = query.list(qParte);
        if (parteList.size() > 0)
        {
            return parteList.get(0);
        }
        return null;
    }

    public List<ParteVW> searchPartes(Long userId, List<ItemFilter> filterList,
                                      List<ItemSort> sortList,List<Long> seleccionados) throws ParseException{
        return searchPartes(userId, filterList, sortList, seleccionados,null);
    }

//    @Transactional
    public List<ParteVW> searchPartes(Long userId, List<ItemFilter> filterList,
                                      List<ItemSort> sortList,List<Long> seleccionados,List<Long> areas) throws ParseException
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (userId != null)
        {
            query.from(qParteVW);

        }
//        Boolean quitarCerrados = true;
        if (filterList.size() > 0 && filterList != null)
        {
            BooleanBuilder result = new BooleanBuilder();
            BooleanBuilder estados = new BooleanBuilder();
            for (ItemFilter f : filterList)
            {
                if (f.getField().equals("fechaInicio") || f.getField().equals("fechaFin"))
                {
                    try
                    {
                        if (f.getField().equals("fechaInicio"))
                        {
                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            Date dateformat = format.parse(f.getValue());
                            result.and(qParteVW.fechaCreacion.goe(dateformat));
                        }
                        else
                        {
                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            Date dateformat = format.parse(f.getValue() + " 23:59:59");
                            result.and(qParteVW.fechaCreacion.lt(dateformat));
                        }
                    }
                    catch (ParseException ex)
                    {
                        log.error("Parse exception", ex);
                    }
                }
                if (f.getField().equals("estado"))
                {
                    result.and(qParteVW.estado.eq(Long.valueOf(f.getValue())).or(qParteVW.estadoParent.eq(Long.valueOf(f.getValue()))));
//                    if (Long.valueOf(f.getValue()).equals(Estados.CERRADO.getValue()))
//                    {
//                        quitarCerrados = false;
//                    }
                }
                if (f.getField().equals("area"))
                {
                    result.and(qParteVW.areaId.eq(Long.valueOf(f.getValue())));
                }
                if (f.getField().equals("tagId"))
                {
                    result.and(qParteVW.id.in(new JPASubQuery().from(qParteTag).where(qParteTag.tag.id.like(f.getValue())).list(qParteTag.parte.id)));
                }
            }
            query.where(result);
        }
//        if (quitarCerrados)
//        {
//            query.where(qParteVW.estado.ne(Estados.CERRADO.getValue()));
//        }
        Map<String, Object> validFilters = new HashMap<String, Object>();

        validFilters.put("codigoBarras", qParteVW.codigoBarras);
        validFilters.put("tecnicoAsignado", qParteVW.personaAsignadoId);

        validFilters.put("personaCreador", qParteVW.personaCreacionId);
        validFilters.put("cuentaCreador", qParteVW.creadorCuenta);
        validFilters.put("ubicacionCreador", qParteVW.creadorUbicacion);
        validFilters.put("extensionCreador", qParteVW.creadorExtensionTelefono);

        validFilters.put("personaAfectado", qParteVW.personaAfectadoId);
        validFilters.put("cuentaAfectado", qParteVW.afectadoCuenta);
        validFilters.put("ubicacionAfectado", qParteVW.afectadoUbicacion);
        validFilters.put("extensionAfectado", qParteVW.afectadoExtensionTelefono);

        validFilters.put("asunto", qParteVW.asunto);
        validFilters.put("descripcion", qParteVW.descripcion);

        validFilters.put("personaSolicitante", qParteVW.personaSolicitanteId);
        validFilters.put("cuentaSolicitante", qParteVW.solicitanteCuenta);

        validFilters.put("ubicacionSolicitante", qParteVW.solicitanteUbicacion);
        validFilters.put("extensionSolicitante", qParteVW.extensionTelefono);

        validFilters.put("impacto", qParteVW.impactoId);
        validFilters.put("urgenciaId", qParteVW.urgenciaId);
        validFilters.put("parteId", qParteVW.id);
        validFilters.put("categoria", qParteVW.categoriaId);
        validFilters.put("ipInventario", qParteVW.ipInventario);

        Map<String, Object> validSorts = new HashMap<String, Object>();

        validSorts.put("id", qParteVW.id);
        validSorts.put("estado", qParteVW.estado);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));
        if(areas!=null){
            BooleanBuilder areasBuild = new BooleanBuilder();
            areasBuild.and(qParteVW.areaId.in(areas)
                    .or(qParteVW.personaAfectadoId.eq(userId))
                    .or(qParteVW.personaSolicitanteId.eq(userId))
                    .or(qParteVW.personaCreacionId.eq(userId))

            );
            areasBuild.or(qParteVW.id.in(new JPASubQuery().from(qParteEstado).where(qParteEstado.personaFin.id.eq(userId)).distinct().list(qParteEstado.parteEstado.id)));
            query.where(areasBuild);
        }
        if(ParamUtils.isNotNull(seleccionados) && seleccionados.size()>0){
            query.where(qParteVW.id.in(seleccionados));
        }

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        query.orderBy(qParteVW.fechaInicio.desc());

        return query.list(qParteVW);
    }
    public List<ParteVW> getPartesByUserAfectado(Long userId, List<ItemFilter> filterList,
                                                 List<ItemSort> sortList) throws ParseException{
        return getPartesByUserAfectado(userId, filterList, sortList,null);
    }

//    @Transactional
    public List<ParteVW> getPartesByUserAfectado(Long userId, List<ItemFilter> filterList,
                                                 List<ItemSort> sortList, List<Long> areas) throws ParseException
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (userId != null)
        {
            query.from(qParteVW)
                    .orderBy(qParteVW.fechaInicio.desc());
        }
        Map<String, Object> validFilters = new HashMap<String, Object>();

        validFilters.put("personaAfectado", qParteVW.personaAfectadoId);
//        validFilters.put("area", qParteVW.areaId);

        Map<String, Object> validSorts = new HashMap<String, Object>();

        validSorts.put("id", qParteVW.id);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));
        if(areas!=null){
            query.where(qParteVW.areaId.in(areas));
        }

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }

        return query.list(qParteVW);
    }

    public List<ParteVW> getParteByCodigoBarras(Long userId, String codigoBarras,
                                                List<ItemSort> sortList) throws ParseException {
        return getParteByCodigoBarras(userId,codigoBarras,sortList, null);
    }


    @Transactional
    public List<ParteVW> getParteByCodigoBarras(Long userId, String codigoBarras,
                                                List<ItemSort> sortList,List<Long> areas) throws ParseException
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (userId != null)
        {
            query.from(qParteVW).where(qParteVW.codigoBarras.eq(codigoBarras))
                    .orderBy(qParteVW.fechaInicio.desc());
        }
//        Map<String, Object> validFilters = new HashMap<String, Object>();

//        validFilters.put("area", qParteVW.areaId);

        Map<String, Object> validSorts = new HashMap<String, Object>();

        validSorts.put("id", qParteVW.id);

//        query.where(DynamicFilter.applyFilter(filterList, validFilters));
        if(areas!=null){
            query.where(qParteVW.areaId.in(areas));
        }
        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }


        return query.list(qParteVW);
    }

    public List<ParteVW> getPartesAsignadosByTecnicoId(Long tecnicoId, List<ItemFilter> filterList,
                                                       List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (tecnicoId != null)
        {
            query.from(qParteVW).where(
                    qParteVW.personaAsignadoId.eq(tecnicoId).and(
                            qParteVW.estado.between(Estados.ASIGNADO.getValue(),
                                    Estados.RESUELTO.getValue() - 1)
                    )
            );
            return query.list(qParteVW);
        }
        else
        {
            return null;
        }
    }

    public List<ParteVW> getSubPartesVWById(Long parteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qParteVW).where(
                qParteVW.parteExternoId.eq(parteId));
        return query.list(qParteVW);
    }

    public ParteVW getParteVWByIdAndAreaId(Long userId, Long parteId, List<Long> areasId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qParteVW).where(
                qParteVW.id.eq(parteId).and(qParteVW.areaId.in(areasId).or(qParteVW.personaAfectadoId.eq(userId)
                        .or(qParteVW.personaSolicitanteId.eq(userId)
                                .or(qParteVW.personaCreacionId.eq(userId))))));
        return query.uniqueResult(qParteVW);
    }

    @Transactional
    public void updateParteNotificaciones(long parte, int notifica)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager,qParte);
        update.where(qParte.id.eq(parte)).set(qParte.notificaTecnico,notifica).execute();
    }

    public Long getTotalIncidencias()
    {
        Date todayLastYear = new Date();
        todayLastYear.setHours(0);
        todayLastYear.setMinutes(0);
        todayLastYear.setSeconds(0);
        todayLastYear.setYear(todayLastYear.getYear()-1);
        return new JPAQuery(entityManager)
                .from(qParte)
                .join(qParteEstado).on(qParte.id.eq(qParteEstado.parteEstado.id))
                .where(qParteEstado.fechaInicio.goe(todayLastYear))
                .singleResult(qParte.id.countDistinct());
    }
}
