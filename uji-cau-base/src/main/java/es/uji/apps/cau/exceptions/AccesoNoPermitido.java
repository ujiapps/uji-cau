package es.uji.apps.cau.exceptions;

public class AccesoNoPermitido extends Throwable
{
    public AccesoNoPermitido()
    {
        super("Access no permes a la incidencia.");
    }


    public AccesoNoPermitido(String message)
    {
        super(message);
    }
}
