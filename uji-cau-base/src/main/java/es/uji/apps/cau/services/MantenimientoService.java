package es.uji.apps.cau.services;

import es.uji.apps.cau.dao.MantenimientoDAO;
import es.uji.apps.cau.model.Mantenimiento;
import es.uji.apps.cau.utils.CategoriaMantenimiento;
import es.uji.apps.cau.utils.DateUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MantenimientoService {

    MantenimientoDAO mantenimientoDAO;

    public MantenimientoService(MantenimientoDAO mantenimientoDAO) {
        this.mantenimientoDAO = mantenimientoDAO;
    }

    public List<Mantenimiento> getMantenimientos() {

        return mantenimientoDAO.getMantenimientos();
    }

    public Mantenimiento addMantenimiento(Long userId,
                                          String fechaInicio,
                                          String fechaFin,
                                          String titulo,
                                          String tituloEs,
                                          Long categoria,
                                          Integer estado) {
        Mantenimiento mantenimiento = new Mantenimiento();
        mantenimiento.setCreadorId(userId);
        mantenimiento.setFechaCreacion(new Date());
        mantenimiento.setFechaInicio(DateUtils.formatHorarioFecha(fechaInicio));
        mantenimiento.setFechaFin(DateUtils.formatHorarioFecha(fechaFin));
        mantenimiento.setTitulo(titulo);
        mantenimiento.setTituloEs(tituloEs);
        mantenimiento.setCategoria(CategoriaMantenimiento.valueOfId(categoria));

        return mantenimientoDAO.insert(mantenimiento);

    }

    public Mantenimiento updateMantenimiento(Long id, Long userId, String fechaInicio, String fechaFin, String titulo, String tituloEs, Long categoria) {
        Mantenimiento mantenimiento = mantenimientoDAO.getById(id);
        mantenimiento.setCreadorId(userId);
        mantenimiento.setFechaInicio(DateUtils.formatHorarioFecha(fechaInicio));
        mantenimiento.setFechaFin(DateUtils.formatHorarioFecha(fechaFin));
        mantenimiento.setTitulo(titulo);
        mantenimiento.setTituloEs(tituloEs);
        mantenimiento.setCategoriaId(categoria);
        mantenimiento.setCategoria(CategoriaMantenimiento.valueOfId(categoria));

        return mantenimientoDAO.update(mantenimiento);
    }

    public void deleteMantenimiento(Long id) {
        mantenimientoDAO.delete(Mantenimiento.class, id);
    }
}
