package es.uji.apps.cau.services;

import es.uji.apps.cau.dao.ParteEstadoDAO;
import es.uji.apps.cau.model.Parte;
import es.uji.apps.cau.model.ParteEstado;
import es.uji.apps.cau.model.Persona;
import es.uji.apps.cau.model.TipoEstado;
import es.uji.apps.cau.utils.Estados;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class PartesEstadoService {

    private ParteEstadoDAO parteEstadoDAO;

    private PartesService partesService;

    @Autowired
    public PartesEstadoService(ParteEstadoDAO parteEstadoDAO, PartesService partesService) {
        this.parteEstadoDAO = parteEstadoDAO;
        this.partesService = partesService;
    }

    public ParteEstado insertParteEstado(ParteEstado parteEstado) {
        return parteEstadoDAO.insert(parteEstado);
    }

    public ParteEstado updateParteEstado(ParteEstado parteEstado) {
        return parteEstadoDAO.update(parteEstado);
    }

    public void deleteParteEstado(Long id) {
        parteEstadoDAO.delete(ParteEstado.class, id);
    }

    public List<ParteEstado> getParteEstados(Long parteId, Long userId) {
        return parteEstadoDAO.getPartesEstado(parteId, userId);
    }

    public ParteEstado getParteEstado(Long parteId) {
        return parteEstadoDAO.getParteEstado(parteId);
    }

    public ParteEstado getEstadoActual(Long id) {
        return parteEstadoDAO.getEstadoActual(id);
    }

    public Date getFechaAsignacion(Long parteId) {
        return parteEstadoDAO.getFechaAsignacion(parteId);
    }

    public ParteEstado getParteEstadoCreacion(Long parteId) {
        return parteEstadoDAO.getParteEstadoCreacion(parteId);
    }

    public ParteEstado iniciarParteEstado(Parte parte, Persona persona, Long estado, String desc) {

        ParteEstado parteEstadoNew = new ParteEstado();
        TipoEstado tipoEstado = new TipoEstado();
        tipoEstado.setId(estado);
        parteEstadoNew.setTipoEstado(tipoEstado);
        parteEstadoNew.setParteEstado(parte);
        parteEstadoNew.setPersonaIni(persona);
        if (estado.equals(Estados.CERRADO.getValue())) {
            parteEstadoNew.setFechaFin(new Date());
            parteEstadoNew.setPersonaFin(persona);
        }
        parteEstadoNew.setFechaInicio(new Date());
        parteEstadoNew.setDescripcion(desc);
        parteEstadoNew.setActual(1);
        return insertParteEstado(parteEstadoNew);
    }

    public ParteEstado finalizarParteEstado(Parte parte, Persona persona) {

        ParteEstado parteEstado = getParteEstado(parte.getId());
        parteEstado.setFechaFin(new Date());
        parteEstado.setPersonaFin(persona);
        parteEstado.setActual(0);
        return updateParteEstado(parteEstado);
    }

    @Transactional
    public ParteEstado changeParteEstado(Parte parte, Persona persona, Long estado, String desc) {
        finalizarParteEstado(parte, persona);
        return iniciarParteEstado(parte, persona, estado, desc);
    }

    public Integer setParteAutoResuelto() {
        List<ParteEstado> list = parteEstadoDAO.setParteAutoResuelto();
        for (ParteEstado parteEstado : list) {
            Persona persona = new Persona();
            persona.setId(parteEstado.getPersonaIni().getId());
            parteEstado.setFechaFin(new Date());
            parteEstado.setPersonaFin(persona);
            parteEstado.setActual(0);
            updateParteEstado(parteEstado);
            ParteEstado parteEstadoNew = new ParteEstado();
            TipoEstado tipoEstado = new TipoEstado();
            tipoEstado.setId(Estados.CERRADO.getValue());
            parteEstadoNew.setTipoEstado(tipoEstado);

            Parte parte = new Parte();
            parte.setId(parteEstado.getParteEstado().getId());
            parteEstadoNew.setParteEstado(parte);
            parteEstadoNew.setPersonaIni(persona);
            parteEstadoNew.setFechaFin(new Date());
            parteEstadoNew.setPersonaFin(persona);
            parteEstadoNew.setFechaInicio(new Date());
            parteEstadoNew.setDescripcion("Incidència Tancada Automàticament");
            parteEstadoNew.setActual(1);
            insertParteEstado(parteEstadoNew);
        }
        return list.size();
    }

    public void cerrarIncidenciaCdC(Long userId, Long indicenciaId) {
        Parte parte = partesService.getParte(indicenciaId);
        Persona persona = new Persona();
        persona.setId(userId);
        this.changeParteEstado(parte, persona, Estados.CERRADO.getValue(),
                "Incidència tancada per incumpliment del codi de conducta.");

    }
}
