package es.uji.apps.cau.ui;

import java.util.List;

import es.uji.apps.cau.model.ElementoInventario;
import es.uji.apps.cau.model.ParteVW;

public class ParteUI
{
    private ParteVW parte;
    private ElementoInventario inventario;
    private List<ComunicacionUI> comunicacion;

    public ParteUI(ParteVW parte)
    {
        this.parte = parte;
    }

    public ParteVW getParte()
    {
        return parte;
    }

    public void setParte(ParteVW parte)
    {
        this.parte = parte;
    }

    public ElementoInventario getInventario()
    {
        return inventario;
    }

    public void setInventario(ElementoInventario inventario)
    {
        this.inventario = inventario;
    }

    public List<ComunicacionUI> getComunicacion()
    {
        return comunicacion;
    }

    public void setComunicacion(List<ComunicacionUI> comunicacion)
    {
        this.comunicacion = comunicacion;
    }
}
