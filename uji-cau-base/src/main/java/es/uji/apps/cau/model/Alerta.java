package es.uji.apps.cau.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "CAU_ALERTAS")
public class Alerta
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "PRIORIDAD_NOMBRE")
    private String prioridadNombre;

    @Column(name = "NIVEL")
    private Long nivel;

    @Column(name = "TIEMPO")
    private Long tiempo;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getPrioridadNombre()
    {
        return prioridadNombre;
    }

    public void setPrioridadNombre(String prioridadNombre)
    {
        this.prioridadNombre = prioridadNombre;
    }

    public Long getNivel()
    {
        return nivel;
    }

    public void setNivel(Long nivel)
    {
        this.nivel = nivel;
    }

    public Long getTiempo()
    {
        return tiempo;
    }

    public void setTiempo(Long tiempo)
    {
        this.tiempo = tiempo;
    }
}
