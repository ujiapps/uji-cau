package es.uji.apps.cau.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.QParteVW;
import es.uji.apps.cau.model.QPersona;
import es.uji.apps.cau.model.QTecnicoArea;
import es.uji.apps.cau.model.TecnicoArea;
import es.uji.apps.cau.utils.DynamicFilter;
import es.uji.apps.cau.utils.Estados;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Repository
public class TecnicoAreaDAO extends BaseDAODatabaseImpl {
    private QTecnicoArea qTecnicoArea = QTecnicoArea.tecnicoArea1;
    private QPersona qPersona = QPersona.persona;
    private QParteVW qParteVW = QParteVW.parteVW;

    public List<TecnicoArea> getTecnicosArea(List<ItemFilter> filterList) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTecnicoArea)
                .join(qTecnicoArea.tecnicoArea, qPersona).fetch()
                .join(qTecnicoArea.area).fetch()
                .where(qTecnicoArea.areaPrincipal.eq(1L));

        Map<String, Object> validFilters = new HashMap<String, Object>();

        validFilters.put("nombreTecnico", qTecnicoArea.tecnicoArea.nombreCompleto);
        validFilters.put("area", qTecnicoArea.area.id);


        query.where(DynamicFilter.applyFilter(filterList, validFilters));
        query.where(qTecnicoArea.fechaFinInactividad.gt(new Date()).or(qTecnicoArea.fechaFinInactividad.isNull()));

        return query.list(qTecnicoArea);
    }

    public TecnicoArea getTecnicoAreaById(Long tecnicoAreaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTecnicoArea).join(qTecnicoArea.tecnicoArea, qPersona).fetch()
                .join(qTecnicoArea.area).fetch()
                .where(qTecnicoArea.id.eq(tecnicoAreaId));

        List<TecnicoArea> tecnicoAreaList = query.list(qTecnicoArea);
        if (tecnicoAreaList.size() > 0)
        {
            return tecnicoAreaList.get(0);
        }
        return null;
    }

    public TecnicoArea getTecnicoAreaByPerId(Long tecnicoId) throws RegistroNoEncontradoException {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTecnicoArea).join(qTecnicoArea.tecnicoArea, qPersona).fetch()
                .join(qTecnicoArea.area).fetch()
                .where(qTecnicoArea.tecnicoArea.id.eq(tecnicoId));

        List<TecnicoArea> tecnicoAreaList = query.list(qTecnicoArea);
        if (tecnicoAreaList.size() > 0)
        {
            return tecnicoAreaList.get(0);
        } else
        {
            return new TecnicoArea();
        }
    }

    public boolean hasPerfil(String perfil, Long id) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTecnicoArea).where(
                qTecnicoArea.tecnicoArea.id.eq(id).and(qTecnicoArea.tipo.eq(perfil)));

        return (query.list(qTecnicoArea.tipo).size() > 0);
    }

    public boolean hasPerfiles(List<String> perfiles, Long id) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTecnicoArea).where(
                qTecnicoArea.tecnicoArea.id.eq(id).and(qTecnicoArea.tipo.in(perfiles)));

        return (query.list(qTecnicoArea.tipo).size() > 0);
    }

    public boolean isAdmin(Long id) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTecnicoArea).where(
                qTecnicoArea.tecnicoArea.id.eq(id).and(qTecnicoArea.tipo.eq("ADMINISTRADOR")));

        return (query.list(qTecnicoArea.tipo).size() > 0);
    }

    public boolean isJefeArea(Long id) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTecnicoArea).where(
                qTecnicoArea.tecnicoArea.id.eq(id).and(qTecnicoArea.tipo.eq("JEFE_AREA")));

        return (query.list(qTecnicoArea.tipo).size() > 0);
    }

    public List<Tuple> getTecnicosPartesAsignados() {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qParteVW)
                .where(qParteVW.estado.between(Estados.ASIGNADO.getValue(), Estados.RESUELTO.getValue() - 1))
                .groupBy(qParteVW.personaAsignadoId);
        List<Tuple> result = query.list(qParteVW.personaAsignadoId, qParteVW.count());
        return result;
    }

    public List<TecnicoArea> getJefesAreaId(Long areaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTecnicoArea).join(qTecnicoArea.tecnicoArea, qPersona).fetch()
                .where(qTecnicoArea.area.id.eq(areaId)
                        .and(qTecnicoArea.tipo.eq("JEFE_AREA"))
                        .and(qTecnicoArea.fechaFinInactividad.gt(new Date()).or(qTecnicoArea.fechaFinInactividad.isNull())));
        return query.list(qTecnicoArea);
    }

    public List<TecnicoArea> getTecnicosPrimera() {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTecnicoArea).where(
                qTecnicoArea.tipo.eq("PRIMERA").and(qTecnicoArea.fechaFinInactividad.gt(new Date()).or(qTecnicoArea.fechaFinInactividad.isNull())));

        return query.list(qTecnicoArea);
    }

    public List<TecnicoArea> getTecnicosAreasById(List<ItemFilter> filterList, List<Long> areas) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTecnicoArea)
                .join(qTecnicoArea.tecnicoArea, qPersona).fetch()
                .join(qTecnicoArea.area).fetch();

        Map<String, Object> validFilters = new HashMap<String, Object>();

        validFilters.put("nombreTecnico", qTecnicoArea.tecnicoArea.nombreCompleto);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));
        query.where(qTecnicoArea.area.id.in(areas)
        .and(qTecnicoArea.fechaFinInactividad.gt(new Date()).or(qTecnicoArea.fechaFinInactividad.isNull())));

        return query.list(qTecnicoArea);
    }

    public List<String> getPerfiles(Long id) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTecnicoArea).where(
                qTecnicoArea.tecnicoArea.id.eq(id));
        return query.list(qTecnicoArea.tipo);
    }
}