package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_TIPOS_ESTADOS_PARTES")
public class TipoEstado implements Serializable
{
    @Id
    private Long id;

    private String nombre;

    @Column(name = "ESTADO_PADRE_ID")
    private Long parent;

    @OneToMany(mappedBy = "tipoEstado")
    private Set<ParteEstado> partesEstado;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getParent()
    {
        return parent;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public Set<ParteEstado> getPartesEstado()
    {
        return partesEstado;
    }

    public void setPartesEstado(Set<ParteEstado> partesEstado)
    {
        this.partesEstado = partesEstado;
    }
}
