package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.ElementoInventario;
import es.uji.apps.cau.model.QElementoInventario;
import es.uji.apps.cau.model.QUbicacionFisica;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class ElementoInventarioDAO extends BaseDAODatabaseImpl {
    private QElementoInventario qElementoInventario = QElementoInventario.elementoInventario;
    private QUbicacionFisica qUbicacionFisica = QUbicacionFisica.ubicacionFisica;

    public ElementoInventario getExtElementoInventarioCB(String codigoBarras) {
        if (codigoBarras == null) {
            throw new IllegalArgumentException("El código de barras es obligatorio");
        }

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qElementoInventario).where(qElementoInventario.codigoBarras.like(codigoBarras));

        return query.uniqueResult(qElementoInventario);
    }

    public ElementoInventario getInventarioByCB(String codigoBarras) {
        if (codigoBarras == null) {
           return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qElementoInventario).where(qElementoInventario.codigoBarras.like(codigoBarras));

        return query.uniqueResult(qElementoInventario);
    }

    public List<ElementoInventario> searchExtElementoInventarioCB(String search) {
        JPAQuery query = new JPAQuery(entityManager);

        if (search != null && !search.isEmpty()) {
            query.from(qElementoInventario)
                    .where(qElementoInventario.codigoBarras.lower().like(
                            "%" + search.toLowerCase() + "%"));
        }
        query.limit(10);
        return query.list(qElementoInventario);
    }

    public List<ElementoInventario> searchInventarioById(Long lineaId) {
        JPAQuery query = new JPAQuery(entityManager);
            query.from(qElementoInventario)
                    .where(qElementoInventario.id.eq(lineaId));
        return query.list(qElementoInventario);
    }

    public List<ElementoInventario> getInventarioByUbicacionId(String ubicacionId) {
        JPAQuery query = new JPAQuery(entityManager);

        if (ubicacionId != null) {
            query.from(qElementoInventario).where(qElementoInventario.ubicacionFisica.eq(ubicacionId));
        }

        return query.list(qElementoInventario);
    }
}
