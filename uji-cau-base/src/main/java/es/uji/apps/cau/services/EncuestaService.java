package es.uji.apps.cau.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cau.dao.EncuestaDAO;
import es.uji.apps.cau.exceptions.ErrorAlLeerPlantilla;
import es.uji.apps.cau.model.Encuesta;
import es.uji.apps.cau.model.Parte;
import es.uji.apps.cau.model.ParteVW;
import es.uji.apps.cau.utils.Paginacion;
import es.uji.commons.messaging.client.MessageNotSentException;

@Service
public class EncuestaService
{
    private static Logger log = LoggerFactory.getLogger(EncuestaService.class);


    private EncuestaDAO encuestaDAO;
    private PartesService partesService;
    private NotificacionService notificacionService;

    @Autowired
    public EncuestaService(EncuestaDAO encuestaDAO, PartesService partesService,NotificacionService notificacionService)
    {
        this.encuestaDAO = encuestaDAO;
        this.partesService = partesService;
        this.notificacionService = notificacionService;
    }

    public Encuesta getById(Long id)
    {
        List<Encuesta> encuestas = encuestaDAO.get(Encuesta.class, id);

        if (encuestas != null && !encuestas.isEmpty())
        {
            return encuestas.get(0);
        }

        return null;
    }

    public Encuesta update(Encuesta encuesta)
    {
        return encuestaDAO.update(encuesta);
    }

    public void delete(Long id)
    {
        encuestaDAO.delete(Encuesta.class, id);
    }

    public List<Encuesta> getAll()
    {
        return encuestaDAO.get(Encuesta.class);
    }

    public Encuesta addEncuestas(Parte parte, Long userId, Integer atencion, Integer informacion, Integer solucion, Integer tiempo, String motivo)
    {
        Encuesta encuesta = new Encuesta();
        try
        {
            ParteVW parteVW = partesService.getParteVWById(userId, parte.getId());
            encuesta.setParte(parteVW);
            encuesta.setPersonaId(userId);
            encuesta.setAtencion(atencion);
            encuesta.setInformacion(informacion);
            encuesta.setSolucion(solucion);
            encuesta.setTiempo(tiempo);
            encuesta.setMotivo(motivo);
            encuesta =  this.add(encuesta);
            notificacionService.enviarEncuestaTecnico(parteVW, encuesta);
        }
        catch (MessageNotSentException | ErrorAlLeerPlantilla e)
        {
            log.error(e.getMessage());
        }
        return encuesta;

    }

    public Encuesta add(Encuesta encuesta)
    {
        return encuestaDAO.insert(encuesta);
    }

    public List<Encuesta> getEncuestas(String query, Paginacion paginacion)
    {
        return encuestaDAO.getEncuestas(query, paginacion);
    }
}