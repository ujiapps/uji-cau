package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.Mensaje;
import es.uji.apps.cau.model.QMensaje;
import es.uji.apps.cau.utils.TipoMensaje;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MensajesDAO extends BaseDAODatabaseImpl {

    private QMensaje qMensaje = QMensaje.mensaje1;

    public List<Mensaje> getMensajesAdmin(Long userId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qMensaje).where(qMensaje.personaId.eq(userId).or(
                qMensaje.tipo.in(TipoMensaje.ADMINISTRADOR,TipoMensaje.TECNICO)))
                .orderBy(qMensaje.id.desc());;

        return query.list(qMensaje);
    }

    public Mensaje getMensaje(Long id) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMensaje).where(qMensaje.id.eq(id));

        return query.singleResult(qMensaje);
    }

    public Mensaje getMensajeByUserId(Long id, Long userId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMensaje).where(qMensaje.id.eq(id).and(qMensaje.personaId.eq(userId)));

        return query.singleResult(qMensaje);
    }

    public List<Mensaje> getMensajesByUserId(Long userId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMensaje).where(qMensaje.personaId.eq(userId));
        return query.list(qMensaje);
    }

    public List<Mensaje> getListadosMensajesTecnico(Long userId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMensaje)
                .where(qMensaje.personaId.eq(userId).or(qMensaje.tipo.eq(TipoMensaje.TECNICO)))
                .orderBy(qMensaje.id.desc());
        return query.list(qMensaje);
    }
}