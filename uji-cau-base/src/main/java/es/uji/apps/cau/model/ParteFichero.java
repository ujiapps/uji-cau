package es.uji.apps.cau.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mysema.query.annotations.QueryProjection;

@Entity
@Table(name = "CAU_PARTES_FICHEROS")
public class ParteFichero implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Lob()
    private byte[] fichero;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    private String notas;

    private String mime;

    @ManyToOne
    @JoinColumn(name = "PAR_ID")
    private Parte parteFichero;

    @QueryProjection
    public ParteFichero(Long id, String nombreFichero, String mime)
    {
        this.id = id;
        this.nombreFichero = nombreFichero;
        this.mime = mime;
    }

    public ParteFichero()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public byte[] getFichero()
    {
        return this.fichero;
    }

    public void setFichero(byte[] fichero)
    {
        this.fichero = fichero;
    }

    public String getNombreFichero()
    {
        return this.nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getNotas()
    {
        return this.notas;
    }

    public void setNotas(String notas)
    {
        this.notas = notas;
    }

    public Parte getParteFichero()
    {
        return this.parteFichero;
    }

    public void setParteFichero(Parte cauParte)
    {
        this.parteFichero = cauParte;
    }

    public String getMime()
    {
        return mime;
    }

    public void setMime(String mime)
    {
        this.mime = mime;
    }
}
