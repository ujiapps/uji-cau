package es.uji.apps.cau.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {
    public static Date formatHorarioHora(String horaInicio) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return Date.from(LocalDateTime.parse("01/01/2000 ".concat(horaInicio), formatter).atZone(ZoneId.of("Europe/Madrid")).toInstant());
    }

    public static Date formatHorarioFecha(String fechaInicio) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return java.sql.Date.valueOf( LocalDate.parse(fechaInicio, formatter));
    }
}
