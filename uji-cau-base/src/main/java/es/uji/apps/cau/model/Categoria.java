package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_CATEGORIAS_AREAS")
public class Categoria implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;

    private Boolean alta;

    @ManyToOne
    @JoinColumn(name = "CAU_AREAS_ID")
    private Area area;

    @OneToMany(mappedBy = "categoria")
    private Set<Parte> cauPartes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getAlta() {
        return alta;
    }

    public void setAlta(Boolean alta) {
        this.alta = alta;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Set<Parte> getCauPartes() {
        return cauPartes;
    }

    public void setCauPartes(Set<Parte> cauPartes) {
        this.cauPartes = cauPartes;
    }

}
