package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cau.dao.ParteTagDAO;
import es.uji.apps.cau.dao.TagDAO;
import es.uji.apps.cau.model.Parte;
import es.uji.apps.cau.model.ParteTag;
import es.uji.apps.cau.model.Tag;
import es.uji.commons.rest.ParamUtils;

@Service
public class TagService
{
    private TagDAO tagDAO;
    private ParteTagDAO parteTagDAO;

    @Autowired
    public TagService(TagDAO tagDAO, ParteTagDAO parteTagDAO)
    {
        this.tagDAO = tagDAO;
        this.parteTagDAO = parteTagDAO;
    }

    public List<Tag> getAll(String query)
    {
        return tagDAO.getAll(query);
    }

    public List<Tag> getParteAll(Long parteId)
    {
        return tagDAO.getParteAll(parteId);
    }

    public Tag getById(Long id)
    {
        return tagDAO.get(Tag.class, id).get(0);
    }

    public Tag insert(Tag tag)
    {
        return tagDAO.insert(tag);
    }

    public Tag update(Tag tag)
    {
        return tagDAO.update(tag);
    }

    public void delete(Long id)
    {
        tagDAO.delete(Tag.class, id);
    }

    @Transactional
    public Tag addParteTag(Long parteId, String nombre)
    {
        Tag tag = tagDAO.getTagByName(nombre);
        if (!ParamUtils.isNotNull(tag))
        {
            Tag newTag = new Tag();
            newTag.setNombre(nombre);
            tagDAO.insert(newTag);
            tag = newTag;
        }
        ParteTag parteTag = new ParteTag();
        parteTag.setTag(tag);
        Parte parte = new Parte();
        parte.setId(parteId);
        parteTag.setParte(parte);
        parteTagDAO.insert(parteTag);
        return tag;
    }

    public void deleteParteTag(Long parteId, Long tagId)
    {
        parteTagDAO.deleteParteTag(parteId,tagId);
    }
}
