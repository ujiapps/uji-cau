package es.uji.apps.cau.services.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.mysema.query.Tuple;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Area;
import es.uji.apps.cau.model.Persona;
import es.uji.apps.cau.model.TecnicoArea;
import es.uji.apps.cau.services.TecnicoAreaService;
import es.uji.apps.cau.services.TecnicoService;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("tecnicoarea")
public class TecnicoAreaResources extends CoreBaseService {
    @InjectParam
    private TecnicoAreaService tecnicoAreaService;

    @InjectParam
    private TecnicoService tecnicoService;

    private final SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> getTecnicosAreas(
            @QueryParam("nombreTecnico") @DefaultValue("") String filter) {
        ItemFilter item = new ItemFilter("nombreTecnico", filter);

        List<ItemFilter> filterList = new ArrayList<>();
        filterList.add(item);

        List<TecnicoArea> listTecnicoArea = tecnicoAreaService.getTecnicosAreas(filterList);
        List<UIEntity> listTecnicoAreaUI = new ArrayList<>();

        for (TecnicoArea tecnicoArea : listTecnicoArea) {
            UIEntity tecnicoAreaUI = tecnicoToUI(tecnicoArea);
            listTecnicoAreaUI.add(tecnicoAreaUI);
        }

        return listTecnicoAreaUI;
    }

    private UIEntity tecnicoToUI(TecnicoArea tecnicoArea) {
        UIEntity tecnicoAreaUI = UIEntity.toUI(tecnicoArea);
        tecnicoAreaUI.put("nombreTecnico", tecnicoArea.getTecnicoArea().getNombreCompleto());
        tecnicoAreaUI.put("cuenta", tecnicoArea.getTecnicoArea().getCuenta());
        tecnicoAreaUI.put("extension", tecnicoArea.getTecnicoArea().getExtensionTelefono());
        tecnicoAreaUI.put("nombreArea", tecnicoArea.getArea().getNombre());
        return tecnicoAreaUI;
    }

    @GET
    @Path("asignados")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> getTecnicosAreasAsignados(
            @QueryParam("nombreTecnico") @DefaultValue("") String filter) {
        ItemFilter item = new ItemFilter("nombreTecnico", filter);
        Long userId = AccessManager.getConnectedUserId(request);
        List<ItemFilter> filterList = new ArrayList<>();
        filterList.add(item);
        List<TecnicoArea> listTecnicoArea;
        if (tecnicoAreaService.hasPerfil("JEFE_AREA", userId)) {
            List<Long> areas = tecnicoService.getIdAreasByTecnico(userId);
            listTecnicoArea = tecnicoAreaService.getTecnicosAreasById(filterList, areas);
        } else {
            listTecnicoArea = tecnicoAreaService.getTecnicosAreas(filterList);
        }
        List<Tuple> result = tecnicoAreaService.getTecnicosPartesAsignados();
        List<UIEntity> listTecnicoAreaUI = new ArrayList<>();

        for (TecnicoArea tecnicoArea : listTecnicoArea) {
            UIEntity tecnicoAreaUI =tecnicoToUI(tecnicoArea);
            for (Tuple tupla : result) {
                if (tecnicoArea.getTecnicoArea().getId().equals(tupla.get(0, Long.class))) {
                    tecnicoAreaUI.put("asignadosCount", tupla.get(1, Long.class));
                }
            }
            listTecnicoAreaUI.add(tecnicoAreaUI);
        }

        return listTecnicoAreaUI;
    }

    @GET
    @Path("{tecnicoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTecnicoById() {
        Long userId = AccessManager.getConnectedUserId(request);

        TecnicoArea tecnico = tecnicoAreaService.getTecnicoAreaById(userId);
        return tecnicoToUI(tecnico);
    }

    @GET
    @Path("{tecnicoId}/perfil")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTecnicoByPerId(@PathParam("tecnicoId") Long perId) {
        try {
            return tecnicoToUI(tecnicoAreaService.getTecnicoAreaByPerId(perId));
        } catch (RegistroNoEncontradoException e) {
            return new UIEntity();
        }
    }

    @GET
    @Path("perfil")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTecnico() {
        Long userId = AccessManager.getConnectedUserId(request);

        try {
            return tecnicoToUI(tecnicoAreaService.getTecnicoAreaByPerId(userId));
        } catch (RegistroNoEncontradoException e) {
            return new UIEntity();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addTecnicoArea(UIEntity entity) throws ParseException {
        String tecnicoAreaId = entity.get("tecnicoAreaId");
        String areaId = entity.get("areaId");
        String tipo = entity.get("tipo");
        String fechaInicioInactividad = entity.get("fechaInicioInactividad");
        String fechaFinInactividad = entity.get("fechaFinInactividad");

        TecnicoArea tecnicoArea = new TecnicoArea();
        Area area = new Area();
        area.setId(Long.valueOf(areaId));
        Persona extpersona = new Persona();
        extpersona.setId(Long.valueOf(tecnicoAreaId));
        tecnicoArea.setTecnicoArea(extpersona);
        tecnicoArea.setTipo(tipo);
        if (ParamUtils.isNotNull(fechaInicioInactividad)) {
            tecnicoArea.setFechaInicioInactividad(formateadorFecha.parse(fechaInicioInactividad));
        } else {
            tecnicoArea.setFechaInicioInactividad(new Date());
        }
        if (ParamUtils.isNotNull(fechaFinInactividad)) {
            tecnicoArea.setFechaFinInactividad(formateadorFecha.parse(fechaFinInactividad));
        }

        tecnicoArea.setArea(area);
        try {
            tecnicoArea = tecnicoAreaService.insertTecnicoArea(tecnicoArea);
        } catch (Exception e) {

            throw e;
        }

        return tecnicoToUI(tecnicoArea);

    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addTecnicoArea(@PathParam("id") Long id, UIEntity entity) throws ParseException {
        String tecnicoAreaId = entity.get("tecnicoAreaId");
        String areaId = entity.get("areaId");
        String tipo = entity.get("tipo");
        String fechaInicioInactividad = entity.get("fechaInicioInactividad");
        String fechaFinInactividad = entity.get("fechaFinInactividad");

        TecnicoArea tecnicoArea = tecnicoAreaService.getTecnicoAreaById(id);
        Area area = new Area();
        area.setId(Long.valueOf(areaId));
        Persona extpersona = new Persona();
        extpersona.setId(Long.valueOf(tecnicoAreaId));
        tecnicoArea.setTecnicoArea(extpersona);
        tecnicoArea.setTipo(tipo);
        if (ParamUtils.isNotNull(fechaInicioInactividad)) {
            tecnicoArea.setFechaInicioInactividad(formateadorFecha.parse(fechaInicioInactividad));
        } else {
            tecnicoArea.setFechaInicioInactividad(new Date());
        }
        if (ParamUtils.isNotNull(fechaFinInactividad)) {
            tecnicoArea.setFechaFinInactividad(formateadorFecha.parse(fechaFinInactividad));
        }
        tecnicoArea.setArea(area);

        tecnicoArea = tecnicoAreaService.updateTecnicoArea(tecnicoArea);

        return tecnicoToUI(tecnicoArea);

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateTecnicoArea(UIEntity entity) {
        String id = entity.get("id");

        return tecnicoToUI(tecnicoAreaService.getTecnicoAreaById(Long.parseLong(id)));

    }

    @DELETE
    @Path("{tecnicoAreaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteArea(@PathParam("tecnicoAreaId") Long tecnicoAreaId, UIEntity entity) {
        if (tecnicoAreaId != null) {
            tecnicoAreaService.deleteTecnicoArea(tecnicoAreaId);
        }
    }
}
