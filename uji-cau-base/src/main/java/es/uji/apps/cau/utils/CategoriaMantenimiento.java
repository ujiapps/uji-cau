package es.uji.apps.cau.utils;

import java.util.HashMap;
import java.util.Map;

public enum CategoriaMantenimiento {
    AVISOS(0L), CRITICOS(1L);

    public final Long id;
    private static final Map<Long, CategoriaMantenimiento> BY_ID = new HashMap<>();

    static {
        for (CategoriaMantenimiento e : values()) {
            BY_ID.put(e.id, e);
        }
    }

    CategoriaMantenimiento(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public static CategoriaMantenimiento valueOfId(Long id) {
        return BY_ID.get(id);
    }

//    public static CategoriaMantenimiento of(Integer id) {
//        return Stream.of(CategoriaMantenimiento.values())
//                .filter(p -> p.getId() == id)
//                .findFirst()
//                .orElseThrow(IllegalArgumentException::new);
//    }
}
