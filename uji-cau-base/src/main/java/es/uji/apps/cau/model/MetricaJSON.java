package es.uji.apps.cau.model;
import org.codehaus.jackson.annotate.JsonProperty;

public class MetricaJSON {

    @JsonProperty("name_en")
    private String name_en;

    @JsonProperty("name_ca")
    private String name_ca;
    @JsonProperty("name_es")
    private String name_es;
    @JsonProperty("data")
    private String data;

    public MetricaJSON(String name_en, String name_ca, String name_es) {
        this.name_en = name_en;
        this.name_ca = name_ca;
        this.name_es = name_es;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ca() {
        return name_ca;
    }

    public void setName_ca(String name_ca) {
        this.name_ca = name_ca;
    }

    public String getName_es() {
        return name_es;
    }

    public void setName_es(String name_es) {
        this.name_es = name_es;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
