package es.uji.apps.cau.exceptions;

public class NotificacionSinDestinatarios extends GeneralCAUException
{
    public NotificacionSinDestinatarios()
    {
        super("No s\'han trobat destinataris de la notificació.");
    }

    public NotificacionSinDestinatarios(String message)
    {
        super(message);
    }

}