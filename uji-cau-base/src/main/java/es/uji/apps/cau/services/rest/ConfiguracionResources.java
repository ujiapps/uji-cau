package es.uji.apps.cau.services.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Configuracion;
import es.uji.apps.cau.services.ConfiguracionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("configuracion")
public class ConfiguracionResources extends CoreBaseService
{

    @InjectParam
    private ConfiguracionService configuracionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getConfiguracion()
    {
        return UIEntity.toUI(configuracionService.getConfiguracion());

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity updateCorreos(MultivaluedMap<String, String> params)
    {
        String inventario = params.getFirst("inventario");
        String encuestas = params.getFirst("encuestas");
        String sugerencias = params.getFirst("sugerencias");

        Configuracion configuracion = configuracionService.getConfiguracion();
        configuracion.setInventario(inventario);
        configuracion.setEncuestas(encuestas);
        configuracion.setSugerencias(sugerencias);

        configuracion = configuracionService.updateConfiguracion(configuracion);

        return UIEntity.toUI(configuracion);

    }

}
