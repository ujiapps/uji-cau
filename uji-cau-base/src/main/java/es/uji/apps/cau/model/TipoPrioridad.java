package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_TIPOS_PRIORIDADES")
public class TipoPrioridad implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String color;

    private String nombre;

    @OneToMany(mappedBy = "prioridad")
    private Set<Parte> partesPrioridad;

    @Column(name = "ID_IMPACTOS")
    private Long impactoId;

    @Column(name = "ID_URGENCIAS")
    private Long urgenciaId;

    public TipoPrioridad()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getColor()
    {
        return this.color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<Parte> getPartesPrioridad()
    {
        return this.partesPrioridad;
    }

    public void setPartesPrioridad(Set<Parte> cauPartes)
    {
        this.partesPrioridad = cauPartes;
    }

    public Long getImpactoId()
    {
        return impactoId;
    }

    public void setImpactoId(Long impactoId)
    {
        this.impactoId = impactoId;
    }

    public Long getUrgenciaId()
    {
        return urgenciaId;
    }

    public void setUrgenciaId(Long urgenciaId)
    {
        this.urgenciaId = urgenciaId;
    }
}
