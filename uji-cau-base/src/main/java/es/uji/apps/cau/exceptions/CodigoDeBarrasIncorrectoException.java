package es.uji.apps.cau.exceptions;

public class CodigoDeBarrasIncorrectoException extends GeneralCAUException
{
    public CodigoDeBarrasIncorrectoException()
    {
        super("El código de barras introducido es incorrecto");
    }

    public CodigoDeBarrasIncorrectoException(String message)
    {
        super(message);
    }
}
