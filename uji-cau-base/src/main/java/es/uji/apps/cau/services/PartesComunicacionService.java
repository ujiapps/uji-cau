package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cau.dao.ParteComunicacionDAO;
import es.uji.apps.cau.dao.PersonaDAO;
import es.uji.apps.cau.model.ParteComunicacion;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.apps.cau.utils.ItemSort;

@Service
public class PartesComunicacionService
{
    @Autowired
    private ParteComunicacionDAO parteComunicacionDAO;
    @Autowired
    private PersonaDAO personaDAO;

    @Transactional
    public ParteComunicacion insertParteComunicacion(ParteComunicacion parteComunicacion)
    {
        return parteComunicacionDAO.insert(parteComunicacion);
    }

    @Transactional
    public ParteComunicacion updateParteEstado(ParteComunicacion parteComunicacion)
    {
        return parteComunicacionDAO.update(parteComunicacion);
    }

    @Transactional
    public void deleteParteComunicacion(Long id)
    {
        parteComunicacionDAO.delete(ParteComunicacion.class, id);
    }

    public List<ParteComunicacion> getPublicParteComunicados(Long parteId, Long userId,
                                                             List<ItemFilter> filterList, List<ItemSort> sortList) {
        return parteComunicacionDAO.getPublicParteComunicados(parteId, userId, filterList,
                sortList);
    }

    public List<ParteComunicacion> getParteComunicados(Long parteId, Long userId,
            List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        return parteComunicacionDAO.getParteComunicados(parteId, userId, filterList,
                sortList);

    }
}
