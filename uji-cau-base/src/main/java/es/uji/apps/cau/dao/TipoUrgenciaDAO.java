package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.QTipoUrgencia;
import es.uji.apps.cau.model.TipoUrgencia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoUrgenciaDAO extends BaseDAODatabaseImpl
{
    private QTipoUrgencia qTipoUrgencia = QTipoUrgencia.tipoUrgencia;

    public List<TipoUrgencia> getTiposUrgencias()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoUrgencia);

        return query.list(qTipoUrgencia);
    }

    public TipoUrgencia getTipoUrgencia(Long tipoUrgenciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoUrgencia).where(qTipoUrgencia.id.eq(tipoUrgenciaId));

        List<TipoUrgencia> tipoUrgenciaList = query.list(qTipoUrgencia);
        if (tipoUrgenciaList.size() > 0)
        {
            return tipoUrgenciaList.get(0);
        }
        return null;
    }
}
