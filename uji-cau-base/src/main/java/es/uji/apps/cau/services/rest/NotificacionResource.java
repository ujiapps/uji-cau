package es.uji.apps.cau.services.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.services.NotificacionService;
import es.uji.apps.cau.services.PartesService;
import es.uji.apps.cau.services.PersonaService;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;

@Path("notificacion")
public class NotificacionResource extends CoreBaseService
{
    @InjectParam
    private PersonaService personaService;
    @InjectParam
    private PartesService partesService;

    @POST
    @Path("public/sugerencia")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addPublicParte(@FormParam("asunto") String asunto,
                                   @FormParam("mensaje") String mensaje) throws URISyntaxException, MessageNotSentException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(asunto,mensaje);

        NotificacionService.enviarSugerencia(personaService.getPersonaById(userId), asunto, mensaje);
        return Response.status(Response.Status.SEE_OTHER).location(new URI("parte/public/listado"))
                .build();
    }
}
