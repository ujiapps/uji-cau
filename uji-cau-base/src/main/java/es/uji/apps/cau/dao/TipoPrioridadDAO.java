package es.uji.apps.cau.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.QTipoPrioridad;
import es.uji.apps.cau.model.TipoPrioridad;
import es.uji.apps.cau.utils.DynamicFilter;
import es.uji.apps.cau.utils.ItemFilter;
import es.uji.apps.cau.utils.ItemSort;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoPrioridadDAO extends BaseDAODatabaseImpl
{
    private QTipoPrioridad qTipoPrioridad = QTipoPrioridad.tipoPrioridad;

    public List<TipoPrioridad> getTiposPrioridades(List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoPrioridad);

        Map<String, Object> validFilters = new HashMap<String, Object>();

        validFilters.put("urgenciaId", qTipoPrioridad.urgenciaId);
        validFilters.put("impactoId", qTipoPrioridad.impactoId);


        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        return query.list(qTipoPrioridad);
    }

    public TipoPrioridad getTipoPrioridad(Long tipoPrioridadId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoPrioridad).where(qTipoPrioridad.id.eq(tipoPrioridadId));

        List<TipoPrioridad> tipoPrioridadList = query.list(qTipoPrioridad);
        if (tipoPrioridadList.size() > 0)
        {
            return tipoPrioridadList.get(0);
        }
        return null;
    }

    public TipoPrioridad getTipoPrioridadByImpactoAndUrgencia(Long impactoId, Long urgenciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipoPrioridad).where(qTipoPrioridad.impactoId.eq(impactoId).and(qTipoPrioridad.urgenciaId.eq(urgenciaId)));

        List<TipoPrioridad> tipoPrioridadList = query.list(qTipoPrioridad);
        if (tipoPrioridadList.size() > 0)
        {
            return tipoPrioridadList.get(0);
        }
        return null;
    }
}
