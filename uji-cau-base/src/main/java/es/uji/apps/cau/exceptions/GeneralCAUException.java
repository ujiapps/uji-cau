package es.uji.apps.cau.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class GeneralCAUException extends CoreBaseException
{
    public GeneralCAUException(String message)
    {
        super(message);
    }
}
