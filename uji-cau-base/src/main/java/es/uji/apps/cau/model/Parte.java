package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import es.uji.apps.cau.exceptions.CodigoDeBarrasIncorrectoException;
import es.uji.apps.cau.exceptions.CodigoDeBarrasNoNumericoException;

@Entity
@Table(name = "CAU_PARTES")
public class Parte implements Serializable {
    @Id
    @SequenceGenerator(name = "parteSeq", sequenceName = "PARTES_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "parteSeq")
    private Long id;

    private String asunto;

    @Column(name = "CODIGO_BARRAS")
    @NotFound( action = NotFoundAction.IGNORE )
    private String codigoBarras;

    private String descripcion;

    @Column(name = "EXTENSION_TELEFONO")
    private String extensionTelefono;

    private Long presente;

    private String tipo;

    private Integer leido;
    private Integer suscrito;

    @Column(name = "NOTIFICA_TECNICO")
    private Integer notificaTecnico;

    @ManyToOne
    @JoinColumn(name = "PAR_ID_EXTERNO")
    private Parte parteExterno;

    @ManyToOne
    @JoinColumn(name = "ID_AREA")
    private Area area;

    @ManyToOne
    @JoinColumn(name = "PER_ID_CREACION")
    private Persona personaCreacion;

    @ManyToOne
    @JoinColumn(name = "PER_ID_SOLICITANTE")
    private Persona personaSolicitante;

    @ManyToOne
    @JoinColumn(name = "PER_ID_AFECTADO")
    private Persona personaAfectado;

    @ManyToOne
    @JoinColumn(name = "PER_ID_ASIGNADO")
    private Persona personaAsignado;

    @ManyToOne
    @JoinColumn(name = "ID_UBICACION_FISICA")
    @NotFound( action = NotFoundAction.IGNORE )
    private UbicacionFisica ubicacionFisica;

    @ManyToOne
    @JoinColumn(name = "ID_IMPACTO")
    private TipoImpacto impacto;

    @ManyToOne
    @JoinColumn(name = "ID_PRIORIDAD")
    private TipoPrioridad prioridad;

    @ManyToOne
    @JoinColumn(name = "ID_URGENCIA")
    private TipoUrgencia urgencia;

    @OneToMany(mappedBy = "parteComunicacion")
    private Set<ParteComunicacion> comunicaciones;

    @OneToMany(mappedBy = "parteEstado")
    private Set<ParteEstado> estados;

    @OneToMany(mappedBy = "parteFichero")
    private Set<ParteFichero> ficheros;

    @OneToMany(mappedBy = "parte")
    private Set<Encuesta> encuesta;

    @ManyToOne
    @JoinColumn(name = "ID_CATEGORIA")
    private Categoria categoria;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO")
    private TipoParte tipoParte;

    @OneToMany(mappedBy = "parte")
    private Set<ParteTag> parteTags;

    public Parte() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsunto() {
        return this.asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    private boolean esUnCodigoDeBarrasValido(String codigoBarras)
            throws CodigoDeBarrasNoNumericoException {

        if (codigoBarras.length() != 12) {
            return false;
        }
        try {
            Long.parseLong(codigoBarras);
        } catch (Exception e) {
            throw new CodigoDeBarrasNoNumericoException();
        }

        Integer sum = 0;
        for (Integer c = codigoBarras.length() - 2; c >= 0; c--) {
            sum += Character.getNumericValue(codigoBarras.charAt(c)) * (((c + 1) % 2 == 0) ? 1 : 3);
        }

        Integer dif = 10 - (sum % 10);
        if (dif == 10) {
            dif = 0;
        }
        if (dif != Character.getNumericValue(codigoBarras.charAt(codigoBarras.length() - 1))) {
            return false;
        }
        return true;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) throws CodigoDeBarrasIncorrectoException,
            CodigoDeBarrasNoNumericoException {
        if (codigoBarras == null || codigoBarras.isEmpty()) {
            this.codigoBarras = codigoBarras;
        } else {
            if (!esUnCodigoDeBarrasValido(codigoBarras)) {
                throw new CodigoDeBarrasIncorrectoException();
            }
        }
        this.codigoBarras = codigoBarras;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExtensionTelefono() {
        return this.extensionTelefono;
    }

    public void setExtensionTelefono(String extensionTelefono) {
        this.extensionTelefono = extensionTelefono;
    }

    public Long getPresente() {
        return this.presente;
    }

    public void setPresente(Long presente) {
        this.presente = presente;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getLeido() {
        return leido;
    }

    public void setLeido(Integer leido) {
        this.leido = leido;
    }

    public Integer getSuscrito()
    {
        return suscrito;
    }

    public void setSuscrito(Integer suscrito)
    {
        this.suscrito = suscrito;
    }

    public Area getArea() {
        return this.area;
    }

    public void setArea(Area cauArea) {
        this.area = cauArea;
    }

    public Parte getParteExterno() {
        return parteExterno;
    }

    public void setParteExterno(Parte parteExterno) {
        this.parteExterno = parteExterno;
    }

    public Persona getPersonaCreacion() {
        return this.personaCreacion;
    }

    public void setPersonaCreacion(Persona cauPersona1) {
        this.personaCreacion = cauPersona1;
    }

    public Persona getPersonaSolicitante() {
        return this.personaSolicitante;
    }

    public void setPersonaSolicitante(Persona cauPersona2) {
        this.personaSolicitante = cauPersona2;
    }

    public Persona getPersonaAfectado() {
        return this.personaAfectado;
    }

    public void setPersonaAfectado(Persona cauPersona3) {
        this.personaAfectado = cauPersona3;
    }

    public Persona getPersonaAsignado() {
        return this.personaAsignado;
    }

    public void setPersonaAsignado(Persona cauPersona4) {
        this.personaAsignado = cauPersona4;
    }

    public UbicacionFisica getUbicacionFisica() {
        return this.ubicacionFisica;
    }

    public void setUbicacionFisica(UbicacionFisica cauExtUbicacionesFisica) {
        this.ubicacionFisica = cauExtUbicacionesFisica;
    }

    public TipoImpacto getImpacto() {
        return this.impacto;
    }

    public void setImpacto(TipoImpacto cauTiposImpacto) {
        this.impacto = cauTiposImpacto;
    }

    public TipoPrioridad getPrioridad() {
        return this.prioridad;
    }

    public void setPrioridad(TipoPrioridad cauTiposPrioridade) {
        this.prioridad = cauTiposPrioridade;
    }

    public TipoUrgencia getUrgencia() {
        return this.urgencia;
    }

    public void setUrgencia(TipoUrgencia cauTiposUrgencia) {
        this.urgencia = cauTiposUrgencia;
    }

    public Set<ParteComunicacion> getComunicaciones() {
        return this.comunicaciones;
    }

    public void setComunicaciones(Set<ParteComunicacion> cauPartesComunicaciones) {
        this.comunicaciones = cauPartesComunicaciones;
    }

    public Set<ParteEstado> getEstados() {
        return this.estados;
    }

    public void setEstados(Set<ParteEstado> cauPartesEstados) {
        this.estados = cauPartesEstados;
    }

    public Set<ParteFichero> getFicheros() {
        return this.ficheros;
    }

    public void setFicheros(Set<ParteFichero> cauPartesFicheros) {
        this.ficheros = cauPartesFicheros;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public TipoParte getTipoParte() {
        return tipoParte;
    }

    public void setTipoParte(TipoParte tipoParte) {
        this.tipoParte = tipoParte;
    }

    public Set<Encuesta> getEncuesta()
    {
        return encuesta;
    }

    public void setEncuesta(Set<Encuesta> encuesta)
    {
        this.encuesta = encuesta;
    }

    public Set<ParteTag> getParteTags()
    {
        return parteTags;
    }

    public void setParteTags(Set<ParteTag> parteTags)
    {
        this.parteTags = parteTags;
    }

    public Integer getNotificaTecnico()
    {
        return notificaTecnico;
    }

    public void setNotificaTecnico(Integer notificaTecnico)
    {
        this.notificaTecnico = notificaTecnico;
    }
}
