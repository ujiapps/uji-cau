package es.uji.apps.cau.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cau.dao.ElementoInventarioDAO;
import es.uji.apps.cau.model.ElementoInventario;

@Service
public class ElementoInventarioService {
    @Autowired
    private ElementoInventarioDAO elementoInventarioDAO;

    public ElementoInventario getExtElementoInventarioCB(String cb) {
        return elementoInventarioDAO.getExtElementoInventarioCB(cb);
    }

    public ElementoInventario getInventarioByCB(String cb) {
        return elementoInventarioDAO.getInventarioByCB(cb);
    }
    public List<ElementoInventario> searchExtElementoInventarioCB(String search) {
        return elementoInventarioDAO.searchExtElementoInventarioCB(search);
    }

    public List<ElementoInventario> searchInventarioById(Long lineaId) {
        return elementoInventarioDAO.searchInventarioById(lineaId);
    }


    public List<ElementoInventario> getInventarioByUbicacionId(String ubicacionId) {
        return elementoInventarioDAO.getInventarioByUbicacionId(ubicacionId);
    }
}
