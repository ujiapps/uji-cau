package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CAU_EXT_UBICACIONES_FISICAS")
public class UbicacionFisica implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "FIS_ID")
    private Long ubicacionFisicaId;

    @Column(name = "UBI_ID")
    private Long departamentoId;

    @Column(name = "NOMBRE_UBICACION")
    private String nombreDepartamento;

    @Column(name = "ESTADO")
    private String estado;

    @OneToMany(mappedBy = "ubicacionFisica")
    private Set<Parte> partesUbicacionFisica;

    @OneToMany(mappedBy = "ubicacionFisica")
    private Set<Persona> personas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getUbicacionFisicaId() {
        return ubicacionFisicaId;
    }

    public void setUbicacionFisicaId(Long ubicacionFisicaId) {
        this.ubicacionFisicaId = ubicacionFisicaId;
    }

    public Long getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(Long departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Set<Parte> getPartesUbicacionFisica() {
        return partesUbicacionFisica;
    }

    public void setPartesUbicacionFisica(Set<Parte> partesUbicacionFisica) {
        this.partesUbicacionFisica = partesUbicacionFisica;
    }

    public Set<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(Set<Persona> personas) {
        this.personas = personas;
    }
}



