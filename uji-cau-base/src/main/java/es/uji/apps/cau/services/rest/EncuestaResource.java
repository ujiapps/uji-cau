package es.uji.apps.cau.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cau.model.Encuesta;
import es.uji.apps.cau.services.EncuestaService;
import es.uji.apps.cau.utils.Paginacion;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("encuesta")
public class EncuestaResource extends CoreBaseService
{
    public final static Logger log = LoggerFactory.getLogger(EncuestaResource.class);

    @InjectParam
    private EncuestaService encuestaService;

    private List<UIEntity> toUI(List<Encuesta> encuestas)
    {
        return encuestas.stream().map(this::toUI).collect(Collectors.toList());
    }

    private UIEntity toUI(Encuesta encuesta)
    {
        UIEntity uiEntity = UIEntity.toUI(encuesta);
        uiEntity.put("afectadoNombre", encuesta.getParte().getAfectadoNombreCompleto());
        uiEntity.put("tecnicoNombre", encuesta.getParte().getAsignadoNombreCompleto());
        return uiEntity;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEncuesta(@QueryParam("query") @DefaultValue("") String query,
                                      @QueryParam("start") @DefaultValue("0") Long start,
                                      @QueryParam("limit") @DefaultValue("25") Long limit,
                                      @QueryParam("sort") @DefaultValue("[]") String sortJson)
    {

        ResponseMessage responseMessage = new ResponseMessage();

        try
        {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);
            responseMessage.setData(toUI(encuestaService.getEncuestas(query,paginacion)));
            responseMessage.setTotalCount(paginacion.getTotalCount());
            responseMessage.setSuccess(true);
        }
        catch (Exception e)
        {
            log.error("Error en obtener encuestas");
            responseMessage.setSuccess(false);
        }

        return responseMessage;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getPEncuesta(@PathParam("id") Long id)
    {
        return UIEntity.toUI(encuestaService.getById(id));
    }
}
