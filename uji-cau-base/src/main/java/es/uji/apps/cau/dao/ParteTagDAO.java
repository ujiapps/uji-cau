package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.ParteTag;
import es.uji.apps.cau.model.QParteTag;
import es.uji.apps.cau.model.QTag;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ParteTagDAO extends BaseDAODatabaseImpl
{
    QTag qTag= QTag.tag;
    QParteTag qParteTag = QParteTag.parteTag;

    public void deleteParteTag(Long parteId, Long tagId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qParteTag).join(qParteTag.tag,qTag).where(qParteTag.parte.id.eq(parteId).and(qTag.id.eq(tagId)));

        List<ParteTag> parteTag = query.list(qParteTag);

        for(ParteTag par : parteTag){
            delete(ParteTag.class,par.getId());
        }

    }
}
