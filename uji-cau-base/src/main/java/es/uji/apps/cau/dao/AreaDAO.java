package es.uji.apps.cau.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cau.model.Area;
import es.uji.apps.cau.model.QArea;
import es.uji.apps.cau.model.QTecnicoArea;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AreaDAO extends BaseDAODatabaseImpl {
    private QArea qArea = QArea.area;

    public List<Area> getAreas(Integer externa) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qArea);

        query.where(qArea.externa.eq(externa)).orderBy(qArea.nombre.asc());

        return query.list(qArea);
    }

    public Area getAreaById(Long areaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qArea).where(qArea.id.eq(areaId));

        List<Area> areaList = query.list(qArea);
        if (areaList.size() > 0) {
            return areaList.get(0);
        }
        return null;
    }

    public List<Area> getAreasByTecnicoId(Long tecnicoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QTecnicoArea qTecnicoArea = QTecnicoArea.tecnicoArea1;

        query.from(qArea).leftJoin(qArea.tecnicos, qTecnicoArea).fetch()
                .where(qTecnicoArea.tecnicoArea.id.eq(tecnicoId));

        return query.list(qArea);
    }
}
