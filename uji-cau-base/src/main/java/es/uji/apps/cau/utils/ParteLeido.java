package es.uji.apps.cau.utils;

public enum ParteLeido {
    NOLEIDO(0), LEIDOUSER(1), LEIDOTECNICO(2), LEIDOTODOS(3), LEIDOPRIMERA(4);

    private final Integer value;

    private ParteLeido(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public static Integer update(Integer estado, Boolean usuario) {
        if (usuario) {
            if (estado.equals(NOLEIDO.getValue())) {
                return LEIDOUSER.getValue();
            }
            if (estado.equals(LEIDOUSER.getValue())) {
                return LEIDOUSER.getValue();
            }
            if (estado.equals(LEIDOTECNICO.getValue())) {
                return LEIDOTODOS.getValue();
            }
            if (estado.equals(LEIDOTODOS.getValue())) {
                return LEIDOTODOS.getValue();
            }
            if (estado.equals(LEIDOPRIMERA.getValue())) {
                return LEIDOTODOS.getValue();
            }
        } else {
            if (estado.equals(NOLEIDO.getValue())) {
                return LEIDOTECNICO.getValue();
            }
            if (estado.equals(LEIDOUSER.getValue())) {
                return LEIDOTODOS.getValue();
            }
            if (estado.equals(LEIDOTECNICO.getValue())) {
                return LEIDOTECNICO.getValue();
            }
            if (estado.equals(LEIDOTODOS.getValue())) {
                return LEIDOTODOS.getValue();
            }
            if (estado.equals(LEIDOPRIMERA.getValue())) {
                return LEIDOTODOS.getValue();
            }
        }
        return NOLEIDO.getValue();
    }

}
