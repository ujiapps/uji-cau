package es.uji.apps.cau.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CAU_PARTES_ESTADOS")
public class ParteEstado implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer actual;

    @ManyToOne
    @JoinColumn(name = "ESTADO_ID")
    private TipoEstado tipoEstado;

    private String descripcion;

    @Column(name = "F_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "F_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @ManyToOne
    @JoinColumn(name = "PER_ID_FIN")
    private Persona personaFin;

    @ManyToOne
    @JoinColumn(name = "PER_ID_INICIO")
    private Persona personaIni;

    @ManyToOne
    @JoinColumn(name = "PAR_ID")
    private Parte parteEstado;

    public ParteEstado() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoEstado getTipoEstado() {
        return tipoEstado;
    }

    public void setTipoEstado(TipoEstado tipoEstado) {
        this.tipoEstado = tipoEstado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Integer getActual() {
        return actual;
    }

    public void setActual(Integer actual) {
        this.actual = actual;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Persona getPersonaFin() {
        return this.personaFin;
    }

    public void setPersonaFin(Persona cauPersona1) {
        this.personaFin = cauPersona1;
    }

    public Persona getPersonaIni() {
        return this.personaIni;
    }

    public void setPersonaIni(Persona cauPersona2) {
        this.personaIni = cauPersona2;
    }

    public Parte getParteEstado() {
        return this.parteEstado;
    }

    public void setParteEstado(Parte cauParte) {
        this.parteEstado = cauParte;
    }

}
