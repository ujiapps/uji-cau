package es.uji.apps.cau.model;

import org.junit.Test;

import es.uji.apps.cau.exceptions.CodigoDeBarrasIncorrectoException;
import es.uji.apps.cau.exceptions.CodigoDeBarrasNoNumericoException;

import static org.junit.Assert.assertEquals;

public class ParteTest
{
    private String codigoBarrasCorrecto = "200000709617";
    private String codigoBarrasIncorrecto = "2";
    private String codigoBarrasNoNumerico = "AAAAAAAAAAAA";

    @Test
    public void elCodigoDeBarrasDebeTenerUnFormatoCorrecto() throws Exception
    {
        Parte parte = new Parte();
        parte.setCodigoBarras(codigoBarrasCorrecto);

        assertEquals(codigoBarrasCorrecto, parte.getCodigoBarras());
    }

    @Test(expected = CodigoDeBarrasIncorrectoException.class)
    public void conUnCodigoDeBarrasIncorrectoSeLanzaUnaExcepcion() throws Exception
    {
        Parte parte = new Parte();
        parte.setCodigoBarras(codigoBarrasIncorrecto);
    }

    @Test(expected = CodigoDeBarrasNoNumericoException.class)
    public void losCodigoDeBarrasDeberianSerNumericos() throws Exception
    {
        Parte parte = new Parte();
        parte.setCodigoBarras(codigoBarrasNoNumerico);
    }
}
