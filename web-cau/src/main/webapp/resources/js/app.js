const STATUS_CLASS_DEFAULT = 'nodata';
const STATUS_CLASS = {
    OK: 'up',
    WARN: 'warning',
    CRITICAL: 'down',

}

function getLang() {
    let cookie = {};
    document.cookie.split(';').forEach(function (el) {
        let [key, value] = el.split('=');
        cookie[key.trim()] = value;
    })
    return cookie['uji-lang'] || 'ca';
}

const createListElement = (service) => {
    let li = document.createElement("li");
    let servicioNombre = document.createTextNode(getLang() === 'ca' ? service.name_ca : service.name_es);
    li.appendChild(servicioNombre);
    li.classList.add(STATUS_CLASS[service.status] || STATUS_CLASS_DEFAULT);
    return li;
}

const createAlertElement = (texto, role) => {
    let alert = document.createElement("div");
    alert.setAttribute("role", role);
    alert.className = "alert " + role;
    if (role === "error") {
        alert.innerHTML = '<i class="fas fa-exclamation-triangle" aria-hidden="true"></i>' + texto;
    } else {
        alert.innerHTML = '<i class="fas fa-times-circle" aria-hidden="true"></i>' + texto;
    }
    return alert;
}

async function updateStatus() {
    //const url = 'http://nnodor.uji.es/icinga/feed/system-status.json';
    const file = "/web-cau/resources/assets/status.json";
    const url = file + "?v=" + Date.now();
    const listaServicios = document.querySelector(".servidoresSeccion .lista");
    listaServicios.innerHTML = "";
    try {
        const response = await fetch(url, {
            cache: "no-cache"
        });
        const serviceStatus = await response.json();
        serviceStatus.forEach((service) => {
            let li = createListElement(service);
            listaServicios.appendChild(li);
        });
    } catch (error) {
        let li = createListElement({
            "name_ca": "Sense dades",
            "name_es": "Sin datos",
            "status": "nodata"
        });
        listaServicios.appendChild(li);
        console.error("Fetch error: ", error);
    }
}

function loadHorarioAtencion(data) {
    if (!data) return;
    const horarioAtencion = document.querySelector("#horarioAtencion");
    horarioAtencion.innerHTML = "";

    const horarios = data[getLang()] || data.ca;
    horarios.forEach(horario => {
        const tag = document.createElement("p");
        const text = document.createTextNode(horario);
        tag.appendChild(text);
        horarioAtencion.appendChild(tag);
    })
}

function loadAlertar(data) {
    if (!data) return;
    const alertas = document.querySelector("#alertas");
    alertas.innerHTML = "";
    data.forEach((mensaje) => {
        let alerta =
            getLang() === "es"
                ? createAlertElement(mensaje.mensajeES, mensaje.nivel)
                : getLang() === "en"
                    ? createAlertElement(mensaje.mensajeEN, mensaje.nivel)
                    : createAlertElement(mensaje.mensajeCA, mensaje.nivel);
        alertas.appendChild(alerta);
    });
}

function loadEstadisticas(estadisticas) {
    const incidenciasCreadas = document.querySelector("#incidenciasCreadas .numbers");
    const incidenciasResueltas = document.querySelector("#incidenciasResueltas .numbers");
    const tiempoResolucion = document.querySelector("#tiempoResolucion .numbers");
    const llamadasAtencidas = document.querySelector("#llamadasAtencidas .numbers");
    const disponibilidadServicios = document.querySelector("#disponibilidadServicios .numbers");
    incidenciasCreadas.innerHTML = estadisticas && estadisticas.incidencias_creadas || 0;
    incidenciasResueltas.innerHTML = estadisticas && estadisticas.incidencias_resueltas || 0;
    tiempoResolucion.innerHTML = estadisticas && estadisticas.tiempos_resolucion || "0h";
    llamadasAtencidas.innerHTML = estadisticas && estadisticas.llamadas_atendidas || 0;
    disponibilidadServicios.innerHTML = estadisticas && estadisticas.disponibilidad_servicios || "0%";

}

function loadContent() {
    const file = "/web-cau/resources/assets/data.json";
    const url = file + "?v=" + Date.now();
    fetch(url, {
        cache: "no-cache"
    })
        .then(response => response.json())
        .then(data => {
            loadHorarioAtencion(data.horarioAtencion);
            loadAlertar(data.alertas);
            loadEstadisticas(data.estadisticas);
        }).catch(() => {
        console.error('Error al obtener datos');
    });
}

function init() {
    loadContent();
    updateStatus();
    setInterval(updateStatus, 120000);
}

window.addEventListener('DOMContentLoaded', (event) => {
    init()
});
