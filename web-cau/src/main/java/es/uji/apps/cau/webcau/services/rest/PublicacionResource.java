package es.uji.apps.cau.webcau.services.rest;

import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Pagina;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.text.ParseException;
import java.util.Locale;
import java.util.UUID;

@Path("publicacion")
public class PublicacionResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template get(@Context HttpServletRequest request, @Context HttpServletResponse response,
                        @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws IOException, ParseException {

        Template template = new HTMLTemplate("cau/web-cau/base", new Locale(idioma), "cau");
        template.put("userId", AccessManager.getConnectedUserId(request));
        template.put("currentDate", java.util.Calendar.getInstance());
        template.put("cacheControl", UUID.randomUUID().toString());

        Pagina pagina = new Pagina("web-cau", "rest/publicacion", idioma, "Portal CAU");

        template.put("pagina", pagina);
        template.put("urlBase", "/web-cau/rest/publicacion");
        template.put("mostrarLogos", false);

        return template;
    }

}
