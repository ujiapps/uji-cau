package es.uji.apps.cau.avisos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.uji.apps.cau.model.Alerta;
import es.uji.apps.cau.model.AlertaParte;
import es.uji.apps.cau.model.ParteAlertaVW;
import es.uji.apps.cau.model.TecnicoArea;
import es.uji.apps.cau.services.AlertaService;
import es.uji.apps.cau.services.PartesEstadoService;
import es.uji.apps.cau.services.TecnicoAreaService;


public class AvisosProcess implements Runnable
{
    static final Logger log = Logger.getLogger(AvisosProcess.class);
    private static final int TIEMPO_ESPERA_ENTRE_CONSULTAS = 60000;
    private AlertaService alertaService;
    private TecnicoAreaService tecnicoAreaService;
    private PartesEstadoService partesEstadoService;
    private SimpleDateFormat hourOfDay = new SimpleDateFormat("HH");
    private SimpleDateFormat dayOfWeek = new SimpleDateFormat("u");

    public AvisosProcess(AlertaService alertaService, TecnicoAreaService tecnicoAreaService, PartesEstadoService partesEstadoService)
    {
        this.alertaService = alertaService;
        this.tecnicoAreaService = tecnicoAreaService;
        this.partesEstadoService = partesEstadoService;
    }

    public static void main(String[] args) throws Exception
    {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "classpath:applicationContext.xml");
        AlertaService alertaService = context.getBean(AlertaService.class);
        TecnicoAreaService tecnicoAreaService = context.getBean(TecnicoAreaService.class);
        PartesEstadoService partesEstadoService = context.getBean(PartesEstadoService.class);

        AvisosProcess avisosProcess = new AvisosProcess(alertaService, tecnicoAreaService, partesEstadoService);
        log.info("START CAU Avisos " + new Date());
        new Thread(avisosProcess).start();
    }

    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                Date date = new Date();
                Integer hour = Integer.parseInt(hourOfDay.format(date));
                Integer dayOfweek = Integer.parseInt(dayOfWeek.format(date));
                if (hour <= 19 && hour >= 8 && dayOfweek < 6)
                {
                    List<ParteAlertaVW> partesAlertas = alertaService.getAllPartesAlertas();

                    for (ParteAlertaVW parteAlerta : partesAlertas)
                    {
                        List<String> destinatarios = new ArrayList<>();
                        if (parteAlerta.getNivel().equals(1L))
                        {
                            List<TecnicoArea> tecnicoAreas = tecnicoAreaService.getTecnicosPrimera();
                            destinatarios = new ArrayList<>();
                            for (TecnicoArea tecnicoArea : tecnicoAreas)
                            {
                                destinatarios.add(tecnicoArea.getTecnicoArea().getCuenta());
                            }

                        }
                        else if (parteAlerta.getNivel().equals(2L))
                        {
                            List<TecnicoArea> tecnicoAreas = tecnicoAreaService.getJefesAreaByTecnico(parteAlerta.getAsignadoPerId());
                            destinatarios = new ArrayList<>();
                            destinatarios.add(parteAlerta.getAsignadoCuenta());
                            for (TecnicoArea tecnicoArea : tecnicoAreas)
                            {
                                destinatarios.add(tecnicoArea.getTecnicoArea().getCuenta());
                            }
                        }
                        else
                        {
                            List<TecnicoArea> tecnicoAreas = tecnicoAreaService.getJefesAreaByTecnico(parteAlerta.getAsignadoPerId());
                            destinatarios.add(parteAlerta.getAsignadoCuenta());
                            destinatarios.add("navarro@uji.es");
                            for (TecnicoArea tecnicoArea : tecnicoAreas)
                            {
                                destinatarios.add(tecnicoArea.getTecnicoArea().getCuenta());
                            }
                        }

                        AlertaParte alertaParte = new AlertaParte();
                        alertaParte.setFechaAlerta(new Date());
                        Alerta a = new Alerta();
                        a.setId(1L);
                        alertaParte.setAlerta(a);
                        alertaParte.setParteId(parteAlerta.getParteId());
                        alertaParte.setEnvios(destinatarios.toString());
                        alertaService.addAlertaParte(alertaParte);
                        alertaService.enviarAlerta(destinatarios, parteAlerta.getParteId(), parteAlerta.getNivel());
                    }
                    log.info("Alertas enviadas: " + partesAlertas.size());
                }
                try
                {
                    Integer autoResueltos = partesEstadoService.setParteAutoResuelto();
                    log.info("Total de partes autoResueltos: " + autoResueltos);
                }
                catch (Throwable e)
                {
                    log.error(e);
                }

                Thread.sleep(TIEMPO_ESPERA_ENTRE_CONSULTAS);
            }
            catch (Exception e)
            {
                log.error(e);
            }

        }
    }
}
