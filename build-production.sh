#!/bin/bash

export EXTJS_WORKSPACE_HOME=/opt/devel/workspaces/uji/uji-commons-extjs6-workspace
export EXTJS_APP_NAME=CAU
export EXTJS_APP=$EXTJS_WORKSPACE_HOME/apps/cau
export APP_PATH=$(pwd)
export ENV=production

# Build ExtJS

cd $EXTJS_WORKSPACE_HOME
git fetch && git rebase

cd $EXTJS_APP 
sencha app clean
sencha app build -${ENV}

# Copy ExtJS into project

cd $APP_PATH/uji-cau-base
cp -vR ${EXTJS_WORKSPACE_HOME}/build/${ENV}/${EXTJS_APP_NAME}/* src/main/webapp

# Build Java project

mvn -DskipTests -DskipDocker clean package
